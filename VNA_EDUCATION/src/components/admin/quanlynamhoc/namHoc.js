import React, { useState, useEffect } from 'react'
import { Button, Modal, Form, Table } from 'react-bootstrap';
import { FiEdit, FiDelete } from 'react-icons/fi'
import { useForm } from "react-hook-form";
import { Link } from 'react-router-dom'
import swal from 'sweetalert';
// import axios from 'axios';
import TitleBreadcrumb from '../../common/TitleBreadcrumb';
import NamHocApi from '../../../api/NamHocApi';

export default function NamHoc() {
    const { register, handleSubmit, formState: { errors } } = useForm();
    const [show, setShow] = useState(false);
    const [namhoc, setNamHoc] = useState([]);
    const [showEdit, setShowEdit] = useState();
    const [showdetail, setShowDetail] = useState(false);

    // console.log(typeof (namhoc), 'nam');

    // const [showdetail, setShowDetail] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);


    const getNamHoc = async () => {
        let result = await NamHocApi.getNamHoc()
        await setNamHoc(result.data);

    }

    useEffect(() => {
        getNamHoc()
    }, [])

    //edit modal
    async function Edit(id) {
        let result = await NamHocApi.Edit(id);
        await setShowEdit(result.data);
        await setShowDetail(true)
    }

    const Delete = (id) => {
        // console.log('iddelete', id);
        swal({
            title: "Bạn có chắc muốn xóa dữ liệu này?",
            text: "Hãy Cân nhắc kỹ trước khi bạn làm việc này!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then(async (res) => {
            if (res) {
                swal("Xóa thành công", {
                    icon: "success",
                    buttons: false
                });
                await NamHocApi.remove(id);
                await getNamHoc()
                await handleClose()

            } else {
                swal("Đã hủy!");
            }
        });

    }

    const onSubmit = (data) => {
        console.log(data, 1111)
        swal({
            title: "Bạn có chắc muốn tạo đánh giá này không ?",
            text: "Cân nhắc kỹ trước khi bạn thực hiện điều này!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then(async (res) => {
            if (res) {
                swal("Thêm thành công", {
                    icon: "success",
                    buttons: false
                });
                await NamHocApi.add(data);
                await getNamHoc()
                // window.location.reload()
            } else {
                swal("Đã hủy!");
            }
        });
    }
    const cloneNam = { ...showEdit };
    const update = (id, data) => {
        // console.log(id, data, 121212121)
        swal({
            title: "Bạn có chắc muốn thay đổi dữ liệu?",
            text: "Hãy Cân nhắc kỹ trước khi bạn làm việc này!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then(async (res) => {
            if (res) {
                swal("cập nhật thành công", {
                    icon: "success",
                    buttons: false
                });
                await NamHocApi.update(id, data)
                await getNamHoc();
                handleClose()
                // window.location.reload()
                // handleClose()
            } else {
                swal("Đã hủy!");
            }
        });

    }


    return (
        <div className="page-heading">
            <TitleBreadcrumb title="Quản lí năm học" />
            {
                showEdit ?
                    <Modal
                        show={showdetail}
                    // onHide={handleClose}
                    // backdrop="static"
                    // keyboard={false}
                    >
                        <Modal.Header closeButton>
                            <Modal.Title>chỉnh sửa năm</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <Form.Group className="mb-3" controlId="formGroupEmail">
                                <Form.Label>Tên năm</Form.Label>
                                <Form.Control type="text" placeholder="Năm bắt đầu VD: năm 2021-2022" defaultValue={showEdit.tenNam} onChange={(e) => cloneNam.tenNam = e.target.value} />
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="formGroupEmail">
                                <Form.Label>Năm bắt đầu</Form.Label>
                                <Form.Control type="number" min="2021" max="2099" step="1" placeholder="Năm bắt đầu VD: 2021" defaultValue={showEdit.namBatDau} onChange={(e) => cloneNam.namBatDau = e.target.value} />
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="formGroupEmail">
                                <Form.Label>Năm kết thúc</Form.Label>
                                <Form.Control type="number" min="2021" max="2099" step="1" placeholder="Năm kết thúc VD:2023" defaultValue={showEdit.namKetThuc} onChange={(e) => cloneNam.namKetThuc = e.target.value} />
                            </Form.Group>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button variant="secondary" onClick={() => setShowDetail(false)}>
                                Đóng
                            </Button>
                            <Button variant="primary" type="button" onClick={() => update(showEdit.id, cloneNam)}>Lưu</Button>
                        </Modal.Footer>
                    </Modal> : null
            }
            <section className="section ">
                <div className="card shadow-sm mb-3">
                    <div className="card-body">
                        <Button variant="primary" onClick={handleShow}>
                            Thêm năm
                        </Button>

                        <Modal show={show} onHide={handleClose}>
                            <Form onSubmit={handleSubmit(onSubmit)}>
                                <Modal.Header closeButton>
                                    <Modal.Title>Thêm năm</Modal.Title>
                                </Modal.Header>
                                <Modal.Body>
                                    <Form.Group className="mb-3" controlId="formGroupEmail">
                                        <Form.Label>Tên năm</Form.Label>
                                        <Form.Control type="text" placeholder="Năm bắt đầu VD: năm 2021-2022" {...register('nam', { required: true })} />
                                    </Form.Group>
                                    <Form.Group className="mb-3" controlId="formGroupEmail">
                                        <Form.Label>Năm bắt đầu</Form.Label>
                                        <Form.Control type="number" min="2021" max="2099" step="1" placeholder="Năm bắt đầu VD: 2021" {...register('namBatDau', { required: true })} />
                                    </Form.Group>
                                    <Form.Group className="mb-3" controlId="formGroupEmail">
                                        <Form.Label>Năm kết thúc</Form.Label>
                                        <Form.Control type="number" min="2021" max="2099" step="1" placeholder="Năm kết thúc VD:2023" {...register('namKetThuc', { required: true })} />
                                    </Form.Group>


                                </Modal.Body>
                                <Modal.Footer>
                                    <Button variant="secondary" onClick={handleClose}>
                                        Đóng
                                    </Button>
                                    <Button variant="primary" type="submit">
                                        Lưu
                                    </Button>
                                </Modal.Footer>
                            </Form>
                        </Modal>
                        <div className="container mt-3">


                            <Table >
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Tên năm</th>
                                        <th>Năm bắt đầu</th>
                                        <th>Năm kết thúc</th>
                                        <th>Hành động</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        namhoc?.map((e, i) => {
                                            console.log(e, 'nam')
                                            return (
                                                <tr key={i}>
                                                    <td>{i + 1}</td>
                                                    <td>{e.tenNam}</td>
                                                    <td>{e.namBatDau}</td>
                                                    <td>{e.namKetThuc}</td>
                                                    <td><Link style={{ margin: '10%' }} ><FiEdit onClick={() => Edit(e.id)} /></Link><Link><FiDelete onClick={() => Delete(e.id)} /></Link></td>
                                                </tr>
                                            )
                                        })
                                    }


                                </tbody>
                            </Table>
                        </div>
                    </div>
                </div>
            </section>
        </div>

    )
}
