import React, { useState, useEffect } from 'react';
import TitleBreadcrumb from '../common/TitleBreadcrumb';
import { InputGroup, Form } from 'react-bootstrap';
import { useForm } from "react-hook-form";
// import axios from 'axios';
// import { v4 as uuidv4 } from 'uuid';
import ThongBaoApi from '../../api/ThongBaoApi'
import { FiEdit, FiDelete } from 'react-icons/fi'
// import { IoMdAdd } from 'react-icons/io'
import { Link } from 'react-router-dom';
import { Button, Modal } from 'react-bootstrap';
import swal from 'sweetalert';
import CKEditor from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";
// import { Form, Input, TextArea, Button, Select } from 'semantic-ui-react'



const AddBangTin = (props) => {

    const [date, setDate] = useState("");

    // const onChange = (e) => {
    //     setDate(e.target.value)
    // }

    const [showEdit, setShowEdit] = useState();
    const [quantri, setQuanTri] = useState([]);

    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    // const [editorLoaded, setEditorLoaded] = useState(false);
    const [noidung, setNoiDung] = useState("");
    // useEffect(() => {
    //     setEditorLoaded(true);
    // }, []);


    const [option, setOption] = useState()
    const [nguoidang, setNguoiDang] = useState()
    const [ThongBao, setThongBao] = useState()
    // console.log('11111', ThongBao);
    const { register, handleSubmit, formState: { errors }, reset } = useForm();
    const onSubmit = (data) => {
        // console.log('add', { ...data, noidung, date, option, nguoidang })
        // console.log('option', typeof (option))

        swal({
            title: "Bạn có chắc muốn thêm thông báo này ?",
            text: "Hãy Cân nhắc kỹ trước khi bạn làm việc này!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then(async (res) => {
            // console.log('res', res)
            if (res) {
                swal("Thêm thành công", {
                    icon: "success",
                    buttons: false
                });
                await ThongBaoApi.add({
                    noiDung: noidung,
                    nguoiDang: nguoidang,
                    ngayDang: date,
                    danhMuc: option,
                    ...data
                }).then(() => {

                    reset();
                    setNoiDung('');
                });
                // console.log('add', addData);
                getThongBao()
                // window.location.reload()
            } else {
                swal("Đã hủy!");
                console.log(errors);

            }
        });

    }

    async function getThongBao() {
        let result = await ThongBaoApi.getThongBao();
        // console.log('re'result);
        setThongBao(result.data);
    }

    async function getQuanTriCreate() {
        let arr = []
        let result = await ThongBaoApi.getQuanTri();
        for (let i = 0; i <= result.data.length; i++) {
            let ND = result.data[i]?.maND.slice(0, 2)
            console.log('ND', ND);
            if (ND === "QT") {
                arr.push(result.data[i])
            }
        }
        setQuanTri(arr);
    }

    useEffect(() => {
        getThongBao()
        getQuanTriCreate()

    }, []);

    const Delete = (id) => {
        console.log('iddelete', typeof (id));
        swal({
            title: "Bạn có chắc muốn xóa dữ liệu này?",
            text: "Hãy Cân nhắc kỹ trước khi bạn làm việc này!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then(async (res) => {
            if (res) {
                swal("Xóa thành công", {
                    icon: "success",
                    buttons: false
                });
                await ThongBaoApi.remove(id);
                await getThongBao()

            } else {
                swal("Đã hủy!");
            }
        });

    }

    const getDate = (e) => {
        let valueDate = e.target.value;
        let d = new Date(valueDate);
        let formatted = `${d.getDate()}-${d.getMonth() + 1}-${d.getFullYear()}`;
        setDate(formatted)
    }

    const getDateEdit = (e) => {
        let d = new Date(e);
        let formatted = `${d.getDate()}-${d.getMonth() + 1}-${d.getFullYear()}`;
        return formatted;
    }

    // useEffect(async () => { setNoiDung(noidung) }, [noidung])
    async function Edit(id) {
        // console.log('idtb', id)
        let result = await ThongBaoApi.Edit(id);
        await setShowEdit(result.data);
        await setShow(true)

    }


    const cloneShowEdit = { ...showEdit };
    console.log('cloneShowEdit', cloneShowEdit)
    const updateThongBao = (id, data) => {
        // console.log('update', id, data);
        swal({
            title: "Bạn có chắc muốn thay đổi dữ liệu?",
            text: "Hãy Cân nhắc kỹ trước khi bạn làm việc này!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then(async (res) => {
            if (res) {
                // console.log('update', data);
                swal("cập nhật thành công", {
                    icon: "success",
                    buttons: false
                });
                await ThongBaoApi.update(id, data)

                setShowEdit(data);
                getThongBao()

            } else {
                swal("Đã hủy!");
            }
        });

    }

    const handleChange = (event) => {
        setOption(event.target.value)
    }
    const handleNguoiDang = (e) => {
        // console.log('HandlenguoiDang', e.target.value)
        setNguoiDang(e.target.value)
    }

    const formatDate = (date) => {
        const newdate = date.split("-").reverse().join("-");
        const datea = new Date(newdate);
        return `${datea.getFullYear()}-${(datea.getMonth() + 1) < 10 ? `0${datea.getMonth() + 1}` : datea.getMonth() + 1}-${datea.getDate() < 10 ? `0${datea.getDate()}` : datea.getDate()}`;
    }

    // const setDuyet = (e, id) => {

    //     ThongBao?.map((ThongBao, k) => ThongBao.id === id ? ThongBaoApi.add({ e, ThongBao.daDuyet = true }) : ThongBaoApi.add({ e, ThongBao.daDuyet = false }))
    //     // let checkedInput = e.target.value.checked;
    //     // console.log('checkedInput', checkedInput);
    // }
    return (
        <>
            <div key={props}>
                <Modal show={show} onHide={handleClose} animation={false} >
                    <Modal.Header >
                        <Modal.Title>Chỉnh sửa thông báo</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <InputGroup className="mb-3 ">
                            <InputGroup.Text id="basic-addon1">Tiêu đề:</InputGroup.Text>
                            <Form.Control key={showEdit?.tieuDe} defaultValue={showEdit?.tieuDe} onChange={(e) => { cloneShowEdit.tieuDe = e.target.value }}>
                            </Form.Control>
                        </InputGroup>
                        {/* <div className="input-group mb-3">
                            <span className="input-group-text" id="basic-addon1">Tiêu đề: </span>
                            <input className="form-control " key={showEdit?.tieuDe} defaultValue={showEdit?.tieuDe} onChange={(e) => { cloneShowEdit.tieuDe = e.target.value }} />
                        </div> */}
                        <InputGroup className="mb-3 ">
                            <InputGroup.Text id="basic-addon1">Tóm tắt: </InputGroup.Text>
                            <Form.Control key={showEdit?.tomTat} defaultValue={showEdit?.tomTat} onChange={(e) => { cloneShowEdit.tomTat = e.target.value }}>
                            </Form.Control>
                        </InputGroup>
                        {/* <div className="input-group mb-3">
                            <span className="input-group-text" id="basic-addon1">Tóm tắt: </span>
                            <input className="form-control " key={showEdit?.tomTat} defaultValue={showEdit?.tomTat} onChange={(e) => { cloneShowEdit.tomTat = e.target.value }} />
                        </div> */}

                        <InputGroup className="mb-3 ">
                            <InputGroup.Text id="basic-addon1">Loại thông tin</InputGroup.Text>
                            <Form.Control as="select" key={showEdit?.danhMuc} defaultValue={showEdit?.danhMuc} onChange={(e) => { cloneShowEdit.danhMuc = e.target.value }} >
                                <option value="0">--Chọn loại thông tin--</option>
                                <option value="Thông báo">Thông báo</option>
                                <option value="Hoạt động">Hoạt động</option>
                                <option value="Học phí">Học phí</option>
                            </Form.Control>
                        </InputGroup>
                        {/* <input className="form-control " key={showEdit?.LoaiThongTin} defaultValue={showEdit?.LoaiThongTin} onChange={(e)=>{cloneShowEdit.LoaiThongTin=e.target.value}}  /> */}
                        <InputGroup className="mb-3 ">
                            <InputGroup.Text id="basic-addon1">Ngày tạo: </InputGroup.Text>
                            {showEdit ? <Form.Control type='date' key={showEdit?.ngayDang} defaultValue={formatDate(showEdit?.ngayDang)} onChange={(e) => { cloneShowEdit.ngayDang = getDateEdit(e.target.value) }} /> : <></>}
                            {/* </Form.Control> */}
                        </InputGroup>
                        {/* <div className="input-group mb-3">
                            <span className="input-group-text" id="basic-addon1">Ngày tạo: </span>
                            {showEdit ? <input type='date' className="form-control" key={showEdit?.ngayDang} defaultValue={formatDate(showEdit?.ngayDang)} onChange={(e) => { cloneShowEdit.ngayDang = getDateEdit(e.target.value) }} /> : <></>}
                        </div> */}

                        <InputGroup className="mb-3 ">
                            <Form.Control as="select" key={showEdit?.nguoiDang} defaultValue={showEdit?.nguoiDang} onChange={(e) => { cloneShowEdit.nguoiDang = e.target.value }} >
                                <option value="0">--Chọn người đăng--</option>
                                {
                                    quantri.map((e, i) => {
                                        //console.log('nguoiDang', e._id) 
                                        return <option value={e._id} key={i}>{e.hoTen}</option>
                                    })
                                }

                            </Form.Control>
                        </InputGroup>
                        {/* <span className="input-group-text" id="basic-addon1">Người đăng: </span>
                            <input className="form-control " key={showEdit?.nguoiDang} defaultValue={showEdit?.nguoiDang} onChange={(e) => { cloneShowEdit.nguoiDang = e.target.value }} /> */}
                        {/* <InputGroup className="mb-3 ">
                            <Form.Control as="select" key={showEdit?.nguoiDang} defaultValue={showEdit?.nguoiDang} onChange={(e) => { cloneShowEdit.nguoiDang = e.target.value }} >
                                <option value="0">--Chọn người đăng--</option>
                                {
                                    quantri.map((e, i) => {
                                        //console.log('nguoiDang', e._id) 
                                        return <option value={e._id} key={i}>{e.hoTen}</option>
                                    })
                                }

                            </Form.Control>
                        </InputGroup> */}
                        <InputGroup className="mb-3 ">
                            <InputGroup.Text id="basic-addon1">Nội dung: </InputGroup.Text>
                            {
                                showEdit ? <CKEditor
                                    editor={ClassicEditor}
                                    key={showEdit?.noiDung}
                                    data={showEdit?.noiDung}
                                    onChange={(event, editor) => {
                                        const data = editor.getData();
                                        cloneShowEdit.noiDung = data;
                                    }}

                                /> : <></>
                            }
                        </InputGroup>
                        {/* <div className="input-group mb-3">
                            <div className="form-group w-100">
                                <span className="input-group-text" id="basic-addon1">Nội dung: </span>
                                {
                                    showEdit ? <CKEditor
                                        editor={ClassicEditor}
                                        key={showEdit?.noiDung}
                                        data={showEdit?.noiDung}
                                        onChange={(event, editor) => {
                                            const data = editor.getData();
                                            cloneShowEdit.noiDung = data;
                                        }}

                                    /> : <></>
                                }
                            </div>
                        </div> */}
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={handleClose}>
                            Hủy
                        </Button>
                        <Button variant="primary" type="button" onClick={() => { updateThongBao(showEdit._id, cloneShowEdit) }}>
                            Cập nhật
                        </Button>
                    </Modal.Footer>
                </Modal>


                <div className="page-heading" key={props}>
                    <TitleBreadcrumb title="Thêm Bảng Tin" />
                    <section className="section">
                        <div className="card shadow-sm mb-3">
                            <div className="card-body">
                                <div className="row">
                                    <Form onSubmit={handleSubmit(onSubmit)} >
                                        <div className="col-lg-12">
                                            <InputGroup className="mb-3 ">
                                                <InputGroup.Text id="basic-addon1">Tiêu đề</InputGroup.Text>
                                                <Form.Control placeholder="Thêm tiêu đề thông báo" aria-describedby="basic-addon1" {...register("tieuDe", { required: true })} />
                                            </InputGroup>
                                        </div>
                                        <div className="col-lg-6">
                                            <InputGroup className="mb-3 ">
                                                <InputGroup.Text id="basic-addon1">Danh mục</InputGroup.Text>
                                                <Form.Control as="select" onChange={handleChange} >
                                                    <option value="0">--Chọn danh mục--</option>
                                                    <option value="Thông báo">Thông báo</option>
                                                    <option value="Hoạt động">Hoạt động</option>
                                                    <option value="Học phí">Học phí</option>
                                                </Form.Control>
                                            </InputGroup>
                                            <InputGroup className="mb-3 ">

                                                <InputGroup.Text id="basic-addon1">Ngày đăng</InputGroup.Text>
                                                <Form.Control type="date" name="dob" onChange={(e) => getDate(e)} placeholder="Date of Birth" />

                                            </InputGroup>
                                            <InputGroup className="mb-3 ">
                                                <InputGroup.Text id="basic-addon1">Người đăng</InputGroup.Text>
                                                <Form.Control as="select" onChange={(e) => handleNguoiDang(e)} >
                                                    <option value="0">--Chọn người đăng--</option>
                                                    {
                                                        quantri.map((e, i) => {
                                                            return <option value={e._id} key={i}>{e.hoTen}</option>
                                                        })
                                                    }

                                                </Form.Control>
                                            </InputGroup>
                                            <InputGroup className="mb-3 ">
                                                <InputGroup.Text id="basic-addon1">Tóm tắt:</InputGroup.Text>
                                                <Form.Control placeholder="Thêm tóm tắt" aria-describedby="basic-addon1" {...register("tomTat", { required: true })} />
                                            </InputGroup>
                                        </div>
                                        <div className="col-lg-12">
                                            <Form.Group controlId="dob">
                                                <InputGroup.Text id="basic-addon1">Nội dung:</InputGroup.Text>
                                                <CKEditor
                                                    editor={ClassicEditor}
                                                    data={noidung}
                                                    onInit={editor => {
                                                        // console.log("Editor is ready to use!", editor);
                                                    }}

                                                    onChange={(event, editor) => {
                                                        const data = editor.getData();
                                                        // console.log(data, 'data')
                                                        setNoiDung(data)
                                                    }}

                                                />
                                            </Form.Group>
                                            {/* <span className="input-group-text" id="basic-addon1">Nội dung: </span> */}

                                        </div>
                                        <div className="modal-footer">
                                            <button type="submit" className="btn btn-primary">Lưu</button>
                                        </div>
                                    </Form>
                                    <div className="tableShow scrollStudent mt-5" key={props}>
                                        <table >
                                            <thead>
                                                <tr>
                                                    <th>STT</th>
                                                    <th>Tiêu đề</th>
                                                    <th>Tóm tắt</th>
                                                    <th>Loại tin</th>
                                                    <th>Người đăng</th>
                                                    <th>Ngày đăng</th>
                                                    <th>Nội dung</th>
                                                    <th>Hành động</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {
                                                    ThongBao?.map((e, k) => {
                                                        //console.log(1212121, e)
                                                        return (<tr key={k}>
                                                            <td>{k + 1}</td>
                                                            <td><textarea className="form-control" id="exampleFormControlTextarea1" rows="3" defaultValue={e.tieuDe} disabled={true} /></td>
                                                            <td><textarea className="form-control" id="exampleFormControlTextarea1" rows="3" defaultValue={e.tomTat} disabled={true} /></td>
                                                            <td>{e.danhMuc}</td>
                                                            <td>{e.nguoiDang.hoTen}</td>
                                                            <td>{e.ngayDang}</td>
                                                            <td><textarea className="form-control" id="exampleFormControlTextarea1" rows="3" defaultValue={e.noiDung} disabled={true} /></td>
                                                            <td><Link style={{ margin: '10%' }}><FiEdit onClick={() => Edit(e._id)} /></Link><Link><FiDelete onClick={() => Delete(e._id)} /></Link>
                                                                <Form>
                                                                    <Form.Check
                                                                        type="switch"
                                                                        id="custom-switch"
                                                                        label={e.daDuyet === true ? "Đã duyệt" : "Đang chờ duyệt"}
                                                                        onClick={(e) => e.target.value}
                                                                    />

                                                                </Form></td>
                                                        </tr>)
                                                    })
                                                }

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </>
    )
}

export default AddBangTin;