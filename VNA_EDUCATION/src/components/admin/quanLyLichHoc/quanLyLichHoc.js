import React, { useState, useEffect } from 'react'
import { Button } from 'react-bootstrap';
import TitleBreadcrumb from '../../common/TitleBreadcrumb';
// import LoadingFullPage from '../../common/LoadingFullPage';
import { Link, useParams } from 'react-router-dom'
import { FiEdit, FiDelete } from 'react-icons/fi'
// import { useForm } from "react-hook-form";
import swal from 'sweetalert';
import BuoiHocApi from '../../../api/BuoiHocApi';
import TuanHocApi from '../../../api/TuanHocApi';
import BackBtn from '../../common/BackBtn';
import ModalLichHoc from './ModalLichHoc';

export default function QuanLyThu() {
    const { idLH } = useParams()
    // const { register, handleSubmit } = useForm();

    const [show, setShow] = useState(false);
    const [thu, setThu] = useState([]);
    const [tuan, setTuan] = useState([]);
    // const [date, setDate] = useState([]);
    // const [dateUpdate, setDateUpdate] = useState([]);
    const [renderByWeek, setWeek] = useState([]);
    const [showdetail, setShowDetail] = useState(false);
    const [showEdit, setShowEdit] = useState();

    // const handleClose = () => setShow(false);
    // const handleShow = () => setShow(true);
    const handleShowEdit = (status) => setShowDetail(status);
    const handleShow = (status) => setShow(status);

    const onSubmit = (data) => {
        swal({
            title: "Bạn có chắc muốn tạo đánh giá này không ?",
            text: "Cân nhắc kỹ trước khi bạn thực hiện điều này!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then(async (res) => {
            if (res) {
                swal("Thêm thành công", {
                    icon: "success",
                    buttons: false
                });


                await BuoiHocApi.add(data)
                await getThu()
                // window.location.reload()
            } else {
                swal("Đã hủy!");
            }
        });
    }

    // const getDate = (e) => {
    //     let valueDate = e.target.value;
    //     let d = new Date(valueDate);
    //     let formatted = `${d.getDate() < 10 ? `0${d.getDate()}` : d.getDate()}-${(d.getMonth() + 1) < 10 ? `0${d.getMonth() + 1}` : d.getMonth() + 1}-${d.getFullYear()}`;
    //     setDate(formatted)
    // }
    // const getDateUpdate = (e) => {
    //     let valueDate = e.target.value;
    //     let d = new Date(valueDate);
    //     let formatted = `${d.getDate() < 10 ? `0${d.getDate()}` : d.getDate()}-${(d.getMonth() + 1) < 10 ? `0${d.getMonth() + 1}` : d.getMonth() + 1}-${d.getFullYear()}`;
    //     setDateUpdate(formatted)
    // }
    const getThu = async () => {
        let resuft = await BuoiHocApi.getBuoiHoc();
        setThu(resuft?.data)
    }

    const getTuan = async () => {
        let resuft = await TuanHocApi.getTuanHoc();
        setTuan(resuft.data)
    }

    const sortByWeek = (id) => {
        const result = thu?.filter(e => e.tuanHoc.soTuan == id);
        setWeek(result)

    }

    useEffect(() => {
        getThu();
        getTuan();
    }, [])

    useEffect(() => {
        setWeek(thu);
    }, [thu])

    //edit modal
    async function Edit(id) {
        let result = await BuoiHocApi.Edit(id);
        const data = result?.data;
        await setShowEdit({
            thu: data?.thu,
            tuanHoc: data?.tuanHoc?._id,
            ngayHoc: data?.ngayHoc
        });
        await setShowDetail(true)
    }

    const Delete = (id) => {
        // console.log('iddelete', id);
        swal({
            title: "Bạn có chắc muốn xóa dữ liệu này?",
            text: "Hãy Cân nhắc kỹ trước khi bạn làm việc này!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then(async (res) => {
            if (res) {
                swal("Xóa thành công", {
                    icon: "success",
                    buttons: false
                });
                await BuoiHocApi.remove(id);
                await getThu()
                await handleShow(false)

            } else {
                swal("Đã hủy!");
            }
        });

    }

    // const cloneThu = { ...showEdit };

    // const [inputUpdate, setInputUpdate] = useState({});
    // console.log(inputUpdate, 999)

    const update = (data) => {
        swal({
            title: "Bạn có chắc muốn thay đổi dữ liệu?",
            text: "Hãy Cân nhắc kỹ trước khi bạn làm việc này!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then(async (res) => {
            if (res) {
                swal("cập nhật thành công", {
                    icon: "success",
                    buttons: false
                });

                await BuoiHocApi.update(data?._id, data)
                await getThu();
                handleShowEdit(false)
                // window.location.reload()
                // handleClose()
            } else {
                swal("Đã hủy!");
            }
        });

    }
    // function setField(field, value) {
    //     setInputUpdate({
    //         ...inputUpdate,
    //         [field]: value
    //     })
    // }

    return (
        <div>
            <div className="page-heading">
                <TitleBreadcrumb title="Quản lí buổi học" />
                <BackBtn />






                <section className="section">
                    <div className="card shadow-sm mb-3">
                        <div className="card-body">
                            <select name="" id="" className="form-select mb-3" onChange={(e) => sortByWeek(e.target.value)}>
                                <option value="">Chọn tuần</option>
                                {
                                    tuan?.map((e, i) => {
                                        //console.log(e, 'tuan')
                                        return (
                                            <option value={e.soTuan}>{e.tenTuan}</option>
                                        )
                                    })
                                }
                            </select>

                            <Button variant="primary" onClick={handleShow}>
                                Thêm lịch học +
                            </Button>

                            {/* <Modal show={show} onHide={handleShow(false)}>
                                <Modal.Header closeButton>
                                    <Modal.Title>Tạo lịch học</Modal.Title>
                                </Modal.Header>
                                <Form onSubmit={handleSubmit(onSubmit)}>
                                    <Modal.Body>

                                        <Form.Group className="mb-3" controlId="formBasicEmail">
                                            <Form.Label>Buổi học</Form.Label>
                                            <select name="" id="" className="form-select mb-3" {...register('thu')} required>
                                                <option value="">chọn thứ</option>
                                                <option value="Thứ Hai">Thứ 2</option>
                                                <option value="Thứ Ba">Thứ 3</option>
                                                <option value="Thứ Tư">Thứ 4</option>
                                                <option value="Thứ Năm">Thứ 5</option>
                                                <option value="Thứ Sáu">Thứ 6</option>
                                                <option value="Thứ Bảy">Thứ 7</option>
                                                <option value="Chủ Nhật">Chủ nhật</option>
                                            </select>
                                            <Form.Group className="mb-3" controlId="formGroupEmail">
                                                <Form.Label>Tuần học</Form.Label>
                                                <select name="" id="" className="form-select mb-3" {...register('tuanHoc')} required >
                                                    <option value="">chọn tuần</option>
                                                    {
                                                        tuan?.map((e, i) => {
                                                            return (
                                                                <option value={e._id}>{e.tenTuan}</option>
                                                            )
                                                        })
                                                    }
                                                </select>
                                            </Form.Group>
                                            <Form.Group className="mb-3" controlId="formGroupEmail">
                                                <Form.Label>Ngày học</Form.Label>
                                                <Form.Control type="date" onChange={(e) => getDate(e)} required />
                                            </Form.Group>
                                        </Form.Group>
                                    </Modal.Body>
                                    <Modal.Footer>
                                        <Button variant="danger" onClick={handleClose}>
                                            Đóng
                                        </Button>
                                        <Button variant="success" type="submit">
                                            Lưu
                                        </Button>
                                    </Modal.Footer>
                                </Form>
                            </Modal> */}




                            <table id="mytable" className="table mt-3">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Buổi học</th>
                                        <th>Tuần học</th>
                                        <th>Ngày học</th>
                                        <th colSpan="3">Hành động</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        renderByWeek.length > 0 ?
                                            renderByWeek?.map((e, i) => {
                                                {/* console.log(e, 'week') */ }
                                                return (
                                                    <tr key={i} style={{ borderBottom: "6px solid #eceffa" }}>
                                                        <td>{i + 1}</td>
                                                        <td>{e.thu}</td>
                                                        <td>{`Tuần ${e.tuanHoc.soTuan}`}</td>
                                                        <td>{e.ngayHoc}</td>
                                                        <td><Link style={{ margin: '10%' }} onClick={() => Edit(e._id)} ><FiEdit /></Link><Link><FiDelete onClick={() => Delete(e._id)} /></Link></td>
                                                        <td><Link to={`/quan-ly/quan-li-lich-hoc/quan-ly-lop/${idLH}/${e._id}`}>Quản lí tiêt học</Link></td>

                                                    </tr>
                                                )
                                            }) : <tr rowSpan="5"><td colSpan="5" className="text-center">  Đang cập nhật</td></tr>
                                    }

                                </tbody>
                            </table>
                        </div>
                        <ModalLichHoc show={show} tittle="Tạo buổi học" handleClose={handleShow} action={onSubmit} initialValue={{}} />
                        <ModalLichHoc show={showdetail} tittle="Chỉnh sửa buổi học" handleClose={handleShowEdit} action={update} initialValue={showEdit} />
                    </div>
                </section>
            </div>

        </div>
    )
}
