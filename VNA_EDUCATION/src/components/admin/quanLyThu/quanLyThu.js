// import React, { useState, useEffect } from 'react'
// import { Button, Modal, InputGroup, Form, Dropdown, DropdownButton } from 'react-bootstrap';
// import TitleBreadcrumb from '../../common/TitleBreadcrumb';
// import { Link } from 'react-router-dom'
// import { FiEdit, FiDelete } from 'react-icons/fi'
// import axios from 'axios';
// import { useForm } from "react-hook-form";
// import swal from 'sweetalert';

// export default function QuanLyThu() {
//     const { register, handleSubmit, formState: { errors } } = useForm();

//     const [show, setShow] = useState(false);
//     const [thu, setThu] = useState([]);
//     const [tuan, setTuan] = useState([]);
//     const [date, setDate] = useState([]);
//     const [showdetail, setShowDetail] = useState(false);
//     const [showEdit, setShowEdit] = useState();
//     console.log(showEdit, 'showEdit');

//     const handleClose = () => setShow(false);
//     const handleShow = () => setShow(true);

//     const onSubmit = (data) => {
//         // console.log(data.idTh, 'data')
//         swal({
//             title: "Bạn có chắc muốn tạo đánh giá này không ?",
//             text: "Cân nhắc kỹ trước khi bạn thực hiện điều này!",
//             icon: "warning",
//             buttons: true,
//             dangerMode: true,
//         }).then(async (res) => {
//             if (res) {
//                 swal("Thêm thành công", {
//                     icon: "success",
//                     buttons: false
//                 });

//                 await axios.post('http://localhost:1997/thu', { ...data, ngayTao: date })
//                 await getThu()
//                 // window.location.reload()
//             } else {
//                 swal("Đã hủy!");
//             }
//         });
//     }
//     const getDate = (e) => {
//         let valueDate = e.target.value;
//         let d = new Date(valueDate);
//         let formatted = `${d.getDate()}-${d.getMonth() + 1}-${d.getFullYear()}`;
//         setDate(formatted)
//     }

//     const getThu = async () => {
//         let resuft = await axios.get('http://localhost:1997/thu');
//         await setThu(resuft.data)
//     }
//     const getTuan = async () => {
//         let resuft = await axios.get('http://localhost:1998/tuan-hoc');
//         await setTuan(resuft.data)
//     }

//     useEffect(() => {
//         getThu();
//         getTuan();
//     }, [])

//     //edit modal
//     async function Edit(id) {
//         let result = await axios.get(`http://localhost:1997/thu/${id}`);
//         await setShowEdit(result.data);
//         await setShowDetail(true)
//     }

//     const Delete = (id) => {
//         // console.log('iddelete', id);
//         swal({
//             title: "Bạn có chắc muốn xóa dữ liệu này?",
//             text: "Hãy Cân nhắc kỹ trước khi bạn làm việc này!",
//             icon: "warning",
//             buttons: true,
//             dangerMode: true,
//         }).then(async (res) => {
//             if (res) {
//                 swal("Xóa thành công", {
//                     icon: "success",
//                     buttons: false
//                 });
//                 await axios.delete(`http://localhost:1997/thu/${id}`);
//                 await getThu()
//                 await handleClose()

//             } else {
//                 swal("Đã hủy!");
//             }
//         });

//     }

//     const cloneThu = { ...showEdit };


//     const update = (id, data) => {
//         // console.log(id, data, 121212121)
//         swal({
//             title: "Bạn có chắc muốn thay đổi dữ liệu?",
//             text: "Hãy Cân nhắc kỹ trước khi bạn làm việc này!",
//             icon: "warning",
//             buttons: true,
//             dangerMode: true,
//         }).then(async (res) => {
//             if (res) {
//                 swal("cập nhật thành công", {
//                     icon: "success",
//                     buttons: false
//                 });
//                 await axios.patch(`http://localhost:1997/thu/${id}`, data)
//                 await getThu();
//                 handleClose()
//                 // window.location.reload()
//                 // handleClose()
//             } else {
//                 swal("Đã hủy!");
//             }
//         });

//     }
//     return (
//         <div>
//             <div className="page-heading">
//                 <TitleBreadcrumb title="Quản lí thứ" />


//                 {
//                     showEdit ?
//                         <Modal
//                             show={showdetail}
//                         // onHide={handleClose}
//                         // backdrop="static"
//                         // keyboard={false}
//                         >
//                             <Modal.Header closeButton>
//                                 <Modal.Title>chỉnh sửa thứ</Modal.Title>
//                             </Modal.Header>
//                             <Modal.Body>
//                                 <Form.Group className="mb-3" controlId="formBasicEmail">
//                                     <Form.Label>Thứ</Form.Label>
//                                     <select className="form-select mb-3" defaultValue={showEdit.thu} onChange={(e) => cloneThu.thu = e.target.value}>
//                                         <option>chọn thứ</option>
//                                         <option value="Thứ 2">Thứ 2</option>
//                                         <option value="Thứ 3">Thứ 3</option>
//                                         <option value="Thứ 4">Thứ 4</option>
//                                         <option value="Thứ 5">Thứ 5</option>
//                                         <option value="Thứ 6">Thứ 6</option>
//                                         <option value="Thứ 7">Thứ 7</option>
//                                         <option value="Chủ nhật">Chủ nhật</option>
//                                         {/* {
//                                                     thu?.map((e, i) => {
//                                                         return (
//                                                             <option value={e.id}>{e.thu}</option>
//                                                         )
//                                                     })
//                                                 } */}


//                                     </select>
//                                     <Form.Group className="mb-3" controlId="formGroupEmail">
//                                         <Form.Label>Tuần</Form.Label>
//                                         <select name="" id="" className="form-select mb-3" defaultValue={showEdit.tuan} onChange={(e) => cloneThu.tuan = e.target.value}>
//                                             <option value="">chọn tuần</option>
//                                             {
//                                                 tuan?.map((e, i) => {
//                                                     return (
//                                                         <option value={e.tuan} key={i}>{e.tuan}</option>
//                                                     )
//                                                 })
//                                             }


//                                         </select>
//                                     </Form.Group>
//                                     <Form.Group className="mb-3" controlId="formGroupEmail">
//                                         {/* <Form.Control type="text" placeholder="nhập số lượng thứ" /> */}
//                                         <Form.Label>Ngày tạo</Form.Label>
//                                         <Form.Control type="date" defaultValue={showEdit.ngayTao} onChange={(e) => cloneThu.ngayTao = e.target.value} />
//                                     </Form.Group>

//                                 </Form.Group>
//                             </Modal.Body>
//                             <Modal.Footer>
//                                 <Button variant="secondary" onClick={() => setShowDetail(false)}>
//                                     Đóng
//                         </Button>
//                                 <Button variant="primary" type="button" onClick={() => update(showEdit.id, cloneThu)}>Lưu</Button>
//                             </Modal.Footer>
//                         </Modal> : null
//                 }





//                 <section className="section ">
//                     <div className="card shadow-sm mb-3">
//                         <div className="card-body">

//                             <Button variant="primary" onClick={handleShow}>
//                                 Thêm thứ +
//                         </Button>
// ``
//                             <Modal show={show} onHide={handleClose}>
//                                 <Modal.Header closeButton>
//                                     <Modal.Title>Tạo thứ</Modal.Title>
//                                 </Modal.Header>
//                                 <Form onSubmit={handleSubmit(onSubmit)}>
//                                     <Modal.Body>

//                                         <Form.Group className="mb-3" controlId="formBasicEmail">
//                                             <Form.Label>Thứ</Form.Label>
//                                             <select name="" id="" className="form-select mb-3" {...register('thu')} required>
//                                                 <option value="">chọn thứ</option>
//                                                 <option value="Thứ 2">Thứ 2</option>
//                                                 <option value="Thứ 3">Thứ 3</option>
//                                                 <option value="Thứ 4">Thứ 4</option>
//                                                 <option value="Thứ 5">Thứ 5</option>
//                                                 <option value="Thứ 6">Thứ 6</option>
//                                                 <option value="Thứ 7">Thứ 7</option>
//                                                 <option value="Chủ nhật">Chủ nhật</option>
//                                                 {/* {
//                                                     thu?.map((e, i) => {
//                                                         return (
//                                                             <option value={e.id}>{e.thu}</option>
//                                                         )
//                                                     })
//                                                 } */}


//                                             </select>
//                                             <Form.Group className="mb-3" controlId="formGroupEmail">
//                                                 <Form.Label>Tuần</Form.Label>
//                                                 <select name="" id="" className="form-select mb-3" {...register('tuan')} required >
//                                                     <option value="">chọn tuần</option>
//                                                     {
//                                                         tuan?.map((e, i) => {
//                                                             return (
//                                                                 <option value={e.tuan} key={i}>{e.tuan}</option>
//                                                             )
//                                                         })
//                                                     }


//                                                 </select>
//                                             </Form.Group>
//                                             <Form.Group className="mb-3" controlId="formGroupEmail">
//                                                 {/* <Form.Control type="text" placeholder="nhập số lượng thứ" /> */}
//                                                 <Form.Label>Ngày tạo</Form.Label>
//                                                 <Form.Control type="date" onChange={(e) => getDate(e)} required />
//                                             </Form.Group>

//                                         </Form.Group>


//                                     </Modal.Body>
//                                     <Modal.Footer>
//                                         <Button variant="secondary" onClick={handleClose}>
//                                             Đóng
//                         </Button>
//                                         <Button variant="primary" type="submit">
//                                             Lưu
//                         </Button>
//                                     </Modal.Footer>
//                                 </Form>
//                             </Modal>
//                             {/* </ddiv className="tableShow scrollStudent mt-5 table-responsive"> */}
//                             <table className="mt-3">
//                                 <thead>
//                                     <tr>
//                                         <th>STT</th>
//                                         <th>Thứ</th>
//                                         <th>Tuần</th>
//                                         <th>Ngày</th>
//                                         <th colSpan="3">Hành động</th>

//                                     </tr>
//                                 </thead>
//                                 <tbody>
//                                     {
//                                         thu.map((e, i) => {
//                                             return (
//                                                 <tr key={i}>
//                                                     <td>{i}</td>
//                                                     <td>{e.thu}</td>
//                                                     <td>{e.tuan}</td>
//                                                     <td>{e.ngayTao}</td>
//                                                     <td><Link style={{ margin: '10%' }} onClick={() => Edit(e.id)} ><FiEdit /></Link><Link><FiDelete onClick={() => Delete(e.id)} /></Link></td>
//                                                     <td><Link to="/quan-ly/quan-li-lop-hoc/quan-ly-lop">Quản lí lớp</Link></td>

//                                                 </tr>
//                                             )
//                                         })
//                                     }





//                                 </tbody>
//                             </table>
//                         </div>
//                     </div>
//                 </section>
//             </div>

//         </div>
//     )
// }
