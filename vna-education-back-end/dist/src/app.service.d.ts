import { MailerService } from '@nestjs-modules/mailer';
import { ChangePassDTO, SetPassDTO } from './helpers/changePass.dto';
import { BuoiHocService } from './models/buoi-hoc/buoi-hoc.service';
import { DanhGiaService } from './models/danh-gia/danh-gia.service';
import { ThongKeService } from './models/danh-gia/roles/thongKe.service';
import { LopHocService } from './models/lop-hoc/lop-hoc.service';
import { NamHocService } from './models/nam-hoc/nam-hoc.service';
import { AccountService } from './models/nguoi-dung/actions/account.service';
import { NguoiDungService } from './models/nguoi-dung/nguoi-dung.service';
import { QuenMatKhauService } from './models/quen-mat-khau/quen-mat-khau.service';
import { TietHocService } from './models/tiet-hoc/tiet-hoc.service';
import { TuanHocService } from './models/tuan-hoc/tuan-hoc.service';
export declare class AppService {
    private readonly ndSer;
    private readonly lhSer;
    private readonly tuanSer;
    private readonly bhSer;
    private readonly thSer;
    private readonly namSer;
    private readonly dgSer;
    private readonly qmkSer;
    private readonly mailSer;
    private readonly tkSer;
    private readonly accSer;
    constructor(ndSer: NguoiDungService, lhSer: LopHocService, tuanSer: TuanHocService, bhSer: BuoiHocService, thSer: TietHocService, namSer: NamHocService, dgSer: DanhGiaService, qmkSer: QuenMatKhauService, mailSer: MailerService, tkSer: ThongKeService, accSer: AccountService);
    kiemTra_dangNhap(username: string, password: string): Promise<{
        id: any;
        dangHD: true;
        resOK: boolean;
    } | {
        id: any;
        dangHD: boolean;
        resOK: boolean;
    }>;
    thongTin_nguoiDung(user: string): Promise<{
        maND: string;
        hoTen: string;
        emailND: string;
        diaChi: string;
        ngaySinh: string;
        noiSinh: string;
        gioiTinh: string;
        soDienThoai: string;
        dangHoatDong: boolean;
        quocTich: string;
        danToc: string;
        cccd: {
            maSo: string;
            ngayCap: string;
            noiCap: string;
        };
        hoChieu: {
            maSo: string;
            ngayCap: string;
            noiCap: string;
        };
        hocTap: {
            idLop: any;
            ngayNhapHoc: string;
            GVCN: string;
            lopHoc: string;
        };
        chuNhiem: string;
        chucVu: {
            chucVu: string;
            hopDong: string;
            trinhDo: string;
        };
    }>;
    taoLichHoc(tuan: string, lop: string): Promise<{
        lopHoc: string;
        buoiHoc: any[];
        _id: any;
        soTuan: number;
        tenTuan: string;
        ngayBatDau: string;
        ngayKetThuc: string;
        hocKy: number;
    }>;
    taoLichDay(tuan: string, gv: string): Promise<{
        buoiHoc: any[];
        _id: any;
        soTuan: number;
        tenTuan: string;
        ngayBatDau: string;
        ngayKetThuc: string;
        hocKy: number;
    }>;
    doiMatKhau(dto: ChangePassDTO): Promise<{
        msg: string;
        checkOK: boolean;
    }>;
    datMoi_matKhau(dto: SetPassDTO): Promise<{
        msg: string;
        checkOK: boolean;
    }>;
    guiMail_quenMatKhau(email: string): Promise<string>;
    thongKe(): Promise<any>;
}
