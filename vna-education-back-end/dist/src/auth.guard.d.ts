import { CanActivate, ExecutionContext } from '@nestjs/common';
import { NguoiDungService } from './models/nguoi-dung/nguoi-dung.service';
export declare class AuthGuard implements CanActivate {
    private readonly ndSer;
    constructor(ndSer: NguoiDungService);
    canActivate(context: ExecutionContext): Promise<boolean>;
}
