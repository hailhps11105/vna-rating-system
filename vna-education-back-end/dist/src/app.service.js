"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppService = void 0;
const mailer_1 = require("@nestjs-modules/mailer");
const common_1 = require("@nestjs/common");
const bcrypt_1 = require("bcrypt");
const utilities_1 = require("./helpers/utilities");
const buoi_hoc_service_1 = require("./models/buoi-hoc/buoi-hoc.service");
const danh_gia_service_1 = require("./models/danh-gia/danh-gia.service");
const thongKe_service_1 = require("./models/danh-gia/roles/thongKe.service");
const lop_hoc_service_1 = require("./models/lop-hoc/lop-hoc.service");
const nam_hoc_service_1 = require("./models/nam-hoc/nam-hoc.service");
const account_service_1 = require("./models/nguoi-dung/actions/account.service");
const nguoi_dung_service_1 = require("./models/nguoi-dung/nguoi-dung.service");
const quen_mat_khau_service_1 = require("./models/quen-mat-khau/quen-mat-khau.service");
const tiet_hoc_service_1 = require("./models/tiet-hoc/tiet-hoc.service");
const tuan_hoc_service_1 = require("./models/tuan-hoc/tuan-hoc.service");
let AppService = class AppService {
    ndSer;
    lhSer;
    tuanSer;
    bhSer;
    thSer;
    namSer;
    dgSer;
    qmkSer;
    mailSer;
    tkSer;
    accSer;
    constructor(ndSer, lhSer, tuanSer, bhSer, thSer, namSer, dgSer, qmkSer, mailSer, tkSer, accSer) {
        this.ndSer = ndSer;
        this.lhSer = lhSer;
        this.tuanSer = tuanSer;
        this.bhSer = bhSer;
        this.thSer = thSer;
        this.namSer = namSer;
        this.dgSer = dgSer;
        this.qmkSer = qmkSer;
        this.mailSer = mailSer;
        this.tkSer = tkSer;
        this.accSer = accSer;
    }
    async kiemTra_dangNhap(username, password) {
        const user = await this.accSer.getOne_bymaND(username);
        const pass = await this.accSer.onlyPassword(username);
        if (user && pass) {
            if ((await bcrypt_1.compare(password, pass)) && user.dangHoatDong)
                return {
                    id: user._id,
                    dangHD: user.dangHoatDong,
                    resOK: true,
                };
            else
                return {
                    id: null,
                    dangHD: user.dangHoatDong,
                    resOK: false,
                };
        }
        else {
            return {
                id: null,
                dangHD: user.dangHoatDong,
                resOK: false,
            };
        }
    }
    async thongTin_nguoiDung(user) {
        return await this.ndSer.getOne_byID(user);
    }
    async taoLichHoc(tuan, lop) {
        const week = await this.tuanSer.findOne(tuan);
        const classe = await this.lhSer.findOne(lop);
        const buoi = await this.bhSer.getAll({ tuanHoc: Object(tuan) });
        const tiet = await this.thSer.getAll({ lopHoc: Object(lop) });
        const temp = [];
        const result = { ...week, lopHoc: classe.maLH, buoiHoc: [] };
        for (let i = 0; i < buoi.length; i++) {
            const { tuanHoc, ...b } = buoi[i];
            temp.push({ ...b, tietHoc: [] });
        }
        for (let j = 0; j < temp.length; j++) {
            for (let k = 0; k < tiet.length; k++) {
                if (tiet[k].buoiHoc.thu === temp[j].thu &&
                    tiet[k].buoiHoc.ngayHoc === temp[j].ngayHoc) {
                    const { lopHoc, buoiHoc, ...t } = tiet[k];
                    temp[j].tietHoc.push(t);
                }
            }
        }
        for (let l = temp.length - 1; l >= 0; l--) {
            if (temp[l].tietHoc.length === 0)
                temp.splice(l, 1);
        }
        for (let m = 0; m < temp.length; m++) {
            temp[m].tietHoc = temp[m].tietHoc.sort((a, b) => {
                return utilities_1.sessionSort(a.thuTiet, b.thuTiet);
            });
        }
        result.buoiHoc = temp;
        result.buoiHoc.sort((a, b) => {
            return utilities_1.weekdaySort(a.thu, b.thu);
        });
        return result;
    }
    async taoLichDay(tuan, gv) {
        const week = await this.tuanSer.findOne(tuan);
        const buoi = await this.bhSer.getAll({ tuanHoc: Object(tuan) });
        const tiet = await this.thSer.getAll({ giaoVien: Object(gv) });
        const temp = [];
        const result = { ...week, buoiHoc: [] };
        for (let i = 0; i < buoi.length; i++) {
            const { tuanHoc, ...b } = buoi[i];
            temp.push({ ...b, tietHoc: [] });
        }
        for (let j = 0; j < temp.length; j++) {
            for (let k = 0; k < tiet.length; k++) {
                if (tiet[k].buoiHoc.thu === temp[j].thu &&
                    tiet[k].buoiHoc.ngayHoc === temp[j].ngayHoc) {
                    const { giaoVien, buoiHoc, ...t } = tiet[k];
                    temp[j].tietHoc.push(t);
                }
            }
        }
        for (let l = temp.length - 1; l >= 0; l--) {
            if (temp[l].tietHoc.length === 0)
                temp.splice(l, 1);
        }
        for (let m = 0; m < temp.length; m++) {
            temp[m].tietHoc = temp[m].tietHoc.sort((a, b) => {
                return utilities_1.sessionSort(a.thuTiet, b.thuTiet);
            });
        }
        result.buoiHoc = temp;
        result.buoiHoc.sort((a, b) => {
            return utilities_1.weekdaySort(a.thu, b.thu);
        });
        return result;
    }
    async doiMatKhau(dto) {
        return await this.accSer.changePass(dto);
    }
    async datMoi_matKhau(dto) {
        return await this.accSer.setPass(dto);
    }
    async guiMail_quenMatKhau(email) {
        const user = await this.accSer.findOne_byEmail(email);
        const toCreate = await this.qmkSer.create({
            emailND: email,
            nguoiDung: user._id,
            conHan: true,
        });
        return this.mailSer
            .sendMail({
            to: email,
            from: '"VNA Education" - vna-568a20@inbox.mailtrap.io',
            subject: 'Xác nhận đổi mật khẩu - VNA Education',
            html: `<a href="http://localhost:3000/doi-mat-khau/${toCreate._id}.${user._id}">Link vào trang đổi mật khẩu</a>`,
        })
            .then(() => {
            return 'Gửi mail thành công';
        })
            .catch((err) => {
            return 'Lỗi ' + err;
        });
    }
    async thongKe() {
        const { tuanHoc, ...recentYear } = await this.namSer.getLatest();
        const result = { ...recentYear, tuanHoc: [] };
        for (let i = 0; i < tuanHoc.length; i++) {
            result.tuanHoc.push({
                _id: tuanHoc[i]._id,
                tenTuan: tuanHoc[i].tenTuan,
                soTuan: tuanHoc[i].soTuan,
                thongKe: {
                    tongSo_hocSinh: (await this.ndSer.findAll_byRole('HS'))
                        .length,
                    tongSo_giaoVien: (await this.ndSer.findAll_byRole('GV'))
                        .length,
                    tongSo_danhGia: (await this.dgSer.findAll({
                        tuanDG: Object(tuanHoc[i]._id),
                    })).length,
                    giaoVien_diemDG: await this.tkSer.getAll_byWeek(tuanHoc[i]._id),
                },
            });
        }
        return result;
    }
};
AppService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [nguoi_dung_service_1.NguoiDungService,
        lop_hoc_service_1.LopHocService,
        tuan_hoc_service_1.TuanHocService,
        buoi_hoc_service_1.BuoiHocService,
        tiet_hoc_service_1.TietHocService,
        nam_hoc_service_1.NamHocService,
        danh_gia_service_1.DanhGiaService,
        quen_mat_khau_service_1.QuenMatKhauService,
        mailer_1.MailerService,
        thongKe_service_1.ThongKeService,
        account_service_1.AccountService])
], AppService);
exports.AppService = AppService;
//# sourceMappingURL=app.service.js.map