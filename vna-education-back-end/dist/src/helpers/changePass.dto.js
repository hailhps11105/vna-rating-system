"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SetPassDTO = exports.ChangePassDTO = void 0;
const openapi = require("@nestjs/swagger");
class ChangePassDTO {
    idUser;
    oldPass;
    newPass;
    static _OPENAPI_METADATA_FACTORY() {
        return { idUser: { required: true, type: () => String }, oldPass: { required: true, type: () => String }, newPass: { required: true, type: () => String } };
    }
}
exports.ChangePassDTO = ChangePassDTO;
class SetPassDTO {
    idUser;
    newPass;
    static _OPENAPI_METADATA_FACTORY() {
        return { idUser: { required: true, type: () => String }, newPass: { required: true, type: () => String } };
    }
}
exports.SetPassDTO = SetPassDTO;
//# sourceMappingURL=changePass.dto.js.map