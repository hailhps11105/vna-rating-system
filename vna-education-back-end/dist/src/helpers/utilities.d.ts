import { Types } from 'mongoose';
export declare function bulkObjectID(target: string[]): Types.ObjectId[];
export declare type RoleType = 'HS' | 'PH' | 'GV' | 'QT' | 'HT' | 'QT-HT';
export declare function assign(dto: any, doc: any): void;
export declare function removeDuplicates(arr: any, key: string): any[];
export declare function arrange(date: string): Date;
export declare function weekdaySort(a: string, b: string): number;
export declare function sessionSort(a: string, b: string): number;
export declare function average(arr: number[]): number;
