export declare class ChangePassDTO {
    idUser: string;
    oldPass: string;
    newPass: string;
}
export declare class SetPassDTO {
    idUser: string;
    newPass: string;
}
