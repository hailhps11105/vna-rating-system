"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.average = exports.sessionSort = exports.weekdaySort = exports.arrange = exports.removeDuplicates = exports.assign = exports.bulkObjectID = void 0;
const mongoose_1 = require("mongoose");
function bulkObjectID(target) {
    const result = [];
    for (let i = 0; i < target.length; i++) {
        result.push(mongoose_1.Types.ObjectId(target[i]));
    }
    return result;
}
exports.bulkObjectID = bulkObjectID;
function assign(dto, doc) {
    for (const key in dto) {
        if (Object.prototype.hasOwnProperty.call(dto, key)) {
            doc[key] = dto[key];
        }
    }
}
exports.assign = assign;
function removeDuplicates(arr, key) {
    const flag = {};
    const unique = [];
    for (let i = 0; i < arr.length; i++) {
        if (!flag[arr[i][key]]) {
            flag[arr[i][key]] = true;
            unique.push(arr[i]);
        }
    }
    return unique;
}
exports.removeDuplicates = removeDuplicates;
function arrange(date) {
    const dat = date.trim().split('-').reverse().join('-');
    return new Date(dat);
}
exports.arrange = arrange;
function weekdaySort(a, b) {
    const weekDays = [
        'Thứ Hai',
        'Thứ Ba',
        'Thứ Tư',
        'Thứ Năm',
        'Thứ Sáu',
        'Thứ Bảy',
        'Chủ Nhật',
    ];
    let aVal = 0, bVal = 0;
    for (let i = 0; i < weekDays.length; i++) {
        if (a === weekDays[i])
            aVal = i;
        if (b === weekDays[i])
            bVal = i;
    }
    return aVal - bVal;
}
exports.weekdaySort = weekdaySort;
function sessionSort(a, b) {
    const sessions = [
        'Tiết 1',
        'Tiết 2',
        'Tiết 3',
        'Tiết 4',
        'Tiết 5',
        'Tiết 6',
        'Tiết 7',
        'Tiết 8',
    ];
    let aVal = 0, bVal = 0;
    for (let i = 0; i < sessions.length; i++) {
        if (a === sessions[i])
            aVal = i;
        if (b === sessions[i])
            bVal = i;
    }
    return aVal - bVal;
}
exports.sessionSort = sessionSort;
function average(arr) {
    let temp = 0;
    if (arr.length === 0)
        return 0;
    else {
        for (let i = 0; i < arr.length; i++) {
            temp += arr[i];
        }
        return temp / arr.length;
    }
}
exports.average = average;
//# sourceMappingURL=utilities.js.map