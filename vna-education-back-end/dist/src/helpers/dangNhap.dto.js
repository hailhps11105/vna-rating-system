"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.KetQua_DangNhap = exports.DangNhapDTO = void 0;
const openapi = require("@nestjs/swagger");
const swagger_1 = require("@nestjs/swagger");
class DangNhapDTO {
    username;
    password;
    static _OPENAPI_METADATA_FACTORY() {
        return { username: { required: true, type: () => String }, password: { required: true, type: () => String } };
    }
}
__decorate([
    swagger_1.ApiProperty({
        name: 'username',
        description: 'Mã người dùng, dùng để đăng nhập',
        type: String,
        example: 'PH420',
    }),
    __metadata("design:type", String)
], DangNhapDTO.prototype, "username", void 0);
__decorate([
    swagger_1.ApiProperty({
        name: 'password',
        description: 'Mật khẩu đăng nhập',
        type: String,
        example: '123456',
    }),
    __metadata("design:type", String)
], DangNhapDTO.prototype, "password", void 0);
exports.DangNhapDTO = DangNhapDTO;
class KetQua_DangNhap {
    id;
    resOk;
    static _OPENAPI_METADATA_FACTORY() {
        return { id: { required: true, type: () => String }, resOk: { required: true, type: () => Boolean } };
    }
}
__decorate([
    swagger_1.ApiProperty({
        name: 'id',
        description: '_id của người dùng vừa đăng nhập trong CSDL',
        type: String,
        example: '60bxxxxxxxxxxxxxxxxxxxxx',
    }),
    __metadata("design:type", String)
], KetQua_DangNhap.prototype, "id", void 0);
__decorate([
    swagger_1.ApiProperty({
        name: 'resOk',
        description: 'Trạng thái đăng nhập',
        type: Boolean,
    }),
    __metadata("design:type", Boolean)
], KetQua_DangNhap.prototype, "resOk", void 0);
exports.KetQua_DangNhap = KetQua_DangNhap;
//# sourceMappingURL=dangNhap.dto.js.map