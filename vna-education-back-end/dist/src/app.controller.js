"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppController = void 0;
const openapi = require("@nestjs/swagger");
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const app_service_1 = require("./app.service");
const auth_guard_1 = require("./auth.guard");
const changePass_dto_1 = require("./helpers/changePass.dto");
const dangNhap_dto_1 = require("./helpers/dangNhap.dto");
const sendMail_dto_1 = require("./helpers/sendMail.dto");
let AppController = class AppController {
    service;
    constructor(service) {
        this.service = service;
    }
    index() {
        return null;
    }
    async dangNhap(dto) {
        return await this.service.kiemTra_dangNhap(dto.username, dto.password);
    }
    async layLichHoc(tuan, lop) {
        return await this.service.taoLichHoc(tuan, lop);
    }
    async layLichDay(tuan, gv) {
        return await this.service.taoLichDay(tuan, gv);
    }
    async thongTin(user) {
        if (user && user != '')
            return await this.service.thongTin_nguoiDung(user);
    }
    async doiMatKhau(dto) {
        return await this.service.doiMatKhau(dto);
    }
    async datMatKhau(dto) {
        return await this.service.datMoi_matKhau(dto);
    }
    async guiEmail(toSend) {
        return await this.service.guiMail_quenMatKhau(toSend.receiver);
    }
    async thongKe() {
        return await this.service.thongKe();
    }
};
__decorate([
    common_1.Get(),
    common_1.Render('index'),
    openapi.ApiResponse({ status: 200, type: Object }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], AppController.prototype, "index", null);
__decorate([
    common_1.Post('dang-nhap'),
    openapi.ApiResponse({ status: 201, type: Object }),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dangNhap_dto_1.DangNhapDTO]),
    __metadata("design:returntype", Promise)
], AppController.prototype, "dangNhap", null);
__decorate([
    common_1.Get('lich-hoc'),
    common_1.UseGuards(auth_guard_1.AuthGuard),
    openapi.ApiResponse({ status: 200 }),
    __param(0, common_1.Query('tuan')),
    __param(1, common_1.Query('lop')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String]),
    __metadata("design:returntype", Promise)
], AppController.prototype, "layLichHoc", null);
__decorate([
    common_1.Get('lich-day'),
    common_1.UseGuards(auth_guard_1.AuthGuard),
    openapi.ApiResponse({ status: 200 }),
    __param(0, common_1.Query('tuan')),
    __param(1, common_1.Query('gv')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String]),
    __metadata("design:returntype", Promise)
], AppController.prototype, "layLichDay", null);
__decorate([
    common_1.Get('thong-tin'),
    openapi.ApiResponse({ status: 200 }),
    __param(0, common_1.Query('user')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], AppController.prototype, "thongTin", null);
__decorate([
    common_1.Post('doi-mat-khau'),
    openapi.ApiResponse({ status: 201 }),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [changePass_dto_1.ChangePassDTO]),
    __metadata("design:returntype", Promise)
], AppController.prototype, "doiMatKhau", null);
__decorate([
    common_1.Post('dat-mat-khau'),
    openapi.ApiResponse({ status: 201 }),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [changePass_dto_1.SetPassDTO]),
    __metadata("design:returntype", Promise)
], AppController.prototype, "datMatKhau", null);
__decorate([
    common_1.Post('gui-mail'),
    openapi.ApiResponse({ status: 201, type: String }),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [sendMail_dto_1.sendMailDTO]),
    __metadata("design:returntype", Promise)
], AppController.prototype, "guiEmail", null);
__decorate([
    common_1.Get('thong-ke'),
    openapi.ApiResponse({ status: 200, type: Object }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], AppController.prototype, "thongKe", null);
AppController = __decorate([
    common_1.Controller(),
    swagger_1.ApiTags('chung'),
    __metadata("design:paramtypes", [app_service_1.AppService])
], AppController);
exports.AppController = AppController;
//# sourceMappingURL=app.controller.js.map