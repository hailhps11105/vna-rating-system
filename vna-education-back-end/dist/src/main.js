"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@nestjs/core");
const helmet = require("helmet");
const compression = require("compression");
const app_module_1 = require("./app.module");
const swagger_1 = require("@nestjs/swagger");
const path_1 = require("path");
(async () => {
    const app = await core_1.NestFactory.create(app_module_1.AppModule, {
        cors: true,
    });
    app.use(compression());
    app.use(helmet());
    app.useStaticAssets(path_1.join(__dirname, '..', '..', 'public'));
    app.setBaseViewsDir(path_1.join(__dirname, '..', '..', 'views'));
    app.setViewEngine('pug');
    const config = new swagger_1.DocumentBuilder()
        .setTitle('VNA Education- Quản lý giáo dục')
        .setDescription('Ứng dụng đánh giá chất lượng giáo dục')
        .setVersion('3')
        .addTag('nguoi-dung', 'Các API CRUD cho model nguoi_dung')
        .addTag('danh-gia', 'Các API CRUD cho model danh_gia')
        .addTag('thong-bao', 'Các API CRUD cho model thong_bao')
        .addTag('bang-diem-tong', 'Các API CRUD cho model bang_diem_tong')
        .addTag('bang-diem-mon', 'Các API CRUD cho model bang_diem_mon')
        .addTag('diem-danh', 'Các API CRUD cho model diem_danh')
        .addTag('lop-hoc', 'Các API CRUD cho model lop_hoc')
        .addTag('mon-hoc', 'Các API CRUD cho model mon_hoc')
        .addTag('tiet-hoc', 'Các API CRUD cho model tiet_hoc')
        .addTag('tuan-hoc', 'Các API CRUD cho model tuan_hoc')
        .addTag('buoi-hoc', 'Các API CRUD cho model buoi_hoc')
        .addTag('nam-hoc', 'Các API CRUD cho model nam_hoc')
        .addTag('mau-danh-gia', 'Các API CRUD cho model mau_danh_gia')
        .addTag('chung', 'Các API chung')
        .addTag('hieu-truong', 'Các API cho người dùng là hiệu trưởng')
        .addTag('quan-tri', 'Các API cho người dùng là quản trị viên hoặc quản lý')
        .addTag('hoc-sinh', 'Các API cho người dùng là học sinh')
        .addTag('phu-huynh', 'Các API cho người dùng là phụ huynh')
        .addTag('giao-vien', 'Các API cho người dùng là giáo viên')
        .build();
    const document = swagger_1.SwaggerModule.createDocument(app, config);
    swagger_1.SwaggerModule.setup('docs', app, document);
    await app.listen(process.env.PORT || 8000);
})();
//# sourceMappingURL=main.js.map