import { AppService } from './app.service';
import { ChangePassDTO, SetPassDTO } from './helpers/changePass.dto';
import { DangNhapDTO } from './helpers/dangNhap.dto';
import { sendMailDTO } from './helpers/sendMail.dto';
export declare class AppController {
    private service;
    constructor(service: AppService);
    index(): any;
    dangNhap(dto: DangNhapDTO): Promise<{
        id: any;
        dangHD: true;
        resOK: boolean;
    } | {
        id: any;
        dangHD: boolean;
        resOK: boolean;
    }>;
    layLichHoc(tuan: string, lop: string): Promise<{
        lopHoc: string;
        buoiHoc: any[];
        _id: any;
        soTuan: number;
        tenTuan: string;
        ngayBatDau: string;
        ngayKetThuc: string;
        hocKy: number;
    }>;
    layLichDay(tuan: string, gv: string): Promise<{
        buoiHoc: any[];
        _id: any;
        soTuan: number;
        tenTuan: string;
        ngayBatDau: string;
        ngayKetThuc: string;
        hocKy: number;
    }>;
    thongTin(user: string): Promise<{
        maND: string;
        hoTen: string;
        emailND: string;
        diaChi: string;
        ngaySinh: string;
        noiSinh: string;
        gioiTinh: string;
        soDienThoai: string;
        dangHoatDong: boolean;
        quocTich: string;
        danToc: string;
        cccd: {
            maSo: string;
            ngayCap: string;
            noiCap: string;
        };
        hoChieu: {
            maSo: string;
            ngayCap: string;
            noiCap: string;
        };
        hocTap: {
            idLop: any;
            ngayNhapHoc: string;
            GVCN: string;
            lopHoc: string;
        };
        chuNhiem: string;
        chucVu: {
            chucVu: string;
            hopDong: string;
            trinhDo: string;
        };
    }>;
    doiMatKhau(dto: ChangePassDTO): Promise<{
        msg: string;
        checkOK: boolean;
    }>;
    datMatKhau(dto: SetPassDTO): Promise<{
        msg: string;
        checkOK: boolean;
    }>;
    guiEmail(toSend: sendMailDTO): Promise<string>;
    thongKe(): Promise<any>;
}
