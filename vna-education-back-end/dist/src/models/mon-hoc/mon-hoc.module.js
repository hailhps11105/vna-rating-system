"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MonHocModule = void 0;
const common_1 = require("@nestjs/common");
const mon_hoc_service_1 = require("./mon-hoc.service");
const mon_hoc_controller_1 = require("./mon-hoc.controller");
const mongoose_1 = require("@nestjs/mongoose");
const mon_hoc_entity_1 = require("./mon-hoc.entity");
const nguoi_dung_module_1 = require("../nguoi-dung/nguoi-dung.module");
let MonHocModule = class MonHocModule {
};
MonHocModule = __decorate([
    common_1.Module({
        imports: [
            mongoose_1.MongooseModule.forFeature([
                {
                    name: 'mon_hoc',
                    collection: 'mon_hoc',
                    schema: mon_hoc_entity_1.MonHocSchema,
                },
            ]),
            nguoi_dung_module_1.NguoiDungModule,
        ],
        controllers: [mon_hoc_controller_1.MonHocController],
        providers: [mon_hoc_service_1.MonHocService],
        exports: [mon_hoc_service_1.MonHocService],
    })
], MonHocModule);
exports.MonHocModule = MonHocModule;
//# sourceMappingURL=mon-hoc.module.js.map