"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MonHocService = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const utilities_1 = require("../../helpers/utilities");
const nguoi_dung_service_1 = require("../nguoi-dung/nguoi-dung.service");
let MonHocService = class MonHocService {
    model;
    ndSer;
    constructor(model, ndSer) {
        this.model = model;
        this.ndSer = ndSer;
    }
    async create(dto) {
        const { giaoVien, ...rest } = dto;
        return await this.model.create({
            ...rest,
            giaoVien: utilities_1.bulkObjectID(giaoVien),
        });
    }
    async findAll() {
        const all = await this.model
            .find()
            .populate({ path: 'giaoVien', select: 'hoTen' })
            .exec();
        const result = [];
        for (let i = 0; i < all.length; i++) {
            result.push({
                _id: all[i]._id,
                tenMH: all[i].tenMH,
                maMH: all[i].maMH,
                giaoVien: all[i].giaoVien?.map((val, ind) => {
                    return {
                        _id: all[i].populated('giaoVien')[ind],
                        hoTen: val.hoTen,
                    };
                }),
            });
        }
        return result;
    }
    async findOne(mon) {
        const one = await (await this.model.findById(mon))
            .populate({ path: 'giaoVien', select: 'hoTen' })
            .execPopulate();
        return {
            _id: mon,
            tenMH: one.tenMH,
            maMH: one.maMH,
            giaoVien: one.giaoVien?.map((val, ind) => {
                return {
                    _id: one.populated('giaoVien')[ind],
                    hoTen: val.hoTen,
                };
            }),
        };
    }
    async update(id, dto) {
        const { giaoVien, ...rest } = dto;
        return await this.model.findById(id, null, null, async (err, doc) => {
            if (err)
                throw err;
            utilities_1.assign(rest, doc);
            if (giaoVien)
                doc.giaoVien = await this.ndSer.bulkObjectify(giaoVien);
            await doc.save();
        });
    }
    async objectify(mon) {
        return (await this.model.findById(mon))._id;
    }
    async remove(id) {
        return await this.model.findByIdAndDelete(id);
    }
};
MonHocService = __decorate([
    common_1.Injectable(),
    __param(0, mongoose_1.InjectModel('mon_hoc')),
    __metadata("design:paramtypes", [mongoose_2.Model,
        nguoi_dung_service_1.NguoiDungService])
], MonHocService);
exports.MonHocService = MonHocService;
//# sourceMappingURL=mon-hoc.service.js.map