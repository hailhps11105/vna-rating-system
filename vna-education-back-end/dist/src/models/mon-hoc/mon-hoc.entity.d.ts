import { Document, Schema as MongooseSchema } from 'mongoose';
import { NguoiDung } from '../nguoi-dung/nguoi-dung.entity';
export declare type MonHocDocument = MonHoc & Document;
export declare class MonHoc {
    maMH: string;
    tenMH: string;
    giaoVien: NguoiDung[];
}
export declare const MonHocSchema: MongooseSchema<Document<MonHoc, any, any>, import("mongoose").Model<any, any, any>, undefined, any>;
