import { MonHocService } from './mon-hoc.service';
import { MonHocDto } from './mon-hoc.dto';
export declare class MonHocController {
    private readonly service;
    constructor(service: MonHocService);
    create(dto: MonHocDto): Promise<import("./mon-hoc.entity").MonHocDocument>;
    findAll(): Promise<any[]>;
    findOne(id: string): Promise<{
        _id: string;
        tenMH: string;
        maMH: string;
        giaoVien: {
            _id: any;
            hoTen: string;
        }[];
    }>;
    update(id: string, dto: MonHocDto): Promise<import("./mon-hoc.entity").MonHocDocument>;
    remove(id: string): Promise<import("./mon-hoc.entity").MonHocDocument>;
}
