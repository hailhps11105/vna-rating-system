"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MonHocSchema = exports.MonHoc = void 0;
const openapi = require("@nestjs/swagger");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
let MonHoc = class MonHoc {
    maMH;
    tenMH;
    giaoVien;
    static _OPENAPI_METADATA_FACTORY() {
        return { maMH: { required: true, type: () => String }, tenMH: { required: true, type: () => String }, giaoVien: { required: true, type: () => [require("../nguoi-dung/nguoi-dung.entity").NguoiDung] } };
    }
};
__decorate([
    mongoose_1.Prop({ required: true, index: true, unique: true }),
    __metadata("design:type", String)
], MonHoc.prototype, "maMH", void 0);
__decorate([
    mongoose_1.Prop({ required: true }),
    __metadata("design:type", String)
], MonHoc.prototype, "tenMH", void 0);
__decorate([
    mongoose_1.Prop({
        required: true,
        type: [
            {
                type: mongoose_2.Schema.Types.ObjectId,
                ref: 'nguoi_dung',
            },
        ],
    }),
    __metadata("design:type", Array)
], MonHoc.prototype, "giaoVien", void 0);
MonHoc = __decorate([
    mongoose_1.Schema({
        timestamps: {
            createdAt: 'thoiDiemTao',
            updatedAt: 'thoiDiemSua',
        },
        versionKey: false,
    })
], MonHoc);
exports.MonHoc = MonHoc;
exports.MonHocSchema = mongoose_1.SchemaFactory.createForClass(MonHoc);
//# sourceMappingURL=mon-hoc.entity.js.map