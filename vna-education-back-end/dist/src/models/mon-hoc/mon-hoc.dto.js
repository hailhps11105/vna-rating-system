"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MonHocDto = void 0;
const openapi = require("@nestjs/swagger");
class MonHocDto {
    tenMH;
    maMH;
    giaoVien;
    static _OPENAPI_METADATA_FACTORY() {
        return { tenMH: { required: true, type: () => String }, maMH: { required: true, type: () => String }, giaoVien: { required: true, type: () => [String] } };
    }
}
exports.MonHocDto = MonHocDto;
//# sourceMappingURL=mon-hoc.dto.js.map