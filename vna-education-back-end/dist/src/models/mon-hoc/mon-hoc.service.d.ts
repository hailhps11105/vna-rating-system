import { Model } from 'mongoose';
import { NguoiDungService } from '../nguoi-dung/nguoi-dung.service';
import { MonHocDto } from './mon-hoc.dto';
import { MonHocDocument } from './mon-hoc.entity';
export declare class MonHocService {
    private model;
    private readonly ndSer;
    constructor(model: Model<MonHocDocument>, ndSer: NguoiDungService);
    create(dto: MonHocDto): Promise<MonHocDocument>;
    findAll(): Promise<any[]>;
    findOne(mon: string): Promise<{
        _id: string;
        tenMH: string;
        maMH: string;
        giaoVien: {
            _id: any;
            hoTen: string;
        }[];
    }>;
    update(id: string, dto: MonHocDto): Promise<MonHocDocument>;
    objectify(mon: string): Promise<any>;
    remove(id: string): Promise<MonHocDocument>;
}
