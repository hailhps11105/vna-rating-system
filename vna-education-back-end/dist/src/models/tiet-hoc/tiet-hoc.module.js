"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TietHocModule = void 0;
const common_1 = require("@nestjs/common");
const tiet_hoc_service_1 = require("./tiet-hoc.service");
const tiet_hoc_controller_1 = require("./tiet-hoc.controller");
const tiet_hoc_entity_1 = require("./tiet-hoc.entity");
const mongoose_1 = require("@nestjs/mongoose");
const nguoi_dung_module_1 = require("../nguoi-dung/nguoi-dung.module");
const lop_hoc_module_1 = require("../lop-hoc/lop-hoc.module");
const mon_hoc_module_1 = require("../mon-hoc/mon-hoc.module");
const diem_danh_module_1 = require("../diem-danh/diem-danh.module");
const buoi_hoc_module_1 = require("../buoi-hoc/buoi-hoc.module");
let TietHocModule = class TietHocModule {
};
TietHocModule = __decorate([
    common_1.Module({
        imports: [
            mongoose_1.MongooseModule.forFeature([
                {
                    name: 'tiet_hoc',
                    collection: 'tiet_hoc',
                    schema: tiet_hoc_entity_1.TietHocSchema,
                },
            ]),
            nguoi_dung_module_1.NguoiDungModule,
            lop_hoc_module_1.LopHocModule,
            mon_hoc_module_1.MonHocModule,
            diem_danh_module_1.DiemDanhModule,
            buoi_hoc_module_1.BuoiHocModule,
        ],
        controllers: [tiet_hoc_controller_1.TietHocController],
        providers: [tiet_hoc_service_1.TietHocService],
        exports: [tiet_hoc_service_1.TietHocService],
    })
], TietHocModule);
exports.TietHocModule = TietHocModule;
//# sourceMappingURL=tiet-hoc.module.js.map