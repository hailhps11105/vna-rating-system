import { Model } from 'mongoose';
import { BuoiHocService } from '../buoi-hoc/buoi-hoc.service';
import { DiemDanhService } from '../diem-danh/diem-danh.service';
import { LopHocService } from '../lop-hoc/lop-hoc.service';
import { MonHocService } from '../mon-hoc/mon-hoc.service';
import { NguoiDungService } from '../nguoi-dung/nguoi-dung.service';
import { CreateTietHocDto } from './dto/create-tiet-hoc.dto';
import { UpdateTietHocDto } from './dto/update-tiet-hoc.dto';
import { TietHocDocument } from './tiet-hoc.entity';
export declare class TietHocService {
    private model;
    private readonly lhSer;
    private readonly ndSer;
    private readonly mhSer;
    private readonly ddSer;
    private readonly bhSer;
    constructor(model: Model<TietHocDocument>, lhSer: LopHocService, ndSer: NguoiDungService, mhSer: MonHocService, ddSer: DiemDanhService, bhSer: BuoiHocService);
    create(dto: CreateTietHocDto): Promise<TietHocDocument>;
    findAll(condition?: any): Promise<any[]>;
    getAll(condition?: any): Promise<any[]>;
    findOne(tiet: string): Promise<{
        _id: string;
        thuTiet: string;
        thoiGian: string;
        giaoVien: {
            _id: any;
            hoTen: string;
        };
        monHoc: {
            _id: any;
            tenMH: string;
            maMH: string;
        };
        lopHoc: {
            _id: any;
            maLH: string;
        };
        buoiHoc: {
            _id: any;
            thu: string;
            ngayHoc: string;
            tuanHoc: number;
        };
    }>;
    findAll_byDate(buoi: string): Promise<any[]>;
    update(id: string, dto: UpdateTietHocDto): Promise<TietHocDocument>;
    objectify(tiet: string): Promise<any>;
    bulkObjectify(tiet: string[]): Promise<any[]>;
    remove(id: string): Promise<TietHocDocument>;
}
