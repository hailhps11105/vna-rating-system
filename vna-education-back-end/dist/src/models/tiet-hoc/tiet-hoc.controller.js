"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TietHocController = void 0;
const openapi = require("@nestjs/swagger");
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const auth_guard_1 = require("../../auth.guard");
const create_tiet_hoc_dto_1 = require("./dto/create-tiet-hoc.dto");
const update_tiet_hoc_dto_1 = require("./dto/update-tiet-hoc.dto");
const tiet_hoc_service_1 = require("./tiet-hoc.service");
let TietHocController = class TietHocController {
    service;
    constructor(service) {
        this.service = service;
    }
    async create(dto) {
        return await this.service.create(dto);
    }
    async findAll() {
        return await this.service.findAll();
    }
    async findBY(buoi) {
        if (buoi && buoi != '')
            return await this.service.findAll_byDate(buoi);
        return await this.service.findAll();
    }
    async findOne(id) {
        return await this.service.findOne(id);
    }
    async update(id, dto) {
        return await this.service.update(id, dto);
    }
    async remove(id) {
        return await this.service.remove(id);
    }
};
__decorate([
    common_1.Post(),
    swagger_1.ApiCreatedResponse({ description: 'Tạo thành công' }),
    openapi.ApiResponse({ status: 201, type: Object }),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_tiet_hoc_dto_1.CreateTietHocDto]),
    __metadata("design:returntype", Promise)
], TietHocController.prototype, "create", null);
__decorate([
    common_1.Get(),
    swagger_1.ApiOkResponse({ description: 'Trả về tất cả' }),
    openapi.ApiResponse({ status: 200, type: [Object] }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], TietHocController.prototype, "findAll", null);
__decorate([
    common_1.Get('theo'),
    openapi.ApiResponse({ status: 200, type: [Object] }),
    __param(0, common_1.Query('buoi')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], TietHocController.prototype, "findBY", null);
__decorate([
    common_1.Get(':id'),
    swagger_1.ApiOkResponse({ description: 'Trả về 1 đối tượng' }),
    openapi.ApiResponse({ status: 200 }),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], TietHocController.prototype, "findOne", null);
__decorate([
    common_1.Patch(':id'),
    swagger_1.ApiOkResponse({ description: 'Cập nhật thành công' }),
    openapi.ApiResponse({ status: 200, type: Object }),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, update_tiet_hoc_dto_1.UpdateTietHocDto]),
    __metadata("design:returntype", Promise)
], TietHocController.prototype, "update", null);
__decorate([
    common_1.Delete(':id'),
    swagger_1.ApiOkResponse({ description: 'Xóa thành công' }),
    openapi.ApiResponse({ status: 200, type: Object }),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], TietHocController.prototype, "remove", null);
TietHocController = __decorate([
    common_1.Controller('tiet-hoc'),
    common_1.UseGuards(auth_guard_1.AuthGuard),
    swagger_1.ApiTags('tiet-hoc'),
    __metadata("design:paramtypes", [tiet_hoc_service_1.TietHocService])
], TietHocController);
exports.TietHocController = TietHocController;
//# sourceMappingURL=tiet-hoc.controller.js.map