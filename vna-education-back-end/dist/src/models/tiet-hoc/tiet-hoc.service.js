"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TietHocService = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const utilities_1 = require("../../helpers/utilities");
const buoi_hoc_service_1 = require("../buoi-hoc/buoi-hoc.service");
const diem_danh_service_1 = require("../diem-danh/diem-danh.service");
const lop_hoc_service_1 = require("../lop-hoc/lop-hoc.service");
const mon_hoc_service_1 = require("../mon-hoc/mon-hoc.service");
const nguoi_dung_service_1 = require("../nguoi-dung/nguoi-dung.service");
let TietHocService = class TietHocService {
    model;
    lhSer;
    ndSer;
    mhSer;
    ddSer;
    bhSer;
    constructor(model, lhSer, ndSer, mhSer, ddSer, bhSer) {
        this.model = model;
        this.lhSer = lhSer;
        this.ndSer = ndSer;
        this.mhSer = mhSer;
        this.ddSer = ddSer;
        this.bhSer = bhSer;
    }
    async create(dto) {
        const to404 = await this.model.find({
            giaoVien: Object(dto.giaoVien),
            monHoc: Object(dto.monHoc),
            lopHoc: Object(dto.lopHoc),
            thuTiet: dto.thuTiet,
            thoiGian_batDau: dto.thoiGian_batDau,
            buoiHoc: Object(dto.buoiHoc),
        });
        if (to404.length > 0)
            return null;
        else {
            return await this.model.create({
                giaoVien: mongoose_2.Types.ObjectId(dto.giaoVien),
                monHoc: mongoose_2.Types.ObjectId(dto.monHoc),
                lopHoc: mongoose_2.Types.ObjectId(dto.lopHoc),
                thuTiet: dto.thuTiet,
                thoiGian_batDau: dto.thoiGian_batDau,
                buoiHoc: mongoose_2.Types.ObjectId(dto.buoiHoc),
                diemDanh: [],
            });
        }
    }
    async findAll(condition = {}) {
        const all = await this.model
            .find(condition)
            .populate([
            { path: 'giaoVien', select: 'hoTen' },
            { path: 'monHoc', select: ['tenMH', 'maMH'] },
            { path: 'lopHoc', select: 'maLH' },
            {
                path: 'buoiHoc',
                select: ['thu', 'ngayHoc', 'tuanHoc'],
                populate: {
                    path: 'tuanHoc',
                    select: 'soTuan',
                },
            },
        ])
            .exec();
        const result = [];
        for (let i = 0; i < all.length; i++) {
            result.push({
                _id: all[i]._id,
                thuTiet: all[i].thuTiet,
                thoiGian: all[i].thoiGian_batDau,
                giaoVien: all[i].giaoVien
                    ? {
                        _id: all[i].populated('giaoVien'),
                        hoTen: all[i].giaoVien.hoTen,
                    }
                    : null,
                monHoc: all[i].monHoc
                    ? {
                        _id: all[i].populated('monHoc'),
                        tenMH: all[i].monHoc.tenMH,
                        maMH: all[i].monHoc.maMH,
                    }
                    : null,
                lopHoc: all[i].lopHoc
                    ? {
                        _id: all[i].populated('lopHoc'),
                        maLH: all[i].lopHoc.maLH,
                    }
                    : null,
                buoiHoc: all[i].buoiHoc
                    ? {
                        _id: all[i].populated('buoiHoc'),
                        thu: all[i].buoiHoc.thu,
                        ngayHoc: all[i].buoiHoc.ngayHoc,
                        tuanHoc: all[i].buoiHoc.tuanHoc.soTuan,
                    }
                    : null,
            });
        }
        return result;
    }
    async getAll(condition = {}) {
        const all = await this.model
            .find(condition)
            .populate([
            { path: 'giaoVien', select: 'hoTen' },
            { path: 'monHoc', select: 'tenMH' },
            { path: 'lopHoc', select: 'maLH' },
            {
                path: 'buoiHoc',
                select: ['tuanHoc', 'thu', 'ngayHoc'],
                populate: {
                    path: 'tuanHoc',
                    select: 'soTuan',
                },
            },
        ])
            .exec();
        const result = [];
        for (let i = 0; i < all.length; i++) {
            result.push({
                _id: all[i]._id,
                thuTiet: all[i].thuTiet,
                thoiGian: all[i].thoiGian_batDau,
                giaoVien: all[i].giaoVien ? all[i].giaoVien.hoTen : null,
                monHoc: all[i].monHoc ? all[i].monHoc.tenMH : null,
                lopHoc: all[i].lopHoc ? all[i].lopHoc.maLH : null,
                buoiHoc: all[i].buoiHoc
                    ? {
                        thu: all[i].buoiHoc.thu,
                        ngayHoc: all[i].buoiHoc.ngayHoc,
                        tuanHoc: all[i].buoiHoc.tuanHoc.soTuan,
                    }
                    : null,
            });
        }
        return result;
    }
    async findOne(tiet) {
        const cl = await (await this.model.findById(tiet))
            .populate([
            { path: 'giaoVien', select: 'hoTen' },
            { path: 'monHoc', select: ['tenMH', 'maMH'] },
            { path: 'lopHoc', select: 'maLH' },
            {
                path: 'buoiHoc',
                select: ['thu', 'ngayHoc', 'tuanHoc'],
                populate: {
                    path: 'tuanHoc',
                    select: 'soTuan',
                },
            },
        ])
            .execPopulate();
        return {
            _id: tiet,
            thuTiet: cl.thuTiet,
            thoiGian: cl.thoiGian_batDau,
            giaoVien: cl.giaoVien
                ? {
                    _id: cl.populated('giaoVien'),
                    hoTen: cl.giaoVien.hoTen,
                }
                : null,
            monHoc: cl.monHoc
                ? {
                    _id: cl.populated('monHoc'),
                    tenMH: cl.monHoc.tenMH,
                    maMH: cl.monHoc.maMH,
                }
                : null,
            lopHoc: cl.lopHoc
                ? {
                    _id: cl.populated('lopHoc'),
                    maLH: cl.lopHoc.maLH,
                }
                : null,
            buoiHoc: cl.buoiHoc
                ? {
                    _id: cl.populated('buoiHoc'),
                    thu: cl.buoiHoc.thu,
                    ngayHoc: cl.buoiHoc.ngayHoc,
                    tuanHoc: cl.buoiHoc.tuanHoc.soTuan,
                }
                : null,
        };
    }
    async findAll_byDate(buoi) {
        return await this.findAll({ buoiHoc: Object(buoi) });
    }
    async update(id, dto) {
        const { lopHoc, giaoVien, monHoc, diemDanh, buoiHoc, ...rest } = dto;
        return await this.model.findById(id, null, null, async (err, doc) => {
            if (err)
                throw err;
            utilities_1.assign(rest, doc);
            if (lopHoc)
                doc.lopHoc = await this.lhSer.objectify_fromID(lopHoc);
            if (giaoVien)
                doc.giaoVien = await this.ndSer.objectify(giaoVien);
            if (monHoc)
                doc.monHoc = await this.mhSer.objectify(monHoc);
            if (buoiHoc)
                doc.buoiHoc = await this.bhSer.objectify(buoiHoc);
            if (diemDanh)
                doc.diemDanh = await this.ddSer.bulkObjectify(diemDanh);
            await doc.save();
        });
    }
    async objectify(tiet) {
        return (await this.model.findById(tiet))._id;
    }
    async bulkObjectify(tiet) {
        const result = [];
        for (let i = 0; i < tiet.length; i++) {
            result.push(await this.objectify(tiet[i]));
        }
        return result;
    }
    async remove(id) {
        return await this.model.findByIdAndDelete(id);
    }
};
TietHocService = __decorate([
    common_1.Injectable(),
    __param(0, mongoose_1.InjectModel('tiet_hoc')),
    __metadata("design:paramtypes", [mongoose_2.Model,
        lop_hoc_service_1.LopHocService,
        nguoi_dung_service_1.NguoiDungService,
        mon_hoc_service_1.MonHocService,
        diem_danh_service_1.DiemDanhService,
        buoi_hoc_service_1.BuoiHocService])
], TietHocService);
exports.TietHocService = TietHocService;
//# sourceMappingURL=tiet-hoc.service.js.map