"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateTietHocDto = void 0;
const openapi = require("@nestjs/swagger");
class CreateTietHocDto {
    giaoVien;
    monHoc;
    lopHoc;
    thuTiet;
    thoiGian_batDau;
    buoiHoc;
    static _OPENAPI_METADATA_FACTORY() {
        return { giaoVien: { required: true, type: () => String }, monHoc: { required: true, type: () => String }, lopHoc: { required: true, type: () => String }, thuTiet: { required: true, type: () => String }, thoiGian_batDau: { required: true, type: () => String }, buoiHoc: { required: true, type: () => String } };
    }
}
exports.CreateTietHocDto = CreateTietHocDto;
//# sourceMappingURL=create-tiet-hoc.dto.js.map