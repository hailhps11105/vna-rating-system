"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateTietHocDto = void 0;
const openapi = require("@nestjs/swagger");
const swagger_1 = require("@nestjs/swagger");
const create_tiet_hoc_dto_1 = require("./create-tiet-hoc.dto");
class UpdateTietHocDto extends swagger_1.PartialType(create_tiet_hoc_dto_1.CreateTietHocDto) {
    diemDanh;
    static _OPENAPI_METADATA_FACTORY() {
        return { diemDanh: { required: true, type: () => [String] } };
    }
}
exports.UpdateTietHocDto = UpdateTietHocDto;
//# sourceMappingURL=update-tiet-hoc.dto.js.map