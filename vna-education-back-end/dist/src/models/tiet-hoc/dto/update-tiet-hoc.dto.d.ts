import { CreateTietHocDto } from './create-tiet-hoc.dto';
declare const UpdateTietHocDto_base: import("@nestjs/common").Type<Partial<CreateTietHocDto>>;
export declare class UpdateTietHocDto extends UpdateTietHocDto_base {
    diemDanh: string[];
}
export {};
