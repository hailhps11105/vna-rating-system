"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TietHocSchema = exports.TietHoc = void 0;
const openapi = require("@nestjs/swagger");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const buoi_hoc_entity_1 = require("../buoi-hoc/buoi-hoc.entity");
const lop_hoc_entity_1 = require("../lop-hoc/lop-hoc.entity");
const mon_hoc_entity_1 = require("../mon-hoc/mon-hoc.entity");
const nguoi_dung_entity_1 = require("../nguoi-dung/nguoi-dung.entity");
let TietHoc = class TietHoc {
    giaoVien;
    monHoc;
    lopHoc;
    thuTiet;
    thoiGian_batDau;
    buoiHoc;
    diemDanh;
    static _OPENAPI_METADATA_FACTORY() {
        return { giaoVien: { required: true, type: () => require("../nguoi-dung/nguoi-dung.entity").NguoiDung }, monHoc: { required: true, type: () => require("../mon-hoc/mon-hoc.entity").MonHoc }, lopHoc: { required: true, type: () => require("../lop-hoc/lop-hoc.entity").LopHoc }, thuTiet: { required: true, type: () => String }, thoiGian_batDau: { required: true, type: () => String }, buoiHoc: { required: true, type: () => require("../buoi-hoc/buoi-hoc.entity").BuoiHoc }, diemDanh: { required: true, type: () => [require("../diem-danh/diem-danh.entity").DiemDanh] } };
    }
};
__decorate([
    mongoose_1.Prop({
        required: true,
        ref: 'nguoi_dung',
        type: mongoose_2.Schema.Types.ObjectId,
    }),
    __metadata("design:type", nguoi_dung_entity_1.NguoiDung)
], TietHoc.prototype, "giaoVien", void 0);
__decorate([
    mongoose_1.Prop({
        required: true,
        ref: 'mon_hoc',
        type: mongoose_2.Schema.Types.ObjectId,
    }),
    __metadata("design:type", mon_hoc_entity_1.MonHoc)
], TietHoc.prototype, "monHoc", void 0);
__decorate([
    mongoose_1.Prop({
        required: true,
        ref: 'lop_hoc',
        type: mongoose_2.Schema.Types.ObjectId,
    }),
    __metadata("design:type", lop_hoc_entity_1.LopHoc)
], TietHoc.prototype, "lopHoc", void 0);
__decorate([
    mongoose_1.Prop({
        default: 'Tiết 0',
    }),
    __metadata("design:type", String)
], TietHoc.prototype, "thuTiet", void 0);
__decorate([
    mongoose_1.Prop({
        default: '0h',
    }),
    __metadata("design:type", String)
], TietHoc.prototype, "thoiGian_batDau", void 0);
__decorate([
    mongoose_1.Prop({
        required: true,
        ref: 'buoi_hoc',
        type: mongoose_2.Schema.Types.ObjectId,
    }),
    __metadata("design:type", buoi_hoc_entity_1.BuoiHoc)
], TietHoc.prototype, "buoiHoc", void 0);
__decorate([
    mongoose_1.Prop({
        type: [
            {
                ref: 'diem_danh',
                type: mongoose_2.Schema.Types.ObjectId,
            },
        ],
    }),
    __metadata("design:type", Array)
], TietHoc.prototype, "diemDanh", void 0);
TietHoc = __decorate([
    mongoose_1.Schema({
        timestamps: {
            createdAt: 'thoiDiemTao',
            updatedAt: 'thoiDiemSua',
        },
        versionKey: false,
    })
], TietHoc);
exports.TietHoc = TietHoc;
exports.TietHocSchema = mongoose_1.SchemaFactory.createForClass(TietHoc);
//# sourceMappingURL=tiet-hoc.entity.js.map