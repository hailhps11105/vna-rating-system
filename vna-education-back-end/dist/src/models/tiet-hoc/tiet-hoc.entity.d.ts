import { Document, Schema as MongooseSchema } from 'mongoose';
import { BuoiHoc } from '../buoi-hoc/buoi-hoc.entity';
import { DiemDanh } from '../diem-danh/diem-danh.entity';
import { LopHoc } from '../lop-hoc/lop-hoc.entity';
import { MonHoc } from '../mon-hoc/mon-hoc.entity';
import { NguoiDung } from '../nguoi-dung/nguoi-dung.entity';
export declare type TietHocDocument = TietHoc & Document;
export declare class TietHoc {
    giaoVien: NguoiDung;
    monHoc: MonHoc;
    lopHoc: LopHoc;
    thuTiet: string;
    thoiGian_batDau: string;
    buoiHoc: BuoiHoc;
    diemDanh: DiemDanh[];
}
export declare const TietHocSchema: MongooseSchema<Document<TietHoc, any, any>, import("mongoose").Model<any, any, any>, undefined, any>;
