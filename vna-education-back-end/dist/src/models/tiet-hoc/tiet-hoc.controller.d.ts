import { CreateTietHocDto } from './dto/create-tiet-hoc.dto';
import { UpdateTietHocDto } from './dto/update-tiet-hoc.dto';
import { TietHocService } from './tiet-hoc.service';
export declare class TietHocController {
    private readonly service;
    constructor(service: TietHocService);
    create(dto: CreateTietHocDto): Promise<import("./tiet-hoc.entity").TietHocDocument>;
    findAll(): Promise<any[]>;
    findBY(buoi: string): Promise<any[]>;
    findOne(id: string): Promise<{
        _id: string;
        thuTiet: string;
        thoiGian: string;
        giaoVien: {
            _id: any;
            hoTen: string;
        };
        monHoc: {
            _id: any;
            tenMH: string;
            maMH: string;
        };
        lopHoc: {
            _id: any;
            maLH: string;
        };
        buoiHoc: {
            _id: any;
            thu: string;
            ngayHoc: string;
            tuanHoc: number;
        };
    }>;
    update(id: string, dto: UpdateTietHocDto): Promise<import("./tiet-hoc.entity").TietHocDocument>;
    remove(id: string): Promise<import("./tiet-hoc.entity").TietHocDocument>;
}
