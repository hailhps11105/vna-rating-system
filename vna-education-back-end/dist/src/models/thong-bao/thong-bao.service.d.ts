import { Model } from 'mongoose';
import { NguoiDungService } from '../nguoi-dung/nguoi-dung.service';
import { ThongBaoDto } from './thong-bao.dto';
import { ThongBaoDocument } from './thong-bao.entity';
export declare class ThongBaoService {
    private model;
    private readonly ndSer;
    constructor(model: Model<ThongBaoDocument>, ndSer: NguoiDungService);
    create(dto: ThongBaoDto): Promise<ThongBaoDocument>;
    findAll(): Promise<any[]>;
    getAll(condition?: any): Promise<any[]>;
    getAll_byCatalog(catalog: string): Promise<any[]>;
    findOne(tb: string): Promise<{
        _id: string;
        danhMuc: string;
        tieuDe: string;
        tomTat: string;
        nguoiDang: {
            _id: any;
            hoTen: string;
        };
        ngayDang: string;
        noiDung: string;
        daDuyet: boolean;
    }>;
    update(id: string, dto: ThongBaoDto): Promise<ThongBaoDocument>;
    remove(id: string): Promise<ThongBaoDocument>;
}
