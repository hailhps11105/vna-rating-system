export declare class ThongBaoDto {
    nguoiDang: string;
    tieuDe: string;
    tomTat: string;
    noiDung: string;
    danhMuc: string;
    ngayDang: string;
    daDuyet: boolean;
}
