"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ThongBaoModule = void 0;
const common_1 = require("@nestjs/common");
const thong_bao_service_1 = require("./thong-bao.service");
const thong_bao_controller_1 = require("./thong-bao.controller");
const mongoose_1 = require("@nestjs/mongoose");
const thong_bao_entity_1 = require("./thong-bao.entity");
const nguoi_dung_module_1 = require("../nguoi-dung/nguoi-dung.module");
let ThongBaoModule = class ThongBaoModule {
};
ThongBaoModule = __decorate([
    common_1.Module({
        imports: [
            mongoose_1.MongooseModule.forFeature([
                {
                    name: 'thong_bao',
                    collection: 'thong_bao',
                    schema: thong_bao_entity_1.ThongBaoSchema,
                },
            ]),
            nguoi_dung_module_1.NguoiDungModule,
        ],
        controllers: [thong_bao_controller_1.ThongBaoController],
        providers: [thong_bao_service_1.ThongBaoService],
        exports: [thong_bao_service_1.ThongBaoService],
    })
], ThongBaoModule);
exports.ThongBaoModule = ThongBaoModule;
//# sourceMappingURL=thong-bao.module.js.map