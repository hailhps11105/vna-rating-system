import { ThongBaoService } from './thong-bao.service';
import { ThongBaoDto } from './thong-bao.dto';
export declare class ThongBaoController {
    private readonly service;
    constructor(service: ThongBaoService);
    create(dto: ThongBaoDto): Promise<import("./thong-bao.entity").ThongBaoDocument>;
    findAll(): Promise<any[]>;
    findBy(muc: string): Promise<any[]>;
    findOne(id: string): Promise<{
        _id: string;
        danhMuc: string;
        tieuDe: string;
        tomTat: string;
        nguoiDang: {
            _id: any;
            hoTen: string;
        };
        ngayDang: string;
        noiDung: string;
        daDuyet: boolean;
    }>;
    update(id: string, dto: ThongBaoDto): Promise<import("./thong-bao.entity").ThongBaoDocument>;
    remove(id: string): Promise<import("./thong-bao.entity").ThongBaoDocument>;
}
