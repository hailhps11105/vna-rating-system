import { Document, Schema as MongooseSchema } from 'mongoose';
import { NguoiDung } from '../nguoi-dung/nguoi-dung.entity';
export declare type ThongBaoDocument = ThongBao & Document;
export declare class ThongBao {
    nguoiDang: NguoiDung;
    tieuDe: string;
    tomTat: string;
    noiDung: string;
    danhMuc: string;
    ngayDang: string;
    daDuyet: boolean;
}
export declare const ThongBaoSchema: MongooseSchema<Document<ThongBao, any, any>, import("mongoose").Model<any, any, any>, undefined, any>;
