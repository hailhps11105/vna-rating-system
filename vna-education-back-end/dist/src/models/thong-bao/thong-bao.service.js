"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ThongBaoService = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const utilities_1 = require("../../helpers/utilities");
const nguoi_dung_service_1 = require("../nguoi-dung/nguoi-dung.service");
let ThongBaoService = class ThongBaoService {
    model;
    ndSer;
    constructor(model, ndSer) {
        this.model = model;
        this.ndSer = ndSer;
    }
    async create(dto) {
        const { nguoiDang, ...rest } = dto;
        return await this.model.create({
            ...rest,
            nguoiDang: mongoose_2.Types.ObjectId(nguoiDang),
        });
    }
    async findAll() {
        const news = await this.model
            .find({}, null, { sort: { date: 1 } })
            .populate([
            {
                path: 'nguoiDang',
                select: 'hoTen',
            },
        ])
            .exec();
        const result = [];
        for (let i = 0; i < news.length; i++) {
            result.push({
                _id: news[i]._id,
                danhMuc: news[i].danhMuc,
                tieuDe: news[i].tieuDe,
                tomTat: news[i].tomTat,
                nguoiDang: news[i].nguoiDang
                    ? {
                        _id: news[i].populated('nguoiDang'),
                        hoTen: news[i].nguoiDang.hoTen,
                    }
                    : null,
                ngayDang: news[i].ngayDang,
                noiDung: news[i].noiDung,
                daDuyet: news[i].daDuyet,
            });
        }
        return result;
    }
    async getAll(condition = {}) {
        const all = await this.model
            .find(condition)
            .populate([
            {
                path: 'nguoiDang',
                select: 'hoTen',
            },
        ])
            .exec();
        const result = [];
        for (let i = 0; i < all.length; i++) {
            result.push({
                _id: all[i]._id,
                danhMuc: all[i].danhMuc,
                tieuDe: all[i].tieuDe,
                tomTat: all[i].tomTat,
                nguoiDang: all[i].nguoiDang?.hoTen,
                ngayDang: all[i].ngayDang,
                noiDung: all[i].noiDung,
                daDuyet: all[i].daDuyet,
            });
        }
        return result;
    }
    async getAll_byCatalog(catalog) {
        return await this.getAll({ danhMuc: catalog });
    }
    async findOne(tb) {
        const ns = await (await this.model.findById(tb))
            .populate([
            {
                path: 'nguoiDang',
                select: 'hoTen',
            },
        ])
            .execPopulate();
        return {
            _id: tb,
            danhMuc: ns.danhMuc,
            tieuDe: ns.tieuDe,
            tomTat: ns.tomTat,
            nguoiDang: ns.nguoiDang
                ? {
                    _id: ns.populated('nguoiDang'),
                    hoTen: ns.nguoiDang.hoTen,
                }
                : null,
            ngayDang: ns.ngayDang,
            noiDung: ns.noiDung,
            daDuyet: ns.daDuyet,
        };
    }
    async update(id, dto) {
        const { nguoiDang, ...rest } = dto;
        return await this.model.findById(id, null, null, async (err, doc) => {
            if (err)
                throw err;
            utilities_1.assign(rest, doc);
            if (nguoiDang)
                doc.nguoiDang = await this.ndSer.objectify(nguoiDang);
            await doc.save();
        });
    }
    async remove(id) {
        return await this.model.findByIdAndDelete(id);
    }
};
ThongBaoService = __decorate([
    common_1.Injectable(),
    __param(0, mongoose_1.InjectModel('thong_bao')),
    __metadata("design:paramtypes", [mongoose_2.Model,
        nguoi_dung_service_1.NguoiDungService])
], ThongBaoService);
exports.ThongBaoService = ThongBaoService;
//# sourceMappingURL=thong-bao.service.js.map