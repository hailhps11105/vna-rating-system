"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ThongBaoDto = void 0;
const openapi = require("@nestjs/swagger");
const swagger_1 = require("@nestjs/swagger");
class ThongBaoDto {
    nguoiDang;
    tieuDe;
    tomTat;
    noiDung;
    danhMuc;
    ngayDang;
    daDuyet;
    static _OPENAPI_METADATA_FACTORY() {
        return { nguoiDang: { required: true, type: () => String }, tieuDe: { required: true, type: () => String }, tomTat: { required: true, type: () => String }, noiDung: { required: true, type: () => String }, danhMuc: { required: true, type: () => String }, ngayDang: { required: true, type: () => String }, daDuyet: { required: true, type: () => Boolean } };
    }
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        name: 'nguoiDang',
        type: String,
        example: '60bxxxxxxxxxxxxx',
        description: 'Người đăng thông báo',
    }),
    __metadata("design:type", String)
], ThongBaoDto.prototype, "nguoiDang", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        name: 'tieuDe',
        type: String,
        example: 'Đây là 1 bài viết hay',
        description: 'Tiêu đề thông báo',
    }),
    __metadata("design:type", String)
], ThongBaoDto.prototype, "tieuDe", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        name: 'tomTat',
        type: String,
        example: 'Đây là 1 bài viết không tồi',
        description: 'Nội dung tóm tắt của thông báo',
    }),
    __metadata("design:type", String)
], ThongBaoDto.prototype, "tomTat", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        name: 'noiDung',
        type: String,
        example: 'Không không tồi. Không không tồi',
        description: 'Nội dung đầy đủ của thông báo',
    }),
    __metadata("design:type", String)
], ThongBaoDto.prototype, "noiDung", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        default: 'Khác',
        name: 'danhMuc',
        type: String,
        description: 'Danh mục của thông báo',
    }),
    __metadata("design:type", String)
], ThongBaoDto.prototype, "danhMuc", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        name: 'ngayDang',
        type: String,
        example: '6-7-2021',
        description: 'Ngày đăng thông báo',
    }),
    __metadata("design:type", String)
], ThongBaoDto.prototype, "ngayDang", void 0);
__decorate([
    swagger_1.ApiProperty({
        default: true,
        name: 'daDuyet',
        type: Boolean,
        description: 'Trạng thái duyệt thông báo',
    }),
    __metadata("design:type", Boolean)
], ThongBaoDto.prototype, "daDuyet", void 0);
exports.ThongBaoDto = ThongBaoDto;
//# sourceMappingURL=thong-bao.dto.js.map