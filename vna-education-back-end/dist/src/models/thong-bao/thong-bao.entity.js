"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ThongBaoSchema = exports.ThongBao = void 0;
const openapi = require("@nestjs/swagger");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const nguoi_dung_entity_1 = require("../nguoi-dung/nguoi-dung.entity");
let ThongBao = class ThongBao {
    nguoiDang;
    tieuDe;
    tomTat;
    noiDung;
    danhMuc;
    ngayDang;
    daDuyet;
    static _OPENAPI_METADATA_FACTORY() {
        return { nguoiDang: { required: true, type: () => require("../nguoi-dung/nguoi-dung.entity").NguoiDung }, tieuDe: { required: true, type: () => String }, tomTat: { required: true, type: () => String }, noiDung: { required: true, type: () => String }, danhMuc: { required: true, type: () => String }, ngayDang: { required: true, type: () => String }, daDuyet: { required: true, type: () => Boolean } };
    }
};
__decorate([
    mongoose_1.Prop({
        required: true,
        type: mongoose_2.Schema.Types.ObjectId,
        ref: 'nguoi_dung',
    }),
    __metadata("design:type", nguoi_dung_entity_1.NguoiDung)
], ThongBao.prototype, "nguoiDang", void 0);
__decorate([
    mongoose_1.Prop({ required: true }),
    __metadata("design:type", String)
], ThongBao.prototype, "tieuDe", void 0);
__decorate([
    mongoose_1.Prop({ required: true }),
    __metadata("design:type", String)
], ThongBao.prototype, "tomTat", void 0);
__decorate([
    mongoose_1.Prop({ required: true }),
    __metadata("design:type", String)
], ThongBao.prototype, "noiDung", void 0);
__decorate([
    mongoose_1.Prop({
        required: true,
        default: 'Khác',
    }),
    __metadata("design:type", String)
], ThongBao.prototype, "danhMuc", void 0);
__decorate([
    mongoose_1.Prop({ required: true }),
    __metadata("design:type", String)
], ThongBao.prototype, "ngayDang", void 0);
__decorate([
    mongoose_1.Prop({ default: true }),
    __metadata("design:type", Boolean)
], ThongBao.prototype, "daDuyet", void 0);
ThongBao = __decorate([
    mongoose_1.Schema({
        timestamps: {
            createdAt: 'thoiDiemTao',
            updatedAt: 'thoiDiemSua',
        },
        versionKey: false,
    })
], ThongBao);
exports.ThongBao = ThongBao;
exports.ThongBaoSchema = mongoose_1.SchemaFactory.createForClass(ThongBao);
//# sourceMappingURL=thong-bao.entity.js.map