"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ThongBaoController = void 0;
const openapi = require("@nestjs/swagger");
const common_1 = require("@nestjs/common");
const thong_bao_service_1 = require("./thong-bao.service");
const swagger_1 = require("@nestjs/swagger");
const auth_guard_1 = require("../../auth.guard");
const thong_bao_dto_1 = require("./thong-bao.dto");
let ThongBaoController = class ThongBaoController {
    service;
    constructor(service) {
        this.service = service;
    }
    async create(dto) {
        return await this.service.create(dto);
    }
    async findAll() {
        return await this.service.findAll();
    }
    async findBy(muc) {
        if (muc && muc != '') {
            switch (muc) {
                case 'hoat-dong':
                    return await this.service.getAll_byCatalog('Hoạt động');
                    break;
                case 'hoc-tap':
                    return await this.service.getAll_byCatalog('Học tập');
                    break;
                case 'hoc-phi':
                    return await this.service.getAll_byCatalog('Học phí');
                    break;
                default:
                    return await this.service.getAll_byCatalog('Khác');
                    break;
            }
        }
    }
    async findOne(id) {
        return await this.service.findOne(id);
    }
    async update(id, dto) {
        return await this.service.update(id, dto);
    }
    async remove(id) {
        return await this.service.remove(id);
    }
};
__decorate([
    common_1.Post(),
    swagger_1.ApiCreatedResponse({ description: 'Tạo thành công' }),
    openapi.ApiResponse({ status: 201, type: Object }),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [thong_bao_dto_1.ThongBaoDto]),
    __metadata("design:returntype", Promise)
], ThongBaoController.prototype, "create", null);
__decorate([
    common_1.Get(),
    swagger_1.ApiOkResponse({ description: 'Trả về tất cả' }),
    openapi.ApiResponse({ status: 200, type: [Object] }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], ThongBaoController.prototype, "findAll", null);
__decorate([
    common_1.Get('theo'),
    swagger_1.ApiQuery({
        name: 'muc',
        type: String,
        description: 'Danh mục cần tìm',
    }),
    swagger_1.ApiOkResponse({ description: 'Trả về tất cả, có chọn lọc' }),
    openapi.ApiResponse({ status: 200, type: [Object] }),
    __param(0, common_1.Query('muc')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], ThongBaoController.prototype, "findBy", null);
__decorate([
    common_1.Get(':id'),
    swagger_1.ApiParam({
        name: 'id',
        type: String,
        description: '_id của thông báo',
    }),
    swagger_1.ApiOkResponse({ description: 'Trả về 1 đối tượng' }),
    openapi.ApiResponse({ status: 200 }),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], ThongBaoController.prototype, "findOne", null);
__decorate([
    common_1.Patch(':id'),
    swagger_1.ApiParam({
        name: 'id',
        type: String,
        description: '_id của thông báo',
    }),
    swagger_1.ApiOkResponse({ description: 'Cập nhật thành công' }),
    openapi.ApiResponse({ status: 200, type: Object }),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, thong_bao_dto_1.ThongBaoDto]),
    __metadata("design:returntype", Promise)
], ThongBaoController.prototype, "update", null);
__decorate([
    common_1.Delete(':id'),
    swagger_1.ApiParam({
        name: 'id',
        type: String,
        description: '_id của thông báo',
    }),
    swagger_1.ApiOkResponse({ description: 'Xóa thành công' }),
    openapi.ApiResponse({ status: 200, type: Object }),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], ThongBaoController.prototype, "remove", null);
ThongBaoController = __decorate([
    common_1.Controller('thong-bao'),
    common_1.UseGuards(auth_guard_1.AuthGuard),
    swagger_1.ApiTags('thong-bao'),
    __metadata("design:paramtypes", [thong_bao_service_1.ThongBaoService])
], ThongBaoController);
exports.ThongBaoController = ThongBaoController;
//# sourceMappingURL=thong-bao.controller.js.map