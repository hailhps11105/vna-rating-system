"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BuoiHocSchema = exports.BuoiHoc = void 0;
const openapi = require("@nestjs/swagger");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const tuan_hoc_entity_1 = require("../tuan-hoc/tuan-hoc.entity");
let BuoiHoc = class BuoiHoc {
    thu;
    ngayHoc;
    tuanHoc;
    static _OPENAPI_METADATA_FACTORY() {
        return { thu: { required: true, type: () => String }, ngayHoc: { required: true, type: () => String }, tuanHoc: { required: true, type: () => require("../tuan-hoc/tuan-hoc.entity").TuanHoc } };
    }
};
__decorate([
    mongoose_1.Prop({ default: 'Chủ nhật', required: true }),
    __metadata("design:type", String)
], BuoiHoc.prototype, "thu", void 0);
__decorate([
    mongoose_1.Prop({ default: '1/1/1 AD', required: true }),
    __metadata("design:type", String)
], BuoiHoc.prototype, "ngayHoc", void 0);
__decorate([
    mongoose_1.Prop({
        required: true,
        type: mongoose_2.Schema.Types.ObjectId,
        ref: 'tuan_hoc',
    }),
    __metadata("design:type", tuan_hoc_entity_1.TuanHoc)
], BuoiHoc.prototype, "tuanHoc", void 0);
BuoiHoc = __decorate([
    mongoose_1.Schema({
        timestamps: {
            createdAt: 'thoiDiemTao',
            updatedAt: 'thoiDiemSua',
        },
        versionKey: false,
    })
], BuoiHoc);
exports.BuoiHoc = BuoiHoc;
exports.BuoiHocSchema = mongoose_1.SchemaFactory.createForClass(BuoiHoc);
//# sourceMappingURL=buoi-hoc.entity.js.map