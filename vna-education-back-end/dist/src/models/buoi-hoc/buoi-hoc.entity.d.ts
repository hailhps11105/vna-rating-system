import { Document, Schema as MongooseSchema } from 'mongoose';
import { TuanHoc } from '../tuan-hoc/tuan-hoc.entity';
export declare type BuoiHocDocument = BuoiHoc & Document;
export declare class BuoiHoc {
    thu: string;
    ngayHoc: string;
    tuanHoc: TuanHoc;
}
export declare const BuoiHocSchema: MongooseSchema<Document<BuoiHoc, any, any>, import("mongoose").Model<any, any, any>, undefined, any>;
