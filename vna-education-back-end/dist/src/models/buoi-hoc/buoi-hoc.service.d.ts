import { Model } from 'mongoose';
import { TuanHocService } from '../tuan-hoc/tuan-hoc.service';
import { BuoiHocDto } from './buoi-hoc.dto';
import { BuoiHocDocument } from './buoi-hoc.entity';
export declare class BuoiHocService {
    private model;
    private readonly tuanSer;
    constructor(model: Model<BuoiHocDocument>, tuanSer: TuanHocService);
    create(dto: BuoiHocDto): Promise<BuoiHocDocument>;
    findAll(): Promise<any[]>;
    getAll(condition?: any): Promise<any[]>;
    findOne(buoi: string): Promise<{
        _id: string;
        thu: string;
        ngayHoc: string;
        tuanHoc: {
            _id: any;
            soTuan: number;
        };
    }>;
    update(id: string, dto: BuoiHocDto): Promise<BuoiHocDocument>;
    objectify(buoi: string): Promise<any>;
    remove(id: string): Promise<BuoiHocDocument>;
}
