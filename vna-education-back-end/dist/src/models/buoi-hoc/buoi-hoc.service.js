"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BuoiHocService = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const utilities_1 = require("../../helpers/utilities");
const tuan_hoc_service_1 = require("../tuan-hoc/tuan-hoc.service");
let BuoiHocService = class BuoiHocService {
    model;
    tuanSer;
    constructor(model, tuanSer) {
        this.model = model;
        this.tuanSer = tuanSer;
    }
    async create(dto) {
        return await this.model.create({
            thu: dto.thu,
            ngayHoc: dto.ngayHoc,
            tuanHoc: mongoose_2.Types.ObjectId(dto.tuanHoc),
        });
    }
    async findAll() {
        const all = await this.model
            .find()
            .populate({
            path: 'tuanHoc',
            select: 'soTuan',
        })
            .exec();
        const result = [];
        for (let i = 0; i < all.length; i++) {
            result.push({
                _id: all[i]._id,
                thu: all[i].thu,
                ngayHoc: all[i].ngayHoc,
                tuanHoc: {
                    _id: all[i].populated('tuanHoc'),
                    soTuan: all[i].tuanHoc?.soTuan,
                },
            });
        }
        return result;
    }
    async getAll(condition = {}) {
        const all = await this.model
            .find(condition)
            .populate({
            path: 'tuanHoc',
            select: 'soTuan',
        })
            .exec();
        const result = [];
        for (let i = 0; i < all.length; i++) {
            result.push({
                _id: all[i]._id,
                thu: all[i].thu,
                ngayHoc: all[i].ngayHoc,
                tuanHoc: all[i].tuanHoc?.soTuan,
            });
        }
        return result;
    }
    async findOne(buoi) {
        const b = await (await this.model.findById(buoi))
            .populate({
            path: 'tuanHoc',
            select: 'soTuan',
        })
            .execPopulate();
        return {
            _id: buoi,
            thu: b.thu,
            ngayHoc: b.ngayHoc,
            tuanHoc: {
                _id: b.populated('tuanHoc'),
                soTuan: b.tuanHoc?.soTuan,
            },
        };
    }
    async update(id, dto) {
        const { tuanHoc, ...rest } = dto;
        return await this.model.findById(id, null, null, async (err, doc) => {
            if (err)
                throw err;
            utilities_1.assign(rest, doc);
            if (tuanHoc)
                doc.tuanHoc = await this.tuanSer.objectify(tuanHoc);
            await doc.save();
        });
    }
    async objectify(buoi) {
        return (await this.model.findById(buoi))._id;
    }
    async remove(id) {
        return this.model.findByIdAndDelete(id);
    }
};
BuoiHocService = __decorate([
    common_1.Injectable(),
    __param(0, mongoose_1.InjectModel('buoi_hoc')),
    __metadata("design:paramtypes", [mongoose_2.Model,
        tuan_hoc_service_1.TuanHocService])
], BuoiHocService);
exports.BuoiHocService = BuoiHocService;
//# sourceMappingURL=buoi-hoc.service.js.map