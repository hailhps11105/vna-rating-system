"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BuoiHocModule = void 0;
const common_1 = require("@nestjs/common");
const buoi_hoc_service_1 = require("./buoi-hoc.service");
const buoi_hoc_controller_1 = require("./buoi-hoc.controller");
const mongoose_1 = require("@nestjs/mongoose");
const buoi_hoc_entity_1 = require("./buoi-hoc.entity");
const nguoi_dung_module_1 = require("../nguoi-dung/nguoi-dung.module");
const tuan_hoc_module_1 = require("../tuan-hoc/tuan-hoc.module");
let BuoiHocModule = class BuoiHocModule {
};
BuoiHocModule = __decorate([
    common_1.Module({
        imports: [
            mongoose_1.MongooseModule.forFeature([
                {
                    name: 'buoi_hoc',
                    collection: 'buoi_hoc',
                    schema: buoi_hoc_entity_1.BuoiHocSchema,
                },
            ]),
            nguoi_dung_module_1.NguoiDungModule,
            tuan_hoc_module_1.TuanHocModule,
        ],
        controllers: [buoi_hoc_controller_1.BuoiHocController],
        providers: [buoi_hoc_service_1.BuoiHocService],
        exports: [buoi_hoc_service_1.BuoiHocService],
    })
], BuoiHocModule);
exports.BuoiHocModule = BuoiHocModule;
//# sourceMappingURL=buoi-hoc.module.js.map