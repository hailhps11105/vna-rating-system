import { BuoiHocService } from './buoi-hoc.service';
import { BuoiHocDto } from './buoi-hoc.dto';
export declare class BuoiHocController {
    private readonly service;
    constructor(service: BuoiHocService);
    create(dto: BuoiHocDto): Promise<import("./buoi-hoc.entity").BuoiHocDocument>;
    findAll(): Promise<any[]>;
    findOne(id: string): Promise<{
        _id: string;
        thu: string;
        ngayHoc: string;
        tuanHoc: {
            _id: any;
            soTuan: number;
        };
    }>;
    update(id: string, dto: BuoiHocDto): Promise<import("./buoi-hoc.entity").BuoiHocDocument>;
    remove(id: string): Promise<import("./buoi-hoc.entity").BuoiHocDocument>;
}
