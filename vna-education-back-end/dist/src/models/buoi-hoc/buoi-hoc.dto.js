"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BuoiHocDto = void 0;
const openapi = require("@nestjs/swagger");
class BuoiHocDto {
    thu;
    ngayHoc;
    tuanHoc;
    static _OPENAPI_METADATA_FACTORY() {
        return { thu: { required: true, type: () => String }, ngayHoc: { required: true, type: () => String }, tuanHoc: { required: true, type: () => String } };
    }
}
exports.BuoiHocDto = BuoiHocDto;
//# sourceMappingURL=buoi-hoc.dto.js.map