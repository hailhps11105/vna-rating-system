import { Model } from 'mongoose';
import { TuanHocService } from '../tuan-hoc/tuan-hoc.service';
import { NamHocDto } from './nam-hoc.dto';
import { NamHocDocument } from './nam-hoc.entity';
export declare class NamHocService {
    private model;
    private readonly tuanSer;
    constructor(model: Model<NamHocDocument>, tuanSer: TuanHocService);
    create(dto: NamHocDto): Promise<NamHocDocument>;
    getOne(nam: string): Promise<any>;
    getAll(): Promise<any[]>;
    getLatest(): Promise<any>;
    getLatest_latestWeek(): Promise<any>;
    update(id: string, dto: NamHocDto): Promise<NamHocDocument>;
    addWeeks(id: string, weeks: string[]): Promise<NamHocDocument>;
    objectify(nam: string): Promise<any>;
    remove(id: string): Promise<NamHocDocument>;
}
