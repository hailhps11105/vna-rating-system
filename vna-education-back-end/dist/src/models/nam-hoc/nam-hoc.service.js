"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NamHocService = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const utilities_1 = require("../../helpers/utilities");
const tuan_hoc_service_1 = require("../tuan-hoc/tuan-hoc.service");
let NamHocService = class NamHocService {
    model;
    tuanSer;
    constructor(model, tuanSer) {
        this.model = model;
        this.tuanSer = tuanSer;
    }
    async create(dto) {
        const { tuanHoc, ...rest } = dto;
        const to404 = await this.model.find({
            tenNam: dto.tenNam,
            namBatDau: dto.namBatDau,
            namKetThuc: dto.namKetThuc,
        });
        if (to404.length > 0)
            return null;
        else {
            return await this.model.create({
                ...rest,
                tuanHoc: utilities_1.bulkObjectID(tuanHoc),
            });
        }
    }
    async getOne(nam) {
        const agg = await this.model.aggregate([
            { $match: { _id: mongoose_2.Types.ObjectId(nam) } },
            {
                $lookup: {
                    from: 'tuan_hoc',
                    localField: 'tuanHoc',
                    foreignField: '_id',
                    as: 'tuanHoc',
                },
            },
            {
                $project: {
                    tenNam: 1,
                    namBatDau: 1,
                    namKetThuc: 1,
                    tuanHoc: 1,
                },
            },
        ]);
        return agg[0];
    }
    async getAll() {
        return await this.model.aggregate([
            {
                $lookup: {
                    from: 'tuan_hoc',
                    localField: 'tuanHoc',
                    foreignField: '_id',
                    as: 'tuanHoc',
                },
            },
            {
                $project: {
                    tenNam: 1,
                    namBatDau: 1,
                    namKetThuc: 1,
                    tuanHoc: 1,
                },
            },
        ]);
    }
    async getLatest() {
        const agg = await this.getAll();
        const thisYear = new Date().getFullYear();
        for (let i = 0; i < agg.length; i++) {
            if (agg[i].namBatDau <= thisYear && agg[i].namKetThuc >= thisYear) {
                agg[i].tuanHoc.sort((a, b) => {
                    return b.soTuan - a.soTuan;
                });
                return agg[i];
            }
        }
    }
    async getLatest_latestWeek() {
        const latest = await this.getLatest();
        return latest.tuanHoc[0];
    }
    async update(id, dto) {
        const { tuanHoc, ...rest } = dto;
        return await this.model.findById(id, null, null, async (err, doc) => {
            if (err)
                throw err;
            utilities_1.assign(rest, doc);
            if (tuanHoc && tuanHoc.length > 0)
                doc.tuanHoc = await this.tuanSer.bulkObjectify(tuanHoc);
            await doc.save();
        });
    }
    async addWeeks(id, weeks) {
        return await this.model.findByIdAndUpdate(id, {
            $push: { tuanHoc: { $each: utilities_1.bulkObjectID(weeks) } },
        });
    }
    async objectify(nam) {
        return (await this.model.findById(nam))._id;
    }
    async remove(id) {
        return this.model.findByIdAndDelete(id);
    }
};
NamHocService = __decorate([
    common_1.Injectable(),
    __param(0, mongoose_1.InjectModel('nam_hoc')),
    __metadata("design:paramtypes", [mongoose_2.Model,
        tuan_hoc_service_1.TuanHocService])
], NamHocService);
exports.NamHocService = NamHocService;
//# sourceMappingURL=nam-hoc.service.js.map