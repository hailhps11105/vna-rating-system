"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NamHocDto = void 0;
const openapi = require("@nestjs/swagger");
class NamHocDto {
    tenNam;
    namBatDau;
    namKetThuc;
    tuanHoc;
    static _OPENAPI_METADATA_FACTORY() {
        return { tenNam: { required: true, type: () => String }, namBatDau: { required: true, type: () => Number }, namKetThuc: { required: true, type: () => Number }, tuanHoc: { required: true, type: () => [String] } };
    }
}
exports.NamHocDto = NamHocDto;
//# sourceMappingURL=nam-hoc.dto.js.map