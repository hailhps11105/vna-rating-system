import { NamHocService } from './nam-hoc.service';
import { NamHocDto } from './nam-hoc.dto';
export declare class NamHocController {
    private readonly service;
    constructor(service: NamHocService);
    create(dto: NamHocDto): Promise<import("./nam-hoc.entity").NamHocDocument>;
    findAll(): Promise<any[]>;
    findLatest(): Promise<any>;
    findLatestWeek(): Promise<any>;
    findOne(id: string): Promise<any>;
    addWeek(id: string, weeks: string[]): Promise<import("./nam-hoc.entity").NamHocDocument>;
    update(id: string, dto: NamHocDto): Promise<import("./nam-hoc.entity").NamHocDocument>;
    remove(id: string): Promise<import("./nam-hoc.entity").NamHocDocument>;
}
