"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NamHocSchema = exports.NamHoc = void 0;
const openapi = require("@nestjs/swagger");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
let NamHoc = class NamHoc {
    tenNam;
    namBatDau;
    namKetThuc;
    tuanHoc;
    static _OPENAPI_METADATA_FACTORY() {
        return { tenNam: { required: true, type: () => String }, namBatDau: { required: true, type: () => Number }, namKetThuc: { required: true, type: () => Number }, tuanHoc: { required: true, type: () => [require("../tuan-hoc/tuan-hoc.entity").TuanHoc] } };
    }
};
__decorate([
    mongoose_1.Prop({ default: '', required: true }),
    __metadata("design:type", String)
], NamHoc.prototype, "tenNam", void 0);
__decorate([
    mongoose_1.Prop({ default: 1 }),
    __metadata("design:type", Number)
], NamHoc.prototype, "namBatDau", void 0);
__decorate([
    mongoose_1.Prop({ default: 1 }),
    __metadata("design:type", Number)
], NamHoc.prototype, "namKetThuc", void 0);
__decorate([
    mongoose_1.Prop({ type: [{ type: mongoose_2.Schema.Types.ObjectId }], default: [] }),
    __metadata("design:type", Array)
], NamHoc.prototype, "tuanHoc", void 0);
NamHoc = __decorate([
    mongoose_1.Schema({
        timestamps: {
            createdAt: 'thoiDiemTao',
            updatedAt: 'thoiDiemSua',
        },
        versionKey: false,
    })
], NamHoc);
exports.NamHoc = NamHoc;
exports.NamHocSchema = mongoose_1.SchemaFactory.createForClass(NamHoc);
//# sourceMappingURL=nam-hoc.entity.js.map