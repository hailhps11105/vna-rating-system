import { Document, Schema as MongooseSchema } from 'mongoose';
import { TuanHoc } from '../tuan-hoc/tuan-hoc.entity';
export declare type NamHocDocument = NamHoc & Document;
export declare class NamHoc {
    tenNam: string;
    namBatDau: number;
    namKetThuc: number;
    tuanHoc: TuanHoc[];
}
export declare const NamHocSchema: MongooseSchema<Document<NamHoc, any, any>, import("mongoose").Model<any, any, any>, undefined, any>;
