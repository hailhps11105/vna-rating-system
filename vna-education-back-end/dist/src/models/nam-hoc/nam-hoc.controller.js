"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NamHocController = void 0;
const openapi = require("@nestjs/swagger");
const common_1 = require("@nestjs/common");
const nam_hoc_service_1 = require("./nam-hoc.service");
const auth_guard_1 = require("../../auth.guard");
const swagger_1 = require("@nestjs/swagger");
const nam_hoc_dto_1 = require("./nam-hoc.dto");
let NamHocController = class NamHocController {
    service;
    constructor(service) {
        this.service = service;
    }
    async create(dto) {
        return await this.service.create(dto);
    }
    async findAll() {
        return await this.service.getAll();
    }
    async findLatest() {
        return await this.service.getLatest();
    }
    async findLatestWeek() {
        return await this.service.getLatest_latestWeek();
    }
    async findOne(id) {
        return await this.service.getOne(id);
    }
    async addWeek(id, weeks) {
        return await this.service.addWeeks(id, weeks);
    }
    async update(id, dto) {
        return await this.service.update(id, dto);
    }
    async remove(id) {
        return await this.service.remove(id);
    }
};
__decorate([
    common_1.Post(),
    openapi.ApiResponse({ status: 201, type: Object }),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [nam_hoc_dto_1.NamHocDto]),
    __metadata("design:returntype", Promise)
], NamHocController.prototype, "create", null);
__decorate([
    common_1.Get(),
    openapi.ApiResponse({ status: 200, type: [Object] }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], NamHocController.prototype, "findAll", null);
__decorate([
    common_1.Get('gan-nhat'),
    openapi.ApiResponse({ status: 200, type: Object }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], NamHocController.prototype, "findLatest", null);
__decorate([
    common_1.Get('gan-nhat/tuan-gan-nhat'),
    openapi.ApiResponse({ status: 200, type: Object }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], NamHocController.prototype, "findLatestWeek", null);
__decorate([
    common_1.Get(':id'),
    openapi.ApiResponse({ status: 200, type: Object }),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], NamHocController.prototype, "findOne", null);
__decorate([
    common_1.Patch(':id/them-tuan'),
    openapi.ApiResponse({ status: 200, type: Object }),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Array]),
    __metadata("design:returntype", Promise)
], NamHocController.prototype, "addWeek", null);
__decorate([
    common_1.Patch(':id'),
    openapi.ApiResponse({ status: 200, type: Object }),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, nam_hoc_dto_1.NamHocDto]),
    __metadata("design:returntype", Promise)
], NamHocController.prototype, "update", null);
__decorate([
    common_1.Delete(':id'),
    openapi.ApiResponse({ status: 200, type: Object }),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], NamHocController.prototype, "remove", null);
NamHocController = __decorate([
    common_1.Controller('nam-hoc'),
    swagger_1.ApiTags('nam-hoc'),
    common_1.UseGuards(auth_guard_1.AuthGuard),
    __metadata("design:paramtypes", [nam_hoc_service_1.NamHocService])
], NamHocController);
exports.NamHocController = NamHocController;
//# sourceMappingURL=nam-hoc.controller.js.map