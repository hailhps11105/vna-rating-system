"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LopHocDto = void 0;
const openapi = require("@nestjs/swagger");
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
class LopHocDto {
    maLH;
    namHoc;
    GVCN;
    hocSinh;
    static _OPENAPI_METADATA_FACTORY() {
        return { maLH: { required: true, type: () => String }, namHoc: { required: true, type: () => String }, GVCN: { required: true, type: () => String }, hocSinh: { required: true, type: () => [String] } };
    }
}
__decorate([
    swagger_1.ApiProperty({
        name: 'maLH',
        type: String,
        required: true,
        example: '13A4',
        description: 'Tên lớp học',
    }),
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], LopHocDto.prototype, "maLH", void 0);
__decorate([
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], LopHocDto.prototype, "namHoc", void 0);
__decorate([
    swagger_1.ApiProperty({
        name: 'GVCN',
        type: String,
        required: true,
        example: '60bxxxxxxxxxxxxx',
        description: 'Giáo viên chủ nhiệm của lớp',
    }),
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], LopHocDto.prototype, "GVCN", void 0);
__decorate([
    swagger_1.ApiProperty({
        name: 'hocSinh',
        type: 'array',
        required: true,
        example: "['60bxxxxxxxxxxxxx', '60bxxxxxxxxxxxxy']",
        description: 'Các học sinh của lớp',
        minItems: 1,
        uniqueItems: true,
        default: [],
    }),
    class_validator_1.IsArray(),
    __metadata("design:type", Array)
], LopHocDto.prototype, "hocSinh", void 0);
exports.LopHocDto = LopHocDto;
//# sourceMappingURL=lop-hoc.dto.js.map