"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LopHocSchema = exports.LopHoc = void 0;
const openapi = require("@nestjs/swagger");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const nam_hoc_entity_1 = require("../nam-hoc/nam-hoc.entity");
const nguoi_dung_entity_1 = require("../nguoi-dung/nguoi-dung.entity");
let LopHoc = class LopHoc {
    maLH;
    namHoc;
    GVCN;
    hocSinh;
    static _OPENAPI_METADATA_FACTORY() {
        return { maLH: { required: true, type: () => String }, namHoc: { required: true, type: () => require("../nam-hoc/nam-hoc.entity").NamHoc }, GVCN: { required: true, type: () => require("../nguoi-dung/nguoi-dung.entity").NguoiDung }, hocSinh: { required: true, type: () => [require("../nguoi-dung/nguoi-dung.entity").NguoiDung] } };
    }
};
__decorate([
    mongoose_1.Prop({ required: true, index: true, unique: true }),
    __metadata("design:type", String)
], LopHoc.prototype, "maLH", void 0);
__decorate([
    mongoose_1.Prop({
        required: true,
        type: mongoose_2.Schema.Types.ObjectId,
        ref: 'nam_hoc',
    }),
    __metadata("design:type", nam_hoc_entity_1.NamHoc)
], LopHoc.prototype, "namHoc", void 0);
__decorate([
    mongoose_1.Prop({
        required: true,
        type: mongoose_2.Schema.Types.ObjectId,
        ref: 'nguoi_dung',
    }),
    __metadata("design:type", nguoi_dung_entity_1.NguoiDung)
], LopHoc.prototype, "GVCN", void 0);
__decorate([
    mongoose_1.Prop({
        default: [],
        type: [
            {
                type: mongoose_2.Schema.Types.ObjectId,
                ref: 'nguoi_dung',
            },
        ],
    }),
    __metadata("design:type", Array)
], LopHoc.prototype, "hocSinh", void 0);
LopHoc = __decorate([
    mongoose_1.Schema({
        timestamps: {
            createdAt: 'thoiDiemTao',
            updatedAt: 'thoiDiemSua',
        },
        versionKey: false,
    })
], LopHoc);
exports.LopHoc = LopHoc;
exports.LopHocSchema = mongoose_1.SchemaFactory.createForClass(LopHoc);
//# sourceMappingURL=lop-hoc.entity.js.map