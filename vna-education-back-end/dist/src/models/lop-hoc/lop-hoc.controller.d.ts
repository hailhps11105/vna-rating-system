import { LopHocService } from './lop-hoc.service';
import { LopHocDto } from './lop-hoc.dto';
export declare class LopHocController {
    private readonly service;
    constructor(service: LopHocService);
    create(dto: LopHocDto): Promise<import("./lop-hoc.entity").LopHocDocument>;
    findAll(): Promise<any[]>;
    findOne(id: string): Promise<{
        _id: string;
        maLH: string;
        namHoc: {
            _id: any;
            tenNam: string;
        };
        GVCN: {
            _id: any;
            hoTen: string;
        };
        hocSinh: {
            _id: any;
            hoTen: string;
        }[];
    }>;
    update(id: string, dto: LopHocDto): Promise<import("./lop-hoc.entity").LopHocDocument>;
    remove(id: string): Promise<import("./lop-hoc.entity").LopHocDocument>;
}
