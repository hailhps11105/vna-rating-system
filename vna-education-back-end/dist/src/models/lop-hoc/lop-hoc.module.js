"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LopHocModule = void 0;
const common_1 = require("@nestjs/common");
const lop_hoc_service_1 = require("./lop-hoc.service");
const lop_hoc_controller_1 = require("./lop-hoc.controller");
const mongoose_1 = require("@nestjs/mongoose");
const lop_hoc_entity_1 = require("./lop-hoc.entity");
const nguoi_dung_module_1 = require("../nguoi-dung/nguoi-dung.module");
const nam_hoc_module_1 = require("../nam-hoc/nam-hoc.module");
let LopHocModule = class LopHocModule {
};
LopHocModule = __decorate([
    common_1.Module({
        imports: [
            mongoose_1.MongooseModule.forFeature([
                {
                    name: 'lop_hoc',
                    collection: 'lop_hoc',
                    schema: lop_hoc_entity_1.LopHocSchema,
                },
            ]),
            common_1.forwardRef(() => nguoi_dung_module_1.NguoiDungModule),
            nam_hoc_module_1.NamHocModule,
        ],
        controllers: [lop_hoc_controller_1.LopHocController],
        providers: [lop_hoc_service_1.LopHocService],
        exports: [lop_hoc_service_1.LopHocService],
    })
], LopHocModule);
exports.LopHocModule = LopHocModule;
//# sourceMappingURL=lop-hoc.module.js.map