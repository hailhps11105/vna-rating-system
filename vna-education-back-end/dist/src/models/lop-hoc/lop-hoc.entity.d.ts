import { Document, Schema as MongooseSchema } from 'mongoose';
import { NamHoc } from '../nam-hoc/nam-hoc.entity';
import { NguoiDung } from '../nguoi-dung/nguoi-dung.entity';
export declare type LopHocDocument = LopHoc & Document;
export declare class LopHoc {
    maLH: string;
    namHoc: NamHoc;
    GVCN: NguoiDung;
    hocSinh: NguoiDung[];
}
export declare const LopHocSchema: MongooseSchema<Document<LopHoc, any, any>, import("mongoose").Model<any, any, any>, undefined, any>;
