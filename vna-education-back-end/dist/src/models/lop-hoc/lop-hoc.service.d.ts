import { Model } from 'mongoose';
import { NamHocService } from '../nam-hoc/nam-hoc.service';
import { NguoiDungService } from '../nguoi-dung/nguoi-dung.service';
import { LopHocDto } from './lop-hoc.dto';
import { LopHocDocument } from './lop-hoc.entity';
export declare class LopHocService {
    private model;
    private readonly ndSer;
    private readonly namSer;
    constructor(model: Model<LopHocDocument>, ndSer: NguoiDungService, namSer: NamHocService);
    create(dto: LopHocDto): Promise<LopHocDocument>;
    findAll(condition?: any): Promise<any[]>;
    findOne(lop: string): Promise<{
        _id: string;
        maLH: string;
        namHoc: {
            _id: any;
            tenNam: string;
        };
        GVCN: {
            _id: any;
            hoTen: string;
        };
        hocSinh: {
            _id: any;
            hoTen: string;
        }[];
    }>;
    addBulkHS(hs: string[], lop: string): Promise<LopHocDocument>;
    addOneHS(hs: string, lop: string): Promise<any[] | LopHocDocument>;
    onlyHS(lop: string): Promise<any[]>;
    onlyGV(): Promise<any[]>;
    update(id: string, dto: LopHocDto): Promise<LopHocDocument>;
    objectify_fromName(lop: string): Promise<any>;
    objectify_fromID(lop: string): Promise<any>;
    remove(id: string): Promise<LopHocDocument>;
}
