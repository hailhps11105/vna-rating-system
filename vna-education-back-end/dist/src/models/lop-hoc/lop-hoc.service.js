"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LopHocService = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const utilities_1 = require("../../helpers/utilities");
const nam_hoc_service_1 = require("../nam-hoc/nam-hoc.service");
const nguoi_dung_service_1 = require("../nguoi-dung/nguoi-dung.service");
let LopHocService = class LopHocService {
    model;
    ndSer;
    namSer;
    constructor(model, ndSer, namSer) {
        this.model = model;
        this.ndSer = ndSer;
        this.namSer = namSer;
    }
    async create(dto) {
        return await this.model.create({
            maLH: dto.maLH,
            namHoc: mongoose_2.Types.ObjectId(dto.namHoc),
            GVCN: mongoose_2.Types.ObjectId(dto.GVCN),
            hocSinh: utilities_1.bulkObjectID(dto.hocSinh),
        });
    }
    async findAll(condition = {}) {
        const all = await this.model
            .find(condition)
            .populate([
            { path: 'GVCN', select: 'hoTen' },
            { path: 'namHoc', select: 'tenNam' },
            { path: 'hocSinh', select: 'hoTen' },
        ])
            .exec();
        const result = [];
        for (let i = 0; i < all.length; i++) {
            result.push({
                _id: all[i]._id,
                maLH: all[i].maLH,
                namHoc: all[i].namHoc
                    ? {
                        _id: all[i].populated('namHoc'),
                        tenNam: all[i].namHoc.tenNam,
                    }
                    : null,
                GVCN: all[i].GVCN
                    ? {
                        _id: all[i].populated('GVCN'),
                        hoTen: all[i].GVCN.hoTen,
                    }
                    : null,
                hocSinh: all[i].hocSinh?.map((val, index) => {
                    return {
                        _id: all[i].populated('hocSinh')[index],
                        hoTen: val.hoTen,
                    };
                }),
            });
        }
        return result;
    }
    async findOne(lop) {
        const classe = await (await this.model.findById(lop))
            .populate([
            { path: 'GVCN', select: 'hoTen' },
            { path: 'namHoc', select: 'tenNam' },
            { path: 'hocSinh', select: 'hoTen' },
        ])
            .execPopulate();
        return {
            _id: lop,
            maLH: classe.maLH,
            namHoc: classe.namHoc
                ? {
                    _id: classe.populated('namHoc'),
                    tenNam: classe.namHoc.tenNam,
                }
                : null,
            GVCN: classe.GVCN
                ? {
                    _id: classe.populated('GVCN'),
                    hoTen: classe.GVCN.hoTen,
                }
                : null,
            hocSinh: classe.hocSinh?.map((val, index) => {
                return {
                    _id: classe.populated('hocSinh')[index],
                    hoTen: val.hoTen,
                };
            }),
        };
    }
    async addBulkHS(hs, lop) {
        const classe = await this.onlyHS(lop);
        const students = await this.ndSer.groupInfo(hs);
        const toAdd = [];
        for (let i = 0; i < classe.length; i++) {
            for (let j = 0; j < students.length; j++) {
                if (students[j].maND !== classe[i].maND)
                    toAdd.push(students[i]._id);
            }
        }
        return await this.model.findByIdAndUpdate(lop, {
            $push: { hocSinh: { $each: toAdd } },
        }, { new: true });
    }
    async addOneHS(hs, lop) {
        let isDupl = false;
        const classe = await this.onlyHS(lop);
        const student = await this.ndSer.quickInfo(hs);
        for (let i = 0; i < classe.length; i++) {
            if (student[0].maND === classe[i]) {
                isDupl = true;
                break;
            }
        }
        return !isDupl
            ? await this.model.findByIdAndUpdate(lop, {
                $push: { hocSinh: mongoose_2.Types.ObjectId(hs) },
            }, { new: true })
            : classe;
    }
    async onlyHS(lop) {
        const result = [];
        const p = await (await this.model.findById(lop))
            .populate({ path: 'hocSinh', select: ['maND', 'hoTen'] })
            .execPopulate();
        for (let i = 0; i < p.hocSinh.length; i++) {
            const id = p.populated('hocSinh')[i];
            if (p.hocSinh[i] && mongoose_2.isValidObjectId(id))
                result.push({
                    _id: id,
                    maND: p.hocSinh[i].maND,
                    hoTen: p.hocSinh[i].hoTen,
                });
        }
        return result;
    }
    async onlyGV() {
        const all = await this.model
            .find()
            .populate({
            path: 'GVCN',
            select: ['maND', 'hoTen'],
        })
            .exec();
        const result = [];
        for (let i = 0; i < all.length; i++) {
            result.push({
                chuNhiem: all[i].maLH,
                GVCN: all[i].GVCN
                    ? {
                        _id: all[i].populated('GVCN'),
                        maND: all[i].GVCN.maND,
                        hoTen: all[i].GVCN.hoTen,
                    }
                    : null,
            });
        }
        return result;
    }
    async update(id, dto) {
        return await this.model.findById(id, null, null, async (err, doc) => {
            if (err)
                throw err;
            if (dto.maLH)
                doc.maLH = dto.maLH;
            if (dto.namHoc)
                doc.namHoc = await this.namSer.objectify(dto.namHoc);
            if (dto.GVCN)
                doc.GVCN = await this.ndSer.objectify(dto.GVCN);
            if (dto.hocSinh)
                doc.hocSinh = await this.ndSer.bulkObjectify(dto.hocSinh);
            await doc.save();
        });
    }
    async objectify_fromName(lop) {
        const one = await this.model.findOne({ maLH: lop }, null, null, async (err, doc) => {
            if (err) {
                console.log(err);
                return null;
            }
            else
                return doc;
        });
        return one ? one._id : null;
    }
    async objectify_fromID(lop) {
        const one = await this.model.findById(lop, null, null, async (err, doc) => {
            if (err) {
                console.log(err);
                return null;
            }
            else
                return doc;
        });
        return one ? one._id : null;
    }
    async remove(id) {
        return await this.model.findByIdAndDelete(id);
    }
};
LopHocService = __decorate([
    common_1.Injectable(),
    __param(0, mongoose_1.InjectModel('lop_hoc')),
    __param(1, common_1.Inject(common_1.forwardRef(() => nguoi_dung_service_1.NguoiDungService))),
    __metadata("design:paramtypes", [mongoose_2.Model,
        nguoi_dung_service_1.NguoiDungService,
        nam_hoc_service_1.NamHocService])
], LopHocService);
exports.LopHocService = LopHocService;
//# sourceMappingURL=lop-hoc.service.js.map