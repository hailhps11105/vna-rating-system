import { Model } from 'mongoose';
import { ChangePassDTO, SetPassDTO } from '../../../helpers/changePass.dto';
import { NguoiDungDocument } from '../nguoi-dung.entity';
export declare class AccountService {
    private model;
    constructor(model: Model<NguoiDungDocument>);
    getOne_bymaND(ma: string): Promise<{
        _id: any;
        maND: string;
        hoTen: string;
        emailND: string;
        soDienThoai: string;
        dangHoatDong: boolean;
    }>;
    onlyPassword(ma: string): Promise<string>;
    changePass(dto: ChangePassDTO): Promise<{
        msg: string;
        checkOK: boolean;
    }>;
    setPass(dto: SetPassDTO): Promise<{
        msg: string;
        checkOK: boolean;
    }>;
    toggleActivation(account: string, state: boolean): Promise<NguoiDungDocument>;
    findOne_byEmail(email: string): Promise<{
        _id: any;
        maND: string;
        hoTen: string;
        soDienThoai: string;
        dangHoatDong: boolean;
    }>;
}
