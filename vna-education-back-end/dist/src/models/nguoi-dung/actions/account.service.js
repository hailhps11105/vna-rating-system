"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AccountService = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const bcrypt_1 = require("bcrypt");
const mongoose_2 = require("mongoose");
let AccountService = class AccountService {
    model;
    constructor(model) {
        this.model = model;
    }
    async getOne_bymaND(ma) {
        const user = await this.model.findOne({ maND: ma });
        return {
            _id: user._id,
            maND: user.maND,
            hoTen: user.hoTen,
            emailND: user.emailND,
            soDienThoai: user.soDienThoai ? user.soDienThoai : null,
            dangHoatDong: user.dangHoatDong,
        };
    }
    async onlyPassword(ma) {
        const user = await this.model.findOne({ maND: ma }, null, null, (err, doc) => {
            if (err)
                return null;
            else
                return doc;
        });
        if (user)
            return user.matKhau;
        else
            return null;
    }
    async changePass(dto) {
        if (!mongoose_2.isValidObjectId(dto.idUser))
            return {
                msg: '_id người dùng không hợp lệ',
                checkOK: false,
            };
        const user = await this.model.aggregate([
            { $match: { _id: mongoose_2.Types.ObjectId(dto.idUser) } },
            {
                $project: {
                    maND: 1,
                    matKhau: 1,
                },
            },
        ]);
        if (user[0]) {
            if (await bcrypt_1.compare(dto.oldPass, user[0].matKhau)) {
                await this.model.findByIdAndUpdate(user[0]._id, { $set: { matKhau: bcrypt_1.hashSync(dto.newPass, 10) } }, { new: true });
                return {
                    msg: 'Đổi mật khẩu thành công!',
                    checkOK: true,
                };
            }
            else
                return {
                    msg: 'Mật khẩu cũ không đúng!',
                    checkOK: false,
                };
        }
        else
            return {
                msg: 'Người dùng không tồn tại!',
                checkOK: false,
            };
    }
    async setPass(dto) {
        if (!mongoose_2.isValidObjectId(dto.idUser))
            return {
                msg: '_id người dùng không hợp lệ',
                checkOK: false,
            };
        const user = await this.model.findById(dto.idUser);
        if (user) {
            await this.model.findByIdAndUpdate(dto.idUser, { $set: { matKhau: bcrypt_1.hashSync(dto.newPass, 10) } }, { new: true });
            return {
                msg: 'Đặt mật khẩu mới thành công!',
                checkOK: true,
            };
        }
        else
            return {
                msg: 'Người dùng không tồn tại!',
                checkOK: false,
            };
    }
    async toggleActivation(account, state) {
        return await this.model.findByIdAndUpdate(account, {
            $set: { dangHoatDong: state },
        }, { new: true });
    }
    async findOne_byEmail(email) {
        const user = await this.model.findOne({ emailND: email });
        return {
            _id: user._id,
            maND: user.maND,
            hoTen: user.hoTen,
            soDienThoai: user.soDienThoai ? user.soDienThoai : null,
            dangHoatDong: user.dangHoatDong,
        };
    }
};
AccountService = __decorate([
    common_1.Injectable(),
    __param(0, mongoose_1.InjectModel('nguoi_dung')),
    __metadata("design:paramtypes", [mongoose_2.Model])
], AccountService);
exports.AccountService = AccountService;
//# sourceMappingURL=account.service.js.map