import { NguoiDungService } from './nguoi-dung.service';
import { NguoiDungDto } from './nguoi-dung.dto';
export declare class NguoiDungController {
    private readonly service;
    constructor(service: NguoiDungService);
    create(dto: NguoiDungDto): Promise<import("./nguoi-dung.entity").NguoiDungDocument>;
    findAll_according(role: string, lop: string): Promise<void | any[]>;
    import(dto: NguoiDungDto[]): Promise<any[]>;
    findAll(): Promise<any[]>;
    findOne_byID(id: string): Promise<{
        _id: string;
        maND: string;
        hoTen: string;
        emailND: string;
        diaChi: string;
        ngaySinh: string;
        noiSinh: string;
        gioiTinh: string;
        soDienThoai: string;
        dangHoatDong: boolean;
        quocTich: string;
        danToc: string;
        cccd: {
            maSo: string;
            ngayCap: string;
            noiCap: string;
        };
        hoChieu: {
            maSo: string;
            ngayCap: string;
            noiCap: string;
        };
        ngayNhapHoc: string;
        lopHoc: {
            _id: any;
            maLH: import("../lop-hoc/lop-hoc.entity").LopHoc;
        };
        chuNhiem: {
            _id: any;
            maLH: import("../lop-hoc/lop-hoc.entity").LopHoc;
        };
        chucVu: {
            chucVu: string;
            hopDong: string;
            trinhDo: string;
        };
    }>;
    update(id: string, dto: NguoiDungDto): Promise<import("./nguoi-dung.entity").NguoiDungDocument>;
    remove(id: string): Promise<import("./nguoi-dung.entity").NguoiDungDocument>;
}
