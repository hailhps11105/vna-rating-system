export declare class NguoiDungDto {
    maND: string;
    hoTen: string;
    emailND: string;
    matKhau: string;
    soDienThoai: string;
    ngaySinh: string;
    noiSinh: string;
    diaChi: string;
    gioiTinh: string;
    danToc: string;
    quocTich: string;
    cccd: string;
    ngayCap: string;
    noiCap: string;
    chucVu: string;
    hopDong: string;
    tDCM: string;
    lopHoc: string;
    ngayNhapHoc: string;
    dangHoatDong: boolean;
    chuNhiem: string;
    conCai: string[];
}
