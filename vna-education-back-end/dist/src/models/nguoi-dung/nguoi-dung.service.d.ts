import { Model } from 'mongoose';
import { RoleType } from '../../helpers/utilities';
import { LopHocService } from '../lop-hoc/lop-hoc.service';
import { NguoiDungDto } from './nguoi-dung.dto';
import { NguoiDungDocument } from './nguoi-dung.entity';
export declare class NguoiDungService {
    private model;
    private readonly lhSer;
    constructor(model: Model<NguoiDungDocument>, lhSer: LopHocService);
    create(dto: NguoiDungDto): Promise<NguoiDungDocument>;
    bulkCreate(dto: NguoiDungDto[]): Promise<any[]>;
    findAll(condition?: any): Promise<any[]>;
    getAll(condition?: any): Promise<void>;
    findAll_byRole(role: RoleType): Promise<any[]>;
    groupInfo(group: string[]): Promise<any[]>;
    quickInfo(user: string): Promise<any[]>;
    classCount(classe: string): Promise<number>;
    findAll_byClass(lop: string): Promise<void>;
    getOne_byID(nd: string): Promise<{
        maND: string;
        hoTen: string;
        emailND: string;
        diaChi: string;
        ngaySinh: string;
        noiSinh: string;
        gioiTinh: string;
        soDienThoai: string;
        dangHoatDong: boolean;
        quocTich: string;
        danToc: string;
        cccd: {
            maSo: string;
            ngayCap: string;
            noiCap: string;
        };
        hoChieu: {
            maSo: string;
            ngayCap: string;
            noiCap: string;
        };
        hocTap: {
            idLop: any;
            ngayNhapHoc: string;
            GVCN: string;
            lopHoc: string;
        };
        chuNhiem: string;
        chucVu: {
            chucVu: string;
            hopDong: string;
            trinhDo: string;
        };
    }>;
    findOne_byID(nd: string): Promise<{
        _id: string;
        maND: string;
        hoTen: string;
        emailND: string;
        diaChi: string;
        ngaySinh: string;
        noiSinh: string;
        gioiTinh: string;
        soDienThoai: string;
        dangHoatDong: boolean;
        quocTich: string;
        danToc: string;
        cccd: {
            maSo: string;
            ngayCap: string;
            noiCap: string;
        };
        hoChieu: {
            maSo: string;
            ngayCap: string;
            noiCap: string;
        };
        ngayNhapHoc: string;
        lopHoc: {
            _id: any;
            maLH: import("../lop-hoc/lop-hoc.entity").LopHoc;
        };
        chuNhiem: {
            _id: any;
            maLH: import("../lop-hoc/lop-hoc.entity").LopHoc;
        };
        chucVu: {
            chucVu: string;
            hopDong: string;
            trinhDo: string;
        };
    }>;
    update(id: string, dto: NguoiDungDto): Promise<NguoiDungDocument>;
    objectify(user: string): Promise<any>;
    bulkObjectify(user: string[]): Promise<any[]>;
    remove(id: string): Promise<NguoiDungDocument>;
}
