"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NguoiDungSchema = exports.NguoiDung = void 0;
const openapi = require("@nestjs/swagger");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const lop_hoc_entity_1 = require("../lop-hoc/lop-hoc.entity");
const giayTo_schema_1 = require("./giayTo.schema");
const laoDong_schema_1 = require("./laoDong.schema");
let NguoiDung = class NguoiDung {
    maND;
    hoTen;
    emailND;
    matKhau;
    soDienThoai;
    ngaySinh;
    noiSinh;
    diaChi;
    gioiTinh;
    danToc;
    quocTich;
    cccd;
    hoChieu;
    chucVu;
    lopHoc;
    ngayNhapHoc;
    chuNhiem;
    dangHoatDong;
    conCai;
    static _OPENAPI_METADATA_FACTORY() {
        return { maND: { required: true, type: () => String }, hoTen: { required: true, type: () => String }, emailND: { required: true, type: () => String }, matKhau: { required: true, type: () => String }, soDienThoai: { required: true, type: () => String }, ngaySinh: { required: true, type: () => String }, noiSinh: { required: true, type: () => String }, diaChi: { required: true, type: () => String }, gioiTinh: { required: true, type: () => String }, danToc: { required: true, type: () => String }, quocTich: { required: true, type: () => String }, cccd: { required: true, type: () => require("./giayTo.schema").GiayTo }, hoChieu: { required: true, type: () => require("./giayTo.schema").GiayTo }, chucVu: { required: true, type: () => require("./laoDong.schema").LaoDong }, lopHoc: { required: true, type: () => require("../lop-hoc/lop-hoc.entity").LopHoc }, ngayNhapHoc: { required: true, type: () => String }, chuNhiem: { required: true, type: () => require("../lop-hoc/lop-hoc.entity").LopHoc }, dangHoatDong: { required: true, type: () => Boolean }, conCai: { required: true, type: () => [require("./nguoi-dung.entity").NguoiDung] } };
    }
};
__decorate([
    mongoose_1.Prop({ required: true, index: true, unique: true }),
    __metadata("design:type", String)
], NguoiDung.prototype, "maND", void 0);
__decorate([
    mongoose_1.Prop({ required: true }),
    __metadata("design:type", String)
], NguoiDung.prototype, "hoTen", void 0);
__decorate([
    mongoose_1.Prop({ default: '' }),
    __metadata("design:type", String)
], NguoiDung.prototype, "emailND", void 0);
__decorate([
    mongoose_1.Prop({ required: true }),
    __metadata("design:type", String)
], NguoiDung.prototype, "matKhau", void 0);
__decorate([
    mongoose_1.Prop({ default: '' }),
    __metadata("design:type", String)
], NguoiDung.prototype, "soDienThoai", void 0);
__decorate([
    mongoose_1.Prop({ required: true }),
    __metadata("design:type", String)
], NguoiDung.prototype, "ngaySinh", void 0);
__decorate([
    mongoose_1.Prop({ required: true }),
    __metadata("design:type", String)
], NguoiDung.prototype, "noiSinh", void 0);
__decorate([
    mongoose_1.Prop({ required: true }),
    __metadata("design:type", String)
], NguoiDung.prototype, "diaChi", void 0);
__decorate([
    mongoose_1.Prop({ required: true }),
    __metadata("design:type", String)
], NguoiDung.prototype, "gioiTinh", void 0);
__decorate([
    mongoose_1.Prop({ required: true }),
    __metadata("design:type", String)
], NguoiDung.prototype, "danToc", void 0);
__decorate([
    mongoose_1.Prop({ required: true, default: 'Việt Nam' }),
    __metadata("design:type", String)
], NguoiDung.prototype, "quocTich", void 0);
__decorate([
    mongoose_1.Prop({ type: giayTo_schema_1.GiayToSchema, default: {} }),
    __metadata("design:type", giayTo_schema_1.GiayTo)
], NguoiDung.prototype, "cccd", void 0);
__decorate([
    mongoose_1.Prop({ type: giayTo_schema_1.GiayToSchema, default: {} }),
    __metadata("design:type", giayTo_schema_1.GiayTo)
], NguoiDung.prototype, "hoChieu", void 0);
__decorate([
    mongoose_1.Prop({ type: laoDong_schema_1.LaoDongSchema, default: {} }),
    __metadata("design:type", laoDong_schema_1.LaoDong)
], NguoiDung.prototype, "chucVu", void 0);
__decorate([
    mongoose_1.Prop({
        ref: 'lop_hoc',
        type: mongoose_2.Schema.Types.ObjectId,
    }),
    __metadata("design:type", lop_hoc_entity_1.LopHoc)
], NguoiDung.prototype, "lopHoc", void 0);
__decorate([
    mongoose_1.Prop({ default: '' }),
    __metadata("design:type", String)
], NguoiDung.prototype, "ngayNhapHoc", void 0);
__decorate([
    mongoose_1.Prop({
        ref: 'lop_hoc',
        type: mongoose_2.Schema.Types.ObjectId,
    }),
    __metadata("design:type", lop_hoc_entity_1.LopHoc)
], NguoiDung.prototype, "chuNhiem", void 0);
__decorate([
    mongoose_1.Prop({ default: true }),
    __metadata("design:type", Boolean)
], NguoiDung.prototype, "dangHoatDong", void 0);
__decorate([
    mongoose_1.Prop({
        type: [{ type: mongoose_2.Schema.Types.ObjectId, ref: 'nguoi_dung' }],
    }),
    __metadata("design:type", Array)
], NguoiDung.prototype, "conCai", void 0);
NguoiDung = __decorate([
    mongoose_1.Schema({
        timestamps: {
            createdAt: 'thoiDiemTao',
            updatedAt: 'thoiDiemSua',
        },
        versionKey: false,
    })
], NguoiDung);
exports.NguoiDung = NguoiDung;
exports.NguoiDungSchema = mongoose_1.SchemaFactory.createForClass(NguoiDung);
//# sourceMappingURL=nguoi-dung.entity.js.map