import { Document, Schema as MongooseSchema } from 'mongoose';
import { LopHoc } from '../lop-hoc/lop-hoc.entity';
import { GiayTo } from './giayTo.schema';
import { LaoDong } from './laoDong.schema';
export declare type NguoiDungDocument = NguoiDung & Document;
export declare class NguoiDung {
    maND: string;
    hoTen: string;
    emailND: string;
    matKhau: string;
    soDienThoai: string;
    ngaySinh: string;
    noiSinh: string;
    diaChi: string;
    gioiTinh: string;
    danToc: string;
    quocTich: string;
    cccd: GiayTo;
    hoChieu: GiayTo;
    chucVu: LaoDong;
    lopHoc: LopHoc;
    ngayNhapHoc: string;
    chuNhiem: LopHoc;
    dangHoatDong: boolean;
    conCai: NguoiDung[];
}
export declare const NguoiDungSchema: MongooseSchema<Document<NguoiDung, any, any>, import("mongoose").Model<any, any, any>, undefined, any>;
