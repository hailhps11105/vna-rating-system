"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LaoDongSchema = exports.LaoDong = void 0;
const mongoose_1 = require("@nestjs/mongoose");
let LaoDong = class LaoDong {
    chucVu;
    hopDong;
    trinhDo;
};
__decorate([
    mongoose_1.Prop(),
    __metadata("design:type", String)
], LaoDong.prototype, "chucVu", void 0);
__decorate([
    mongoose_1.Prop(),
    __metadata("design:type", String)
], LaoDong.prototype, "hopDong", void 0);
__decorate([
    mongoose_1.Prop(),
    __metadata("design:type", String)
], LaoDong.prototype, "trinhDo", void 0);
LaoDong = __decorate([
    mongoose_1.Schema({
        timestamps: {
            createdAt: 'thoiDiemTao',
            updatedAt: 'thoiDiemSua',
        },
        versionKey: false,
    })
], LaoDong);
exports.LaoDong = LaoDong;
exports.LaoDongSchema = mongoose_1.SchemaFactory.createForClass(LaoDong);
//# sourceMappingURL=laoDong.schema.js.map