"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NguoiDungService = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const bcrypt_1 = require("bcrypt");
const mongoose_2 = require("mongoose");
const utilities_1 = require("../../helpers/utilities");
const lop_hoc_service_1 = require("../lop-hoc/lop-hoc.service");
let NguoiDungService = class NguoiDungService {
    model;
    lhSer;
    constructor(model, lhSer) {
        this.model = model;
        this.lhSer = lhSer;
    }
    async create(dto) {
        const { cccd, ngayCap, noiCap, tDCM, chucVu, hopDong, lopHoc, chuNhiem, conCai, matKhau, ...rest } = dto;
        let result = {
            ...rest,
            matKhau: bcrypt_1.hashSync(matKhau, 10),
        };
        const temp = [];
        if (lopHoc)
            result = Object.assign(result, {
                lopHoc: await this.lhSer.objectify_fromName(lopHoc),
            });
        if (chuNhiem)
            result = Object.assign(result, {
                chuNhiem: await this.lhSer.objectify_fromName(chuNhiem),
            });
        if (cccd && ngayCap && noiCap)
            result = Object.assign(result, {
                cccd: {
                    maSo: cccd,
                    ngayCap: ngayCap,
                    noiCap: noiCap,
                },
            });
        if (chucVu && hopDong && tDCM)
            result = Object.assign(result, {
                chucVu: {
                    chucVu: chucVu,
                    hopDong: hopDong,
                    trinhDo: tDCM,
                },
            });
        if (conCai) {
            for (let i = 0; i < conCai.length; i++) {
                temp.push(mongoose_2.Types.ObjectId(conCai[i]));
            }
            result = Object.assign(result, { conCai: temp });
        }
        return await this.model.create(result);
    }
    async bulkCreate(dto) {
        for (let i = 0; i < dto.length; i++) {
            await this.create(dto[i]);
        }
        const latest = await this.model
            .find()
            .sort({ _id: -1 })
            .limit(dto.length);
        const result = [];
        for (let i = 0; i < latest.length; i++) {
            result.push(await this.findOne_byID(latest[i]._id));
        }
        return result;
    }
    async findAll(condition = {}) {
        const all = await this.model
            .find(condition)
            .populate([
            {
                path: 'chuNhiem',
                select: 'maLH',
            },
            {
                path: 'lopHoc',
                select: 'maLH',
            },
        ])
            .exec();
        const result = [];
        for (let i = 0; i < all.length; i++) {
            result.push({
                _id: all[i]._id,
                maND: all[i].maND,
                hoTen: all[i].hoTen,
                emailND: all[i].emailND,
                diaChi: all[i].diaChi,
                ngaySinh: all[i].ngaySinh,
                noiSinh: all[i].noiSinh,
                gioiTinh: all[i].gioiTinh,
                soDienThoai: all[i].soDienThoai ? all[i].soDienThoai : null,
                dangHoatDong: all[i].dangHoatDong,
                quocTich: all[i].quocTich,
                danToc: all[i].danToc,
                cccd: all[i].cccd
                    ? {
                        maSo: all[i].cccd.maSo,
                        ngayCap: all[i].cccd.ngayCap,
                        noiCap: all[i].cccd.noiCap,
                    }
                    : null,
                hoChieu: all[i].hoChieu
                    ? {
                        maSo: all[i].hoChieu.maSo,
                        ngayCap: all[i].hoChieu.ngayCap,
                        noiCap: all[i].hoChieu.noiCap,
                    }
                    : null,
                lopHoc: all[i].lopHoc
                    ? {
                        _id: all[i].populated('lopHoc'),
                        maLH: all[i].lopHoc.maLH,
                    }
                    : null,
                ngayNhapHoc: all[i].ngayNhapHoc ? all[i].ngayNhapHoc : null,
                chuNhiem: all[i].chuNhiem
                    ? {
                        _id: all[i].populated('chuNhiem'),
                        maLH: all[i].chuNhiem,
                    }
                    : null,
                chucVu: all[i].chucVu
                    ? {
                        chucVu: all[i].chucVu.chucVu,
                        hopDong: all[i].chucVu.hopDong,
                        trinhDo: all[i].chucVu.trinhDo,
                    }
                    : null,
            });
        }
        return result;
    }
    async getAll(condition = {}) {
        const all = await this.model
            .find(condition)
            .populate([
            {
                path: 'chuNhiem',
                select: 'maLH',
            },
            {
                path: 'lopHoc',
                select: 'maLH',
            },
        ])
            .exec();
        const result = [];
        for (let i = 0; i < all.length; i++) {
            result.push({
                maND: all[i].maND,
                hoTen: all[i].hoTen,
                emailND: all[i].emailND,
                diaChi: all[i].diaChi,
                ngaySinh: all[i].ngaySinh,
                noiSinh: all[i].noiSinh,
                gioiTinh: all[i].gioiTinh,
                soDienThoai: all[i].soDienThoai ? all[i].soDienThoai : null,
                dangHoatDong: all[i].dangHoatDong,
                quocTich: all[i].quocTich,
                danToc: all[i].danToc,
                cccd: all[i].cccd
                    ? {
                        maSo: all[i].cccd.maSo,
                        ngayCap: all[i].cccd.ngayCap,
                        noiCap: all[i].cccd.noiCap,
                    }
                    : null,
                hoChieu: all[i].hoChieu
                    ? {
                        maSo: all[i].hoChieu.maSo,
                        ngayCap: all[i].hoChieu.ngayCap,
                        noiCap: all[i].hoChieu.noiCap,
                    }
                    : null,
                ngayNhapHoc: all[i].ngayNhapHoc ? all[i].ngayNhapHoc : null,
                lopHoc: all[i].lopHoc ? all[i].lopHoc.maLH : null,
                chuNhiem: all[i].chuNhiem ? all[i].chuNhiem.maLH : null,
                chucVu: all[i].chucVu
                    ? {
                        chucVu: all[i].chucVu.chucVu,
                        hopDong: all[i].chucVu.hopDong,
                        trinhDo: all[i].chucVu.trinhDo,
                    }
                    : null,
            });
        }
        return;
    }
    async findAll_byRole(role) {
        const reg = role === 'QT-HT' ? new RegExp('QT|HT', 'i') : new RegExp(role, 'i');
        return await this.findAll({ maND: reg });
    }
    async groupInfo(group) {
        return await this.model.aggregate([
            {
                $match: { _id: { $all: utilities_1.bulkObjectID(group) } },
            },
            {
                $project: {
                    maND: 1,
                    hoTen: 1,
                },
            },
        ]);
    }
    async quickInfo(user) {
        return await this.model.aggregate([
            {
                $match: { _id: mongoose_2.Types.ObjectId(user) },
            },
            {
                $project: {
                    maND: 1,
                    hoTen: 1,
                },
            },
        ]);
    }
    async classCount(classe) {
        const all = await this.model.aggregate([
            {
                $match: { lopHoc: mongoose_2.Types.ObjectId(classe) },
            },
            {
                $project: {
                    maND: 1,
                    hoTen: 1,
                },
            },
        ]);
        return all.length;
    }
    async findAll_byClass(lop) {
        return await this.getAll({ lopHoc: mongoose_2.Types.ObjectId(lop) });
    }
    async getOne_byID(nd) {
        const user = await (await this.model.findById(nd))
            .populate([
            {
                path: 'chuNhiem',
                select: 'maLH',
            },
            {
                path: 'lopHoc',
                select: ['maLH', 'GVCN'],
                populate: {
                    path: 'GVCN',
                    select: 'hoTen',
                },
            },
        ])
            .execPopulate();
        return {
            maND: user.maND,
            hoTen: user.hoTen,
            emailND: user.emailND,
            diaChi: user.diaChi,
            ngaySinh: user.ngaySinh,
            noiSinh: user.noiSinh,
            gioiTinh: user.gioiTinh,
            soDienThoai: user.soDienThoai ? user.soDienThoai : null,
            dangHoatDong: user.dangHoatDong,
            quocTich: user.quocTich,
            danToc: user.danToc,
            cccd: user.cccd
                ? {
                    maSo: user.cccd.maSo,
                    ngayCap: user.cccd.ngayCap,
                    noiCap: user.cccd.noiCap,
                }
                : null,
            hoChieu: user.hoChieu
                ? {
                    maSo: user.hoChieu.maSo,
                    ngayCap: user.hoChieu.ngayCap,
                    noiCap: user.hoChieu.noiCap,
                }
                : null,
            hocTap: user.lopHoc
                ? {
                    idLop: user.populated('lopHoc'),
                    ngayNhapHoc: user.ngayNhapHoc,
                    GVCN: user.lopHoc.GVCN.hoTen,
                    lopHoc: user.lopHoc.maLH,
                }
                : null,
            chuNhiem: user.chuNhiem ? user.chuNhiem.maLH : null,
            chucVu: user.chucVu
                ? {
                    chucVu: user.chucVu.chucVu,
                    hopDong: user.chucVu.hopDong,
                    trinhDo: user.chucVu.trinhDo,
                }
                : null,
        };
    }
    async findOne_byID(nd) {
        const user = await (await this.model.findById(nd))
            .populate([
            {
                path: 'chuNhiem',
                select: 'maLH',
            },
            {
                path: 'lopHoc',
                select: 'maLH',
            },
        ])
            .execPopulate();
        return {
            _id: nd,
            maND: user.maND,
            hoTen: user.hoTen,
            emailND: user.emailND,
            diaChi: user.diaChi,
            ngaySinh: user.ngaySinh,
            noiSinh: user.noiSinh,
            gioiTinh: user.gioiTinh,
            soDienThoai: user.soDienThoai ? user.soDienThoai : null,
            dangHoatDong: user.dangHoatDong,
            quocTich: user.quocTich,
            danToc: user.danToc,
            cccd: user.cccd
                ? {
                    maSo: user.cccd.maSo,
                    ngayCap: user.cccd.ngayCap,
                    noiCap: user.cccd.noiCap,
                }
                : null,
            hoChieu: user.hoChieu
                ? {
                    maSo: user.hoChieu.maSo,
                    ngayCap: user.hoChieu.ngayCap,
                    noiCap: user.hoChieu.noiCap,
                }
                : null,
            ngayNhapHoc: user.ngayNhapHoc ? user.ngayNhapHoc : null,
            lopHoc: user.lopHoc
                ? {
                    _id: user.populated('lopHoc'),
                    maLH: user.lopHoc,
                }
                : null,
            chuNhiem: user.chuNhiem
                ? {
                    _id: user.populated('chuNhiem'),
                    maLH: user.chuNhiem,
                }
                : null,
            chucVu: user.chucVu
                ? {
                    chucVu: user.chucVu.chucVu,
                    hopDong: user.chucVu.hopDong,
                    trinhDo: user.chucVu.trinhDo,
                }
                : null,
        };
    }
    async update(id, dto) {
        const { cccd, ngayCap, noiCap, chuNhiem, chucVu, conCai, hopDong, tDCM, lopHoc, matKhau, ...rest } = dto;
        let gt, ld;
        if (cccd && ngayCap && noiCap) {
            gt = {
                maSo: dto.cccd,
                ngayCap: dto.ngayCap,
                noiCap: dto.noiCap,
            };
        }
        if (chucVu && hopDong && tDCM) {
            ld = {
                chucVu: dto.chucVu,
                hopDong: dto.hopDong,
                trinhDo: dto.tDCM,
            };
        }
        return await this.model.findById(id, null, null, async (err, doc) => {
            if (err)
                throw err;
            utilities_1.assign(rest, doc);
            if (matKhau)
                doc.matKhau = bcrypt_1.hashSync(matKhau, 10);
            if (gt)
                doc.cccd = gt;
            if (ld)
                doc.chucVu = ld;
            if (lopHoc)
                doc.lopHoc = await this.lhSer.objectify_fromID(lopHoc);
            if (chuNhiem)
                doc.chuNhiem = await this.lhSer.objectify_fromID(chuNhiem);
            if (conCai)
                doc.conCai = await this.bulkObjectify(conCai);
            await doc.save();
        });
    }
    async objectify(user) {
        return (await this.model.findById(user))._id;
    }
    async bulkObjectify(user) {
        const result = [];
        for (let i = 0; i < user.length; i++) {
            result.push(await this.objectify(user[i]));
        }
        return result;
    }
    async remove(id) {
        return await this.model.findByIdAndDelete(id);
    }
};
NguoiDungService = __decorate([
    common_1.Injectable(),
    __param(0, mongoose_1.InjectModel('nguoi_dung')),
    __param(1, common_1.Inject(common_1.forwardRef(() => lop_hoc_service_1.LopHocService))),
    __metadata("design:paramtypes", [mongoose_2.Model,
        lop_hoc_service_1.LopHocService])
], NguoiDungService);
exports.NguoiDungService = NguoiDungService;
//# sourceMappingURL=nguoi-dung.service.js.map