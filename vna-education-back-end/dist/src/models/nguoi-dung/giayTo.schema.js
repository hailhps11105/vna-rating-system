"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GiayToSchema = exports.GiayTo = void 0;
const mongoose_1 = require("@nestjs/mongoose");
let GiayTo = class GiayTo {
    maSo;
    ngayCap;
    noiCap;
};
__decorate([
    mongoose_1.Prop(),
    __metadata("design:type", String)
], GiayTo.prototype, "maSo", void 0);
__decorate([
    mongoose_1.Prop(),
    __metadata("design:type", String)
], GiayTo.prototype, "ngayCap", void 0);
__decorate([
    mongoose_1.Prop(),
    __metadata("design:type", String)
], GiayTo.prototype, "noiCap", void 0);
GiayTo = __decorate([
    mongoose_1.Schema({
        timestamps: {
            createdAt: 'thoiDiemTao',
            updatedAt: 'thoiDiemSua',
        },
        versionKey: false,
    })
], GiayTo);
exports.GiayTo = GiayTo;
exports.GiayToSchema = mongoose_1.SchemaFactory.createForClass(GiayTo);
//# sourceMappingURL=giayTo.schema.js.map