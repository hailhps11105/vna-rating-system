"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NguoiDungDto = void 0;
const openapi = require("@nestjs/swagger");
const swagger_1 = require("@nestjs/swagger");
class NguoiDungDto {
    maND;
    hoTen;
    emailND;
    matKhau;
    soDienThoai;
    ngaySinh;
    noiSinh;
    diaChi;
    gioiTinh;
    danToc;
    quocTich;
    cccd;
    ngayCap;
    noiCap;
    chucVu;
    hopDong;
    tDCM;
    lopHoc;
    ngayNhapHoc;
    dangHoatDong;
    chuNhiem;
    conCai;
    static _OPENAPI_METADATA_FACTORY() {
        return { maND: { required: true, type: () => String }, hoTen: { required: true, type: () => String }, emailND: { required: true, type: () => String }, matKhau: { required: true, type: () => String }, soDienThoai: { required: true, type: () => String }, ngaySinh: { required: true, type: () => String }, noiSinh: { required: true, type: () => String }, diaChi: { required: true, type: () => String }, gioiTinh: { required: true, type: () => String }, danToc: { required: true, type: () => String }, quocTich: { required: true, type: () => String }, cccd: { required: true, type: () => String }, ngayCap: { required: true, type: () => String }, noiCap: { required: true, type: () => String }, chucVu: { required: true, type: () => String }, hopDong: { required: true, type: () => String }, tDCM: { required: true, type: () => String }, lopHoc: { required: true, type: () => String }, ngayNhapHoc: { required: true, type: () => String }, dangHoatDong: { required: true, type: () => Boolean }, chuNhiem: { required: true, type: () => String }, conCai: { required: true, type: () => [String] } };
    }
}
__decorate([
    swagger_1.ApiProperty({
        name: 'maND',
        type: String,
        required: true,
        example: 'PH420',
        description: 'Mã người dùng, dùng để đăng nhập',
    }),
    __metadata("design:type", String)
], NguoiDungDto.prototype, "maND", void 0);
__decorate([
    swagger_1.ApiProperty({
        name: 'hoTen',
        type: String,
        required: true,
        example: 'Bành Thị Mẹt',
        description: 'Họ tên của người dùng',
    }),
    __metadata("design:type", String)
], NguoiDungDto.prototype, "hoTen", void 0);
__decorate([
    swagger_1.ApiProperty({
        name: 'emailND',
        type: String,
        example: 'metbanh96@nullmail.com',
        description: 'Email của người dùng',
    }),
    __metadata("design:type", String)
], NguoiDungDto.prototype, "emailND", void 0);
__decorate([
    swagger_1.ApiProperty({
        name: 'matKhau',
        type: String,
        required: true,
        example: '20xxxxxxxxxxxxxxxxxxxxxxxxx',
        description: 'Mật khẩu của người dùng, mã hóa trước lưu vào CSDL',
    }),
    __metadata("design:type", String)
], NguoiDungDto.prototype, "matKhau", void 0);
__decorate([
    swagger_1.ApiProperty({
        name: 'soDienThoai',
        type: String,
        example: '0123 456 7890',
        description: 'Số điện thoại của người dùng',
    }),
    __metadata("design:type", String)
], NguoiDungDto.prototype, "soDienThoai", void 0);
__decorate([
    swagger_1.ApiProperty({
        name: 'ngaySinh',
        type: String,
        example: '20-4-1996',
        required: true,
        description: 'Ngày sinh của người dùng',
    }),
    __metadata("design:type", String)
], NguoiDungDto.prototype, "ngaySinh", void 0);
__decorate([
    swagger_1.ApiProperty({
        name: 'noiSinh',
        type: String,
        example: 'Chắt Kdau',
        required: true,
        description: 'Nơi sinh của người dùng',
    }),
    __metadata("design:type", String)
], NguoiDungDto.prototype, "noiSinh", void 0);
__decorate([
    swagger_1.ApiProperty({
        name: 'soDienThoai',
        type: String,
        example: '13/6/4/13F/2D/5A/6C Cây Mai, P.17, TP. Thủ Đức, TP. HCM',
        required: true,
        description: 'Địa chỉ của người dùng',
    }),
    __metadata("design:type", String)
], NguoiDungDto.prototype, "diaChi", void 0);
__decorate([
    swagger_1.ApiProperty({
        name: 'gioiTinh',
        required: true,
        type: String,
        enum: ['Nam', 'Nữ', 'Khác'],
        description: 'Giới tính của người dùng',
    }),
    __metadata("design:type", String)
], NguoiDungDto.prototype, "gioiTinh", void 0);
__decorate([
    swagger_1.ApiProperty({
        name: 'danToc',
        required: true,
        type: String,
        example: 'Sán Dìu',
        description: 'Dân tộc của người dùng',
    }),
    __metadata("design:type", String)
], NguoiDungDto.prototype, "danToc", void 0);
__decorate([
    swagger_1.ApiProperty({
        name: 'quocTich',
        required: true,
        type: String,
        example: 'Lào',
        default: 'Việt Nam',
        description: 'Quốc tịch của người dùng',
    }),
    __metadata("design:type", String)
], NguoiDungDto.prototype, "quocTich", void 0);
__decorate([
    swagger_1.ApiProperty({
        name: 'cccd',
        type: String,
        description: 'Chi tiết (mã số) về CCCD của người dùng',
    }),
    __metadata("design:type", String)
], NguoiDungDto.prototype, "cccd", void 0);
__decorate([
    swagger_1.ApiProperty({
        name: 'ngayCap',
        type: String,
        description: 'Ngày cấp CCCD/hoChieu của người dùng',
    }),
    __metadata("design:type", String)
], NguoiDungDto.prototype, "ngayCap", void 0);
__decorate([
    swagger_1.ApiProperty({
        name: 'noiCap',
        type: String,
        description: 'Nơi cấp CCCD/hộ chiếu của người dùng',
    }),
    __metadata("design:type", String)
], NguoiDungDto.prototype, "noiCap", void 0);
__decorate([
    swagger_1.ApiProperty({
        name: 'chucVu',
        type: String,
        description: 'Chi tiết về chức vụ của người dùng',
    }),
    __metadata("design:type", String)
], NguoiDungDto.prototype, "chucVu", void 0);
__decorate([
    swagger_1.ApiProperty({
        name: 'hopDong',
        type: String,
        description: 'Hình thức hợp đồng lao động của người dùng',
    }),
    __metadata("design:type", String)
], NguoiDungDto.prototype, "hopDong", void 0);
__decorate([
    swagger_1.ApiProperty({
        name: 'tDCM',
        type: String,
        description: 'Trình độ chuyên môn của người dùng',
    }),
    __metadata("design:type", String)
], NguoiDungDto.prototype, "tDCM", void 0);
__decorate([
    swagger_1.ApiProperty({
        name: 'lopHoc',
        type: String,
        example: '10A4',
        description: 'Lớp học của học sinh',
    }),
    __metadata("design:type", String)
], NguoiDungDto.prototype, "lopHoc", void 0);
__decorate([
    swagger_1.ApiProperty({
        name: 'ngayNhapHoc',
        type: String,
        example: '21-12-2012',
        description: 'Ngày nhập học của học sinh',
    }),
    __metadata("design:type", String)
], NguoiDungDto.prototype, "ngayNhapHoc", void 0);
__decorate([
    swagger_1.ApiProperty({
        name: 'dangHoatDong',
        type: Boolean,
        description: 'Trạng thái hoạt động của học sinh/giáo viên',
    }),
    __metadata("design:type", Boolean)
], NguoiDungDto.prototype, "dangHoatDong", void 0);
__decorate([
    swagger_1.ApiProperty({
        name: 'chuNhiem',
        type: String,
        example: '60bxxxxxxxxxxxxx',
        description: 'Lớp học mà giáo viên được giao chủ nhiệm',
    }),
    __metadata("design:type", String)
], NguoiDungDto.prototype, "chuNhiem", void 0);
__decorate([
    swagger_1.ApiProperty({
        name: 'conCai',
        type: 'array',
        description: 'Con cái của phụ huynh',
    }),
    __metadata("design:type", Array)
], NguoiDungDto.prototype, "conCai", void 0);
exports.NguoiDungDto = NguoiDungDto;
//# sourceMappingURL=nguoi-dung.dto.js.map