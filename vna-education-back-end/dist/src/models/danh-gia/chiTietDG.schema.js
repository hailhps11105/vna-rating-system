"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChiTietDGSchema = exports.ChiTietDG = void 0;
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const nguoi_dung_entity_1 = require("../nguoi-dung/nguoi-dung.entity");
let ChiTietDG = class ChiTietDG {
    nguoiDG;
    diemDG;
    gopY;
    trangThai;
    formDG;
};
__decorate([
    mongoose_1.Prop({
        required: true,
        type: mongoose_2.Schema.Types.ObjectId,
        ref: 'nguoi_dung',
    }),
    __metadata("design:type", nguoi_dung_entity_1.NguoiDung)
], ChiTietDG.prototype, "nguoiDG", void 0);
__decorate([
    mongoose_1.Prop({
        min: 0,
        max: 10,
        default: 0,
    }),
    __metadata("design:type", Number)
], ChiTietDG.prototype, "diemDG", void 0);
__decorate([
    mongoose_1.Prop({ default: '' }),
    __metadata("design:type", String)
], ChiTietDG.prototype, "gopY", void 0);
__decorate([
    mongoose_1.Prop({ default: false }),
    __metadata("design:type", Boolean)
], ChiTietDG.prototype, "trangThai", void 0);
__decorate([
    mongoose_1.Prop({ default: [] }),
    __metadata("design:type", Array)
], ChiTietDG.prototype, "formDG", void 0);
ChiTietDG = __decorate([
    mongoose_1.Schema({
        timestamps: {
            createdAt: 'thoiDiemTao',
            updatedAt: 'thoiDiemSua',
        },
        versionKey: false,
    })
], ChiTietDG);
exports.ChiTietDG = ChiTietDG;
exports.ChiTietDGSchema = mongoose_1.SchemaFactory.createForClass(ChiTietDG);
//# sourceMappingURL=chiTietDG.schema.js.map