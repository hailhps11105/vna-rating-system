import { Document, Schema as MongooseSchema } from 'mongoose';
import { LopHoc } from '../lop-hoc/lop-hoc.entity';
import { MauDanhGia } from '../mau-danh-gia/mau-danh-gia.entity';
import { MonHoc } from '../mon-hoc/mon-hoc.entity';
import { NguoiDung } from '../nguoi-dung/nguoi-dung.entity';
import { TuanHoc } from '../tuan-hoc/tuan-hoc.entity';
import { ChiTietDG } from './chiTietDG.schema';
export declare type DanhGiaDocument = DanhGia & Document;
export declare class DanhGia {
    tenDG: string;
    lopHoc: LopHoc;
    chiTiet: ChiTietDG[];
    monHoc: MonHoc;
    giaoVien: NguoiDung;
    tuanDG: TuanHoc;
    mauDG: MauDanhGia;
    choGVCN: boolean;
    daDuyet: boolean;
}
export declare const DanhGiaSchema: MongooseSchema<Document<DanhGia, any, any>, import("mongoose").Model<any, any, any>, undefined, any>;
