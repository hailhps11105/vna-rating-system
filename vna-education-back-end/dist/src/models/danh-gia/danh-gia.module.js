"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DanhGiaModule = void 0;
const common_1 = require("@nestjs/common");
const danh_gia_service_1 = require("./danh-gia.service");
const danh_gia_controller_1 = require("./danh-gia.controller");
const mongoose_1 = require("@nestjs/mongoose");
const danh_gia_entity_1 = require("./danh-gia.entity");
const nguoi_dung_module_1 = require("../nguoi-dung/nguoi-dung.module");
const lop_hoc_module_1 = require("../lop-hoc/lop-hoc.module");
const mon_hoc_module_1 = require("../mon-hoc/mon-hoc.module");
const mau_danh_gia_module_1 = require("../mau-danh-gia/mau-danh-gia.module");
const tuan_hoc_module_1 = require("../tuan-hoc/tuan-hoc.module");
const choHS_service_1 = require("./roles/choHS.service");
const choGV_service_1 = require("./roles/choGV.service");
const choHT_service_1 = require("./roles/choHT.service");
const thongKe_service_1 = require("./roles/thongKe.service");
let DanhGiaModule = class DanhGiaModule {
};
DanhGiaModule = __decorate([
    common_1.Module({
        imports: [
            mongoose_1.MongooseModule.forFeature([
                { name: 'danh_gia', collection: 'danh_gia', schema: danh_gia_entity_1.DanhGiaSchema },
            ]),
            nguoi_dung_module_1.NguoiDungModule,
            lop_hoc_module_1.LopHocModule,
            mon_hoc_module_1.MonHocModule,
            tuan_hoc_module_1.TuanHocModule,
            common_1.forwardRef(() => mau_danh_gia_module_1.MauDanhGiaModule),
        ],
        controllers: [danh_gia_controller_1.DanhGiaController],
        providers: [
            danh_gia_service_1.DanhGiaService,
            choHS_service_1.ChoHocSinhService,
            choGV_service_1.ChoGiaoVienService,
            choHT_service_1.ChoHieuTruongService,
            thongKe_service_1.ThongKeService,
        ],
        exports: [
            danh_gia_service_1.DanhGiaService,
            choHS_service_1.ChoHocSinhService,
            choGV_service_1.ChoGiaoVienService,
            choHT_service_1.ChoHieuTruongService,
            thongKe_service_1.ThongKeService,
        ],
    })
], DanhGiaModule);
exports.DanhGiaModule = DanhGiaModule;
//# sourceMappingURL=danh-gia.module.js.map