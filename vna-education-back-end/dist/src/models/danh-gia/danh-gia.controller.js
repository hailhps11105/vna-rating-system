"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DanhGiaController = void 0;
const openapi = require("@nestjs/swagger");
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const auth_guard_1 = require("../../auth.guard");
const danh_gia_service_1 = require("./danh-gia.service");
const create_danh_gia_dto_1 = require("./dto/create-danh-gia.dto");
const update_danh_gia_dto_1 = require("./dto/update-danh-gia.dto");
let DanhGiaController = class DanhGiaController {
    service;
    constructor(service) {
        this.service = service;
    }
    async create(dto) {
        return await this.service.create(dto);
    }
    async findAll() {
        return await this.service.findAll();
    }
    async findAll_notDone(hs, tuan) {
        return await this.service.getUnfinished(hs, tuan);
    }
    async findAllBy(mon, gv, cn) {
        if (gv && gv != '' && cn)
            return await this.service.finAll_byGVCN(gv);
        if (mon && mon != '' && gv && gv != '')
            return await this.service.findAll_byGVbySub(gv, mon);
        if (mon && mon != '')
            return await this.service.findAll_bySubject(mon);
        return await this.service.findAll();
    }
    async findOne(id) {
        return await this.service.findOne(id);
    }
    async update(id, dto) {
        return await this.service.update(id, dto);
    }
    async remove(id) {
        return await this.service.remove(id);
    }
};
__decorate([
    common_1.Post(),
    swagger_1.ApiBody({
        type: create_danh_gia_dto_1.CreateDanhGiaDto,
        description: 'DTO chứa dữ liệu để tạo 1 đánh giá mới',
    }),
    swagger_1.ApiCreatedResponse({ description: 'Tạo thành công' }),
    swagger_1.ApiForbiddenResponse({
        description: 'Ngăn cản truy cập do chưa đăng nhập vào hệ thống',
    }),
    openapi.ApiResponse({ status: 201, type: Object }),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_danh_gia_dto_1.CreateDanhGiaDto]),
    __metadata("design:returntype", Promise)
], DanhGiaController.prototype, "create", null);
__decorate([
    common_1.Get(),
    swagger_1.ApiOkResponse({
        description: 'Trả về tất cả',
        isArray: true,
    }),
    swagger_1.ApiForbiddenResponse({
        description: 'Ngăn cản truy cập do chưa đăng nhập vào hệ thống',
    }),
    openapi.ApiResponse({ status: 200, type: [Object] }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], DanhGiaController.prototype, "findAll", null);
__decorate([
    common_1.Get('chua-lam'),
    openapi.ApiResponse({ status: 200, type: [Object] }),
    __param(0, common_1.Query('hs')),
    __param(1, common_1.Query('tuan')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String]),
    __metadata("design:returntype", Promise)
], DanhGiaController.prototype, "findAll_notDone", null);
__decorate([
    common_1.Get('theo'),
    openapi.ApiResponse({ status: 200, type: [Object] }),
    __param(0, common_1.Query('mon')),
    __param(1, common_1.Query('gv')),
    __param(2, common_1.Query('chuNhiem')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String, Boolean]),
    __metadata("design:returntype", Promise)
], DanhGiaController.prototype, "findAllBy", null);
__decorate([
    common_1.Get(':id'),
    swagger_1.ApiParam({
        name: 'id',
        type: String,
        description: '_id của đánh giá',
    }),
    swagger_1.ApiForbiddenResponse({
        description: 'Ngăn cản truy cập do chưa đăng nhập vào hệ thống',
    }),
    swagger_1.ApiOkResponse({ description: 'Trả về 1 đối tượng', isArray: false }),
    openapi.ApiResponse({ status: 200 }),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], DanhGiaController.prototype, "findOne", null);
__decorate([
    common_1.Patch(':id'),
    swagger_1.ApiParam({
        name: 'id',
        type: String,
        description: '_id của đánh giá',
    }),
    swagger_1.ApiBody({
        description: 'DTO chứa dữ liệu để cập nhật đánh giá',
        isArray: false,
        type: update_danh_gia_dto_1.UpdateDanhGiaDto,
    }),
    swagger_1.ApiForbiddenResponse({
        description: 'Ngăn cản truy cập do chưa đăng nhập vào hệ thống',
    }),
    swagger_1.ApiOkResponse({ description: 'Cập nhật thành công', isArray: false }),
    openapi.ApiResponse({ status: 200, type: Object }),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, update_danh_gia_dto_1.UpdateDanhGiaDto]),
    __metadata("design:returntype", Promise)
], DanhGiaController.prototype, "update", null);
__decorate([
    common_1.Delete(':id'),
    swagger_1.ApiParam({
        name: 'id',
        type: String,
        description: '_id của đánh giá',
    }),
    swagger_1.ApiForbiddenResponse({
        description: 'Ngăn cản truy cập do chưa đăng nhập vào hệ thống',
    }),
    swagger_1.ApiOkResponse({ description: 'Xóa thành công', isArray: false }),
    openapi.ApiResponse({ status: 200, type: Object }),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], DanhGiaController.prototype, "remove", null);
DanhGiaController = __decorate([
    common_1.Controller('danh-gia'),
    common_1.UseGuards(auth_guard_1.AuthGuard),
    swagger_1.ApiTags('danh-gia'),
    swagger_1.ApiExtraModels(create_danh_gia_dto_1.CreateDanhGiaDto, update_danh_gia_dto_1.UpdateDanhGiaDto),
    __metadata("design:paramtypes", [danh_gia_service_1.DanhGiaService])
], DanhGiaController);
exports.DanhGiaController = DanhGiaController;
//# sourceMappingURL=danh-gia.controller.js.map