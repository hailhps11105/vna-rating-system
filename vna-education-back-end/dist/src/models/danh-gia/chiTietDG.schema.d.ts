import { Schema as MongooseSchema } from 'mongoose';
import { NguoiDung } from '../nguoi-dung/nguoi-dung.entity';
export declare class ChiTietDG {
    nguoiDG: NguoiDung;
    diemDG: number;
    gopY: string;
    trangThai: boolean;
    formDG: any[];
}
export declare const ChiTietDGSchema: MongooseSchema<import("mongoose").Document<ChiTietDG, any, any>, import("mongoose").Model<any, any, any>, undefined, any>;
