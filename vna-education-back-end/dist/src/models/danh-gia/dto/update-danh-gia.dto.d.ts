import { CreateDanhGiaDto } from './create-danh-gia.dto';
declare const UpdateDanhGiaDto_base: import("@nestjs/common").Type<Partial<CreateDanhGiaDto>>;
export declare class UpdateDanhGiaDto extends UpdateDanhGiaDto_base {
    chiTiet: any[];
}
export {};
