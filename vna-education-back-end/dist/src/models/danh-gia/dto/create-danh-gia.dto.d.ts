export declare class CreateDanhGiaDto {
    tenDG: string;
    lopHoc: string;
    monHoc: string;
    giaoVien: string;
    tuanDG: string;
    choGVCN: boolean;
    mauDG: string;
    daDuyet: boolean;
}
