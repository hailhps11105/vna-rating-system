"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateDanhGiaDto = void 0;
const openapi = require("@nestjs/swagger");
const swagger_1 = require("@nestjs/swagger");
const create_danh_gia_dto_1 = require("./create-danh-gia.dto");
class UpdateDanhGiaDto extends swagger_1.PartialType(create_danh_gia_dto_1.CreateDanhGiaDto) {
    chiTiet;
    static _OPENAPI_METADATA_FACTORY() {
        return { chiTiet: { required: true, type: () => [Object] } };
    }
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        name: 'chiTiet',
        type: 'array',
        description: 'Chi tiết của đánh giá',
    }),
    __metadata("design:type", Array)
], UpdateDanhGiaDto.prototype, "chiTiet", void 0);
exports.UpdateDanhGiaDto = UpdateDanhGiaDto;
//# sourceMappingURL=update-danh-gia.dto.js.map