"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateDanhGiaDto = void 0;
const openapi = require("@nestjs/swagger");
class CreateDanhGiaDto {
    tenDG;
    lopHoc = null;
    monHoc = null;
    giaoVien;
    tuanDG;
    choGVCN = false;
    mauDG;
    daDuyet = false;
    static _OPENAPI_METADATA_FACTORY() {
        return { tenDG: { required: true, type: () => String }, lopHoc: { required: true, type: () => String, default: null }, monHoc: { required: true, type: () => String, default: null }, giaoVien: { required: true, type: () => String }, tuanDG: { required: true, type: () => String }, choGVCN: { required: true, type: () => Object, default: false }, mauDG: { required: true, type: () => String }, daDuyet: { required: true, type: () => Object, default: false } };
    }
}
exports.CreateDanhGiaDto = CreateDanhGiaDto;
//# sourceMappingURL=create-danh-gia.dto.js.map