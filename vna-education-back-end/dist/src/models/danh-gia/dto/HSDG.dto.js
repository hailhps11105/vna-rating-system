"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.HSDGDto = void 0;
const openapi = require("@nestjs/swagger");
class HSDGDto {
    nguoiDG;
    trangThai;
    gopY;
    diemDG;
    formDG;
    static _OPENAPI_METADATA_FACTORY() {
        return { nguoiDG: { required: true, type: () => String }, trangThai: { required: true, type: () => Boolean }, gopY: { required: true, type: () => String }, diemDG: { required: true, type: () => Number }, formDG: { required: true, type: () => [Object] } };
    }
}
exports.HSDGDto = HSDGDto;
//# sourceMappingURL=HSDG.dto.js.map