import { DanhGiaService } from './danh-gia.service';
import { CreateDanhGiaDto } from './dto/create-danh-gia.dto';
import { UpdateDanhGiaDto } from './dto/update-danh-gia.dto';
export declare class DanhGiaController {
    private readonly service;
    constructor(service: DanhGiaService);
    create(dto: CreateDanhGiaDto): Promise<import("./danh-gia.entity").DanhGiaDocument>;
    findAll(): Promise<any[]>;
    findAll_notDone(hs: string, tuan: string): Promise<any[]>;
    findAllBy(mon: string, gv: string, cn: boolean): Promise<any[]>;
    findOne(id: string): Promise<{
        _id: string;
        tenDG: string;
        tieuChi: import("../mau-danh-gia/tieuChi.schema").TieuChi[];
        monHoc: {
            _id: any;
            tenMH: string;
        };
        giaoVien: {
            _id: any;
            hoTen: string;
        };
        choGVCN: boolean;
        daDuyet: boolean;
        mauDG: {
            _id: any;
            tenMau: string;
        };
        tuanDG: {
            _id: any;
            soTuan: number;
        };
        lopHoc: {
            _id: any;
            maLH: string;
        };
        chiTiet: import("./chiTietDG.schema").ChiTietDG[];
    }>;
    update(id: string, dto: UpdateDanhGiaDto): Promise<import("./danh-gia.entity").DanhGiaDocument>;
    remove(id: string): Promise<import("./danh-gia.entity").DanhGiaDocument>;
}
