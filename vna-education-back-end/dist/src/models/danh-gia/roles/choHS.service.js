"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChoHocSinhService = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const utilities_1 = require("../../../helpers/utilities");
const nguoi_dung_service_1 = require("../../nguoi-dung/nguoi-dung.service");
let ChoHocSinhService = class ChoHocSinhService {
    model;
    ndSer;
    constructor(model, ndSer) {
        this.model = model;
        this.ndSer = ndSer;
    }
    async getAll_byHS(hs, tuan) {
        const result = [];
        const user = await this.ndSer.findOne_byID(hs);
        const now = new Date().getTime();
        const revs = await this.model
            .find({ lopHoc: Object(user.lopHoc._id), tuanDG: Object(tuan) })
            .populate([
            {
                path: 'giaoVien',
                select: 'hoTen',
            },
            { path: 'monHoc', select: 'tenMH' },
            {
                path: 'mauDG',
                select: 'tieuChi',
            },
            {
                path: 'lopHoc',
                select: 'maLH',
            },
            {
                path: 'tuanDG',
                select: ['soTuan', 'ngayKetThuc'],
            },
        ])
            .exec();
        for (let i = 0; i < revs.length; i++) {
            let n;
            const m = {
                _id: revs[i]._id,
                tenDG: revs[i].tenDG,
                monHoc: revs[i].monHoc ? revs[i].monHoc.tenMH : null,
                giaoVien: revs[i].giaoVien?.hoTen,
                choGVCN: revs[i].choGVCN,
                daDuyet: revs[i].daDuyet,
                tuanDG: revs[i].tuanDG?.soTuan,
                hetHan: false,
            };
            if (revs[i].chiTiet.length === 0) {
                n = {
                    ...m,
                    hocSinhDG: {
                        diemDG: 0,
                        formDG: [],
                        gopY: '',
                        nguoiDG: hs,
                        trangThai: false,
                    },
                };
                if (utilities_1.arrange(revs[i].tuanDG.ngayKetThuc).getTime() < now)
                    n.hetHan = true;
                if (n)
                    result.push(n);
            }
            else {
                const x = revs[i].chiTiet.findIndex((user) => {
                    return user.nguoiDG.toString() === hs;
                });
                if (x > -1) {
                    const loopuser = revs[i].chiTiet[x];
                    n = { ...m, hocSinhDG: loopuser };
                    if (revs[i].tuanDG &&
                        utilities_1.arrange(revs[i].tuanDG.ngayKetThuc).getTime() < now)
                        n.hetHan = true;
                    if (n)
                        result.push(n);
                }
                else {
                    n = {
                        ...m,
                        hocSinhDG: {
                            diemDG: 0,
                            formDG: [],
                            gopY: '',
                            nguoiDG: hs,
                            trangThai: false,
                        },
                    };
                    if (revs[i].tuanDG &&
                        utilities_1.arrange(revs[i].tuanDG.ngayKetThuc).getTime() < now)
                        n.hetHan = true;
                    if (n)
                        result.push(n);
                }
            }
        }
        return result;
    }
    async getOne_forHS(id) {
        const now = new Date().getTime();
        const one = await this.model
            .findById(id)
            .populate([
            {
                path: 'giaoVien',
                select: 'hoTen',
            },
            { path: 'monHoc', select: 'tenMH' },
            {
                path: 'mauDG',
                select: 'tieuChi',
            },
            {
                path: 'lopHoc',
                select: ['maLH', 'hocSinh'],
            },
            {
                path: 'tuanDG',
                select: ['soTuan', 'ngayKetThuc'],
            },
        ])
            .exec();
        const n = { hetHan: false };
        if (one.tuanDG && utilities_1.arrange(one.tuanDG.ngayKetThuc).getTime() < now)
            n.hetHan = true;
        return {
            _id: id,
            tenDG: one.tenDG,
            tieuChi: one.mauDG?.tieuChi,
            tuanDG: one.tuanDG?.soTuan,
            giaoVien: one.giaoVien?.hoTen,
            choGVCN: one.choGVCN,
            daDuyet: one.daDuyet,
            monHoc: one.monHoc ? one.monHoc.tenMH : null,
            ...n,
            chiTiet: one.lopHoc
                ? {
                    idLop: one.populated('lopHoc'),
                    lopHoc: one.lopHoc.maLH,
                    siSo: one.lopHoc.hocSinh.length,
                    hocSinhDG: one.chiTiet,
                }
                : null,
        };
    }
    async update_fromHS(id, dto) {
        const { nguoiDG, ...rest } = dto;
        return await this.model.findByIdAndUpdate(id, {
            $push: { chiTiet: { ...rest, nguoiDG: Object(nguoiDG) } },
        });
    }
};
ChoHocSinhService = __decorate([
    common_1.Injectable(),
    __param(0, mongoose_1.InjectModel('danh_gia')),
    __metadata("design:paramtypes", [mongoose_2.Model,
        nguoi_dung_service_1.NguoiDungService])
], ChoHocSinhService);
exports.ChoHocSinhService = ChoHocSinhService;
//# sourceMappingURL=choHS.service.js.map