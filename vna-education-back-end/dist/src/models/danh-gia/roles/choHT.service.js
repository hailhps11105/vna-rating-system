"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChoHieuTruongService = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const utilities_1 = require("../../../helpers/utilities");
const nguoi_dung_service_1 = require("../../nguoi-dung/nguoi-dung.service");
let ChoHieuTruongService = class ChoHieuTruongService {
    model;
    ndSer;
    constructor(model, ndSer) {
        this.model = model;
        this.ndSer = ndSer;
    }
    async findAll_ofGVBM(gv, lop) {
        const all = await this.model
            .find({
            giaoVien: Object(gv),
            lopHoc: Object(lop),
        })
            .populate([
            { path: 'monHoc', select: 'tenMH' },
            {
                path: 'giaoVien',
                select: 'hoTen',
            },
            {
                path: 'lopHoc',
                select: ['maLH', 'hocSinh'],
            },
            {
                path: 'tuanDG',
                select: 'soTuan',
            },
        ])
            .exec();
        const result = [];
        for (let i = 0; i < all.length; i++) {
            let temp = 0, diem = 0;
            for (let j = 0; j < all[i].chiTiet.length; j++) {
                temp += all[i].chiTiet[j].diemDG;
            }
            diem = temp / all[i].chiTiet.length;
            result.push({
                _id: all[i]._id,
                tenDG: all[i].tenDG,
                monHoc: all[i].monHoc ? all[i].monHoc : null,
                giaoVien: all[i].giaoVien ? all[i].giaoVien : null,
                lopHoc: all[i].lopHoc
                    ? {
                        _id: all[i].populated('lopHoc'),
                        maLH: all[i].lopHoc.maLH,
                        siSo: await this.ndSer.classCount(String(all[i].populated('lopHoc'))),
                        luotDG: all[i].chiTiet.length,
                    }
                    : null,
                tuanDG: all[i].tuanDG ? all[i].tuanDG : null,
                diemTB: all[i].chiTiet.length > 0 ? diem : 0,
            });
        }
        return result;
    }
    async findOne_ofGVCN(id) {
        const one = await this.model
            .findById(id)
            .populate([
            { path: 'monHoc', select: 'tenMH' },
            {
                path: 'giaoVien',
                select: 'hoTen',
            },
            {
                path: 'lopHoc',
                select: ['maLH', 'hocSinh'],
            },
            {
                path: 'tuanDG',
                select: 'soTuan',
            },
        ])
            .exec();
        let temp = 0, diem = 0;
        for (let i = 0; i < one.chiTiet.length; i++) {
            temp += one.chiTiet[i].diemDG;
        }
        diem = temp / one.chiTiet.length;
        return {
            _id: id,
            tenDG: one.tenDG,
            monHoc: one.monHoc ? one.monHoc : null,
            giaoVien: one.giaoVien ? one.giaoVien : null,
            lopHoc: one.lopHoc
                ? {
                    _id: one.populated('lopHoc'),
                    maLH: one.lopHoc.maLH,
                    siSo: await this.ndSer.classCount(one.populated('lopHoc')),
                    luotDG: one.chiTiet.length,
                }
                : null,
            tuanDG: one.tuanDG ? one.tuanDG : null,
            chiTiet: one.chiTiet,
            diemTB: one.chiTiet.length > 0 ? diem : 0,
        };
    }
    async getAll_forHT(tuan, lop = 'all') {
        const query = { tuanDG: Object(tuan) };
        if (mongoose_2.isValidObjectId(lop))
            Object.assign(query, { lopHoc: Object(lop) });
        const result = [];
        const now = new Date().getTime();
        const all = await this.model
            .find(query)
            .populate([
            {
                path: 'giaoVien',
                select: 'hoTen',
            },
            { path: 'monHoc', select: 'tenMH' },
            {
                path: 'lopHoc',
                select: ['maLH', 'hocSinh'],
            },
            {
                path: 'tuanDG',
                select: ['soTuan', 'ngayKetThuc'],
            },
        ])
            .exec();
        for (let i = 0; i < all.length; i++) {
            let diem = 0, temp = 0;
            for (let j = 0; j < all[i].chiTiet.length; j++) {
                temp += all[i].chiTiet[j].diemDG;
            }
            diem = temp / all[i].chiTiet.length;
            result.push({
                _id: all[i]._id,
                tenDG: all[i].tenDG,
                monHoc: all[i].monHoc
                    ? {
                        _id: all[i].populated('monHoc'),
                        tenMH: all[i].monHoc.tenMH,
                    }
                    : null,
                lopHoc: all[i].lopHoc
                    ? {
                        _id: all[i].populated('lopHoc'),
                        maLH: all[i].lopHoc?.maLH,
                        siSo: await this.ndSer.classCount(String(all[i].populated('lopHoc'))),
                        luotDG: all[i].chiTiet.length,
                    }
                    : null,
                giaoVien: all[i].giaoVien?.hoTen,
                choGVCN: all[i].choGVCN,
                daDuyet: all[i].daDuyet,
                tuanDG: all[i].tuanDG?.soTuan,
                diemTB: diem,
                hetHan: utilities_1.arrange(all[i].tuanDG.ngayKetThuc).getTime() < now,
            });
        }
        return result;
    }
    async getOne_forHT(dg) {
        const one = await this.model
            .findById(dg)
            .populate([
            {
                path: 'giaoVien',
                select: 'hoTen',
            },
            { path: 'monHoc', select: 'tenMH' },
            { path: 'mauDG', select: 'tieuChi' },
            {
                path: 'lopHoc',
                select: ['maLH', 'hocSinh'],
            },
            {
                path: 'tuanDG',
                select: 'soTuan',
            },
        ])
            .exec();
        let temp = 0, diem = 0;
        for (let i = 0; i < one.chiTiet.length; i++) {
            temp += one.chiTiet[i].diemDG;
        }
        diem = temp / one.chiTiet.length;
        return {
            _id: dg,
            tenDG: one.tenDG,
            monHoc: one.monHoc ? one.monHoc.tenMH : null,
            giaoVien: one.giaoVien?.hoTen,
            tieuChi: one.mauDG?.tieuChi,
            choGVCN: one.choGVCN,
            daDuyet: one.daDuyet,
            lopHoc: one.lopHoc
                ? {
                    maLH: one.lopHoc.maLH,
                    siSo: await this.ndSer.classCount(one.populated('lopHoc')),
                    luotDG: one.chiTiet.length,
                }
                : null,
            tuanDG: one.tuanDG?.soTuan,
            chiTiet: one.chiTiet,
            diemDG: one.chiTiet.length > 0 ? diem : 0,
        };
    }
    async approve(dg, tinhTrang = true) {
        return await this.model.findByIdAndUpdate(dg, { $set: { daDuyet: tinhTrang } }, { new: true });
    }
};
ChoHieuTruongService = __decorate([
    common_1.Injectable(),
    __param(0, mongoose_1.InjectModel('danh_gia')),
    __metadata("design:paramtypes", [mongoose_2.Model,
        nguoi_dung_service_1.NguoiDungService])
], ChoHieuTruongService);
exports.ChoHieuTruongService = ChoHieuTruongService;
//# sourceMappingURL=choHT.service.js.map