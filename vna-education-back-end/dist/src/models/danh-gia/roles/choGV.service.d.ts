import { Model } from 'mongoose';
import { NguoiDungService } from '../../nguoi-dung/nguoi-dung.service';
import { DanhGiaDocument } from '../danh-gia.entity';
export declare class ChoGiaoVienService {
    private model;
    private readonly ndSer;
    constructor(model: Model<DanhGiaDocument>, ndSer: NguoiDungService);
    getAll_byGV(gv: string, tuan: string): Promise<any[]>;
    getOne_forGV(id: string): Promise<{
        _id: string;
        tenDG: string;
        monHoc: string;
        tieuChi: import("../../mau-danh-gia/tieuChi.schema").TieuChi[];
        choGVCN: boolean;
        daDuyet: boolean;
        lopHoc: {
            maLH: string;
            siSo: number;
            luotDG: number;
        };
        tuanDG: number;
        chiTiet: import("../chiTietDG.schema").ChiTietDG[];
        diemDG: number;
    }>;
}
