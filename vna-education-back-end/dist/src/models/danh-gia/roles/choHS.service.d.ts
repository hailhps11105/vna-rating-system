import { Model } from 'mongoose';
import { NguoiDungService } from '../../nguoi-dung/nguoi-dung.service';
import { DanhGiaDocument } from '../danh-gia.entity';
import { HSDGDto } from '../dto/HSDG.dto';
export declare class ChoHocSinhService {
    private model;
    private readonly ndSer;
    constructor(model: Model<DanhGiaDocument>, ndSer: NguoiDungService);
    getAll_byHS(hs: string, tuan: string): Promise<any[]>;
    getOne_forHS(id: string): Promise<{
        chiTiet: {
            idLop: any;
            lopHoc: string;
            siSo: number;
            hocSinhDG: import("../chiTietDG.schema").ChiTietDG[];
        };
        hetHan: boolean;
        _id: string;
        tenDG: string;
        tieuChi: import("../../mau-danh-gia/tieuChi.schema").TieuChi[];
        tuanDG: number;
        giaoVien: string;
        choGVCN: boolean;
        daDuyet: boolean;
        monHoc: string;
    }>;
    update_fromHS(id: string, dto: HSDGDto): Promise<DanhGiaDocument>;
}
