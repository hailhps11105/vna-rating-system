import { Model } from 'mongoose';
import { DanhGiaDocument } from '../danh-gia.entity';
import { ChoGiaoVienService } from './choGV.service';
export declare class ThongKeService {
    private model;
    private readonly choGV;
    constructor(model: Model<DanhGiaDocument>, choGV: ChoGiaoVienService);
    getAll_byWeek(tuan: string): Promise<{
        giaoVien: any;
        diemTong: number;
    }[]>;
}
