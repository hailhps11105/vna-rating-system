import { Model } from 'mongoose';
import { NguoiDungService } from '../../nguoi-dung/nguoi-dung.service';
import { DanhGiaDocument } from '../danh-gia.entity';
export declare class ChoHieuTruongService {
    private model;
    private readonly ndSer;
    constructor(model: Model<DanhGiaDocument>, ndSer: NguoiDungService);
    findAll_ofGVBM(gv: string, lop: string): Promise<any[]>;
    findOne_ofGVCN(id: string): Promise<{
        _id: string;
        tenDG: string;
        monHoc: import("../../mon-hoc/mon-hoc.entity").MonHoc;
        giaoVien: import("../../nguoi-dung/nguoi-dung.entity").NguoiDung;
        lopHoc: {
            _id: any;
            maLH: string;
            siSo: number;
            luotDG: number;
        };
        tuanDG: import("../../tuan-hoc/tuan-hoc.entity").TuanHoc;
        chiTiet: import("../chiTietDG.schema").ChiTietDG[];
        diemTB: number;
    }>;
    getAll_forHT(tuan: string, lop?: string): Promise<any[]>;
    getOne_forHT(dg: string): Promise<{
        _id: string;
        tenDG: string;
        monHoc: string;
        giaoVien: string;
        tieuChi: import("../../mau-danh-gia/tieuChi.schema").TieuChi[];
        choGVCN: boolean;
        daDuyet: boolean;
        lopHoc: {
            maLH: string;
            siSo: number;
            luotDG: number;
        };
        tuanDG: number;
        chiTiet: import("../chiTietDG.schema").ChiTietDG[];
        diemDG: number;
    }>;
    approve(dg: string, tinhTrang?: boolean): Promise<DanhGiaDocument>;
}
