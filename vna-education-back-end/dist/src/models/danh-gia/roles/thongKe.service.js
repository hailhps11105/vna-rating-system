"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ThongKeService = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const utilities_1 = require("../../../helpers/utilities");
const choGV_service_1 = require("./choGV.service");
let ThongKeService = class ThongKeService {
    model;
    choGV;
    constructor(model, choGV) {
        this.model = model;
        this.choGV = choGV;
    }
    async getAll_byWeek(tuan) {
        const all = await this.model
            .find({ tuanDG: Object(tuan) })
            .populate({ path: 'giaoVien', select: 'hoTen' })
            .exec();
        const toSort = utilities_1.removeDuplicates(all, 'giaoVien').map((val) => {
            return {
                giaoVien: val.giaoVien.hoTen,
                diemTong: 0,
            };
        });
        for (let i = 0; i < toSort.length; i++) {
            const toCount = [];
            for (let j = 0; j < all.length; j++) {
                if (all[j].giaoVien.hoTen === toSort[i].giaoVien) {
                    if (all[j].chiTiet.length > 0) {
                        let temp = 0;
                        for (let k = 0; k < all[j].chiTiet.length; k++) {
                            temp += all[j].chiTiet[k].diemDG;
                        }
                        toCount.push(temp / all[j].chiTiet.length);
                    }
                    else
                        toCount.push(0);
                }
            }
            toSort[i].diemTong = utilities_1.average(toCount);
        }
        return toSort;
    }
};
ThongKeService = __decorate([
    common_1.Injectable(),
    __param(0, mongoose_1.InjectModel('danh_gia')),
    __metadata("design:paramtypes", [mongoose_2.Model,
        choGV_service_1.ChoGiaoVienService])
], ThongKeService);
exports.ThongKeService = ThongKeService;
//# sourceMappingURL=thongKe.service.js.map