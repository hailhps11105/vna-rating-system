import { Model } from 'mongoose';
import { LopHocService } from '../lop-hoc/lop-hoc.service';
import { MauDanhGiaService } from '../mau-danh-gia/mau-danh-gia.service';
import { MonHocService } from '../mon-hoc/mon-hoc.service';
import { NguoiDungService } from '../nguoi-dung/nguoi-dung.service';
import { TuanHocService } from '../tuan-hoc/tuan-hoc.service';
import { DanhGiaDocument } from './danh-gia.entity';
import { CreateDanhGiaDto } from './dto/create-danh-gia.dto';
import { UpdateDanhGiaDto } from './dto/update-danh-gia.dto';
export declare class DanhGiaService {
    private model;
    private readonly ndSer;
    private readonly mdgSer;
    private readonly lhSer;
    private readonly mhSer;
    private readonly tuanSer;
    constructor(model: Model<DanhGiaDocument>, ndSer: NguoiDungService, mdgSer: MauDanhGiaService, lhSer: LopHocService, mhSer: MonHocService, tuanSer: TuanHocService);
    create(dto: CreateDanhGiaDto): Promise<DanhGiaDocument>;
    getUnfinished(hs: string, tuan: string): Promise<any[]>;
    findAll_bySubject(mon: string): Promise<any[]>;
    findAll_byGVbySub(gv: string, mon: string): Promise<any[]>;
    finAll_byGVCN(gv: string): Promise<any[]>;
    findAll_byWeek(tuan: string): Promise<any[]>;
    findAll(condition?: any): Promise<any[]>;
    findOne(id: string): Promise<{
        _id: string;
        tenDG: string;
        tieuChi: import("../mau-danh-gia/tieuChi.schema").TieuChi[];
        monHoc: {
            _id: any;
            tenMH: string;
        };
        giaoVien: {
            _id: any;
            hoTen: string;
        };
        choGVCN: boolean;
        daDuyet: boolean;
        mauDG: {
            _id: any;
            tenMau: string;
        };
        tuanDG: {
            _id: any;
            soTuan: number;
        };
        lopHoc: {
            _id: any;
            maLH: string;
        };
        chiTiet: import("./chiTietDG.schema").ChiTietDG[];
    }>;
    update(id: string, dto: UpdateDanhGiaDto): Promise<DanhGiaDocument>;
    remove(id: string): Promise<DanhGiaDocument>;
}
