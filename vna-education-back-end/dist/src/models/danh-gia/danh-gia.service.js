"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DanhGiaService = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const utilities_1 = require("../../helpers/utilities");
const lop_hoc_service_1 = require("../lop-hoc/lop-hoc.service");
const mau_danh_gia_service_1 = require("../mau-danh-gia/mau-danh-gia.service");
const mon_hoc_service_1 = require("../mon-hoc/mon-hoc.service");
const nguoi_dung_service_1 = require("../nguoi-dung/nguoi-dung.service");
const tuan_hoc_service_1 = require("../tuan-hoc/tuan-hoc.service");
let DanhGiaService = class DanhGiaService {
    model;
    ndSer;
    mdgSer;
    lhSer;
    mhSer;
    tuanSer;
    constructor(model, ndSer, mdgSer, lhSer, mhSer, tuanSer) {
        this.model = model;
        this.ndSer = ndSer;
        this.mdgSer = mdgSer;
        this.lhSer = lhSer;
        this.mhSer = mhSer;
        this.tuanSer = tuanSer;
    }
    async create(dto) {
        const to404 = await this.model.find({
            tenDG: dto.tenDG,
            tuanDG: Object(dto.tuanDG),
            mauDG: Object(dto.mauDG),
            giaoVien: Object(dto.giaoVien),
        });
        if (to404.length > 0)
            return null;
        else {
            let toCreate = {
                tenDG: dto.tenDG,
                choGVCN: dto.choGVCN,
                daDuyet: dto.daDuyet,
                tuanDG: mongoose_2.Types.ObjectId(dto.tuanDG),
                mauDG: mongoose_2.Types.ObjectId(dto.mauDG),
                giaoVien: mongoose_2.Types.ObjectId(dto.giaoVien),
            };
            if (dto.monHoc)
                toCreate = Object.assign(toCreate, {
                    monHoc: mongoose_2.Types.ObjectId(dto.monHoc),
                });
            if (dto.lopHoc)
                toCreate = Object.assign(toCreate, {
                    lopHoc: mongoose_2.Types.ObjectId(dto.lopHoc),
                });
            return await this.model.create(toCreate);
        }
    }
    async getUnfinished(hs, tuan) {
        const user = await this.ndSer.findOne_byID(hs);
        const now = new Date().getTime();
        const all = await this.model
            .find({ lopHoc: Object(user.lopHoc._id), tuanDG: Object(tuan) })
            .populate({
            path: 'tuanDG',
            select: 'ngayKetThuc',
        })
            .exec();
        const result = [];
        for (let i = 0; i < all.length; i++) {
            if (utilities_1.arrange(all[i].tuanDG.ngayKetThuc).getTime() > now) {
                if (all[i].chiTiet.length === 0) {
                    result.push({
                        _id: all[i]._id,
                        tenDG: all[i].tenDG,
                    });
                }
                else {
                    const x = all[i].chiTiet.findIndex((user) => {
                        return (user.nguoiDG.toString() === Object(hs).toString());
                    });
                    if (x === -1) {
                        result.push({
                            _id: all[i]._id,
                            tenDG: all[i].tenDG,
                        });
                    }
                }
            }
        }
        return result;
    }
    async findAll_bySubject(mon) {
        return await this.findAll({ monHoc: Object(mon) });
    }
    async findAll_byGVbySub(gv, mon) {
        const all = await this.model
            .find({ giaoVien: Object(gv), monHoc: Object(mon) })
            .populate([
            { path: 'lopHoc', select: 'maLH' },
            { path: 'tuanDG', select: 'soTuan' },
        ])
            .exec();
        const result = [];
        for (let i = 0; i < all.length; i++) {
            let temp = 0, diem = 0;
            for (let j = 0; j < all[i].chiTiet.length; j++) {
                temp += all[i].chiTiet[j].diemDG;
            }
            diem = temp / all[i].chiTiet.length;
            result.push({
                idDG: all[i]._id,
                lopHoc: all[i].lopHoc ? all[i].lopHoc : null,
                tuanDG: all[i].tuanDG ? all[i].tuanDG : null,
                diemTong: all[i].chiTiet.length > 0 ? diem : 0,
            });
        }
        return utilities_1.removeDuplicates(result, 'lopHoc');
    }
    async finAll_byGVCN(gv) {
        const all = await this.model
            .find({ giaoVien: Object(gv), choGVCN: true })
            .populate({ path: 'tuanDG', select: ['soTuan', 'tenTuan'] })
            .exec();
        const result = [];
        for (let i = 0; i < all.length; i++) {
            let temp = 0, diem = 0;
            for (let j = 0; j < all[i].chiTiet.length; j++) {
                temp += all[i].chiTiet[j].diemDG;
            }
            diem = temp / all[i].chiTiet.length;
            result.push({
                idDG: all[i]._id,
                tuanDG: all[i].tuanDG ? all[i].tuanDG : null,
                diemTong: all[i].chiTiet.length > 0 ? diem : 0,
            });
        }
        return utilities_1.removeDuplicates(result, 'lopHoc');
    }
    async findAll_byWeek(tuan) {
        return await this.findAll({ tuanHoc: Object(tuan) });
    }
    async findAll(condition = {}) {
        const result = [];
        const all = await this.model
            .find(condition)
            .populate([
            {
                path: 'giaoVien',
                select: 'hoTen',
            },
            { path: 'monHoc', select: 'tenMH' },
            {
                path: 'mauDG',
                select: ['tieuChi', 'tenMau'],
            },
            {
                path: 'lopHoc',
                select: 'maLH',
            },
            {
                path: 'tuanDG',
                select: 'soTuan',
            },
        ])
            .exec();
        for (let i = 0; i < all.length; i++) {
            result.push({
                _id: all[i]._id,
                tenDG: all[i].tenDG,
                tieuChi: all[i].mauDG?.tieuChi,
                monHoc: all[i].monHoc
                    ? {
                        _id: all[i].populated('monHoc'),
                        tenMH: all[i].monHoc.tenMH,
                    }
                    : null,
                giaoVien: all[i].giaoVien
                    ? {
                        _id: all[i].populated('giaoVien'),
                        hoTen: all[i].giaoVien.hoTen,
                    }
                    : null,
                choGVCN: all[i].choGVCN,
                daDuyet: all[i].daDuyet,
                mauDG: all[i].mauDG
                    ? {
                        _id: all[i].populated('mauDG'),
                        tenMau: all[i].mauDG.tenMau,
                    }
                    : null,
                tuanDG: all[i].tuanDG
                    ? {
                        _id: all[i].populated('tuanDG'),
                        soTuan: all[i].tuanDG.soTuan,
                    }
                    : null,
                lopHoc: all[i].lopHoc
                    ? {
                        _id: all[i].populated('lopHoc'),
                        maLH: all[i].lopHoc.maLH,
                    }
                    : null,
                chiTiet: all[i].chiTiet,
            });
        }
        return result;
    }
    async findOne(id) {
        const one = await this.model
            .findById(id)
            .populate([
            {
                path: 'giaoVien',
                select: 'hoTen',
            },
            { path: 'monHoc', select: 'tenMH' },
            {
                path: 'mauDG',
                select: ['tieuChi', 'tenMau'],
            },
            {
                path: 'lopHoc',
                select: 'maLH',
            },
            {
                path: 'tuanDG',
                select: 'soTuan',
            },
        ])
            .exec();
        return {
            _id: id,
            tenDG: one.tenDG,
            tieuChi: one.mauDG?.tieuChi,
            monHoc: one.monHoc
                ? {
                    _id: one.populated('monHoc'),
                    tenMH: one.monHoc.tenMH,
                }
                : null,
            giaoVien: one.giaoVien
                ? {
                    _id: one.populated('giaoVien'),
                    hoTen: one.giaoVien.hoTen,
                }
                : null,
            choGVCN: one.choGVCN,
            daDuyet: one.daDuyet,
            mauDG: one.mauDG
                ? {
                    _id: one.populated('mauDG'),
                    tenMau: one.mauDG.tenMau,
                }
                : null,
            tuanDG: one.tuanDG
                ? {
                    _id: one.populated('tuanDG'),
                    soTuan: one.tuanDG.soTuan,
                }
                : null,
            lopHoc: one.lopHoc
                ? {
                    _id: one.populated('lopHoc'),
                    maLH: one.lopHoc.maLH,
                }
                : null,
            chiTiet: one.chiTiet,
        };
    }
    async update(id, dto) {
        const { monHoc, mauDG, lopHoc, giaoVien, tuanDG, ...rest } = dto;
        return await this.model.findById(id, null, null, async (err, doc) => {
            if (err)
                throw err;
            utilities_1.assign(rest, doc);
            if (monHoc)
                doc.monHoc = await this.mhSer.objectify(monHoc);
            if (mauDG)
                doc.mauDG = await this.mdgSer.objectify(mauDG);
            if (lopHoc)
                doc.lopHoc = await this.lhSer.objectify_fromID(lopHoc);
            if (giaoVien)
                doc.giaoVien = await this.ndSer.objectify(giaoVien);
            if (tuanDG)
                doc.tuanDG = await this.tuanSer.objectify(tuanDG);
            await doc.save();
        });
    }
    async remove(id) {
        return await this.model.findByIdAndDelete(id);
    }
};
DanhGiaService = __decorate([
    common_1.Injectable(),
    __param(0, mongoose_1.InjectModel('danh_gia')),
    __param(2, common_1.Inject(common_1.forwardRef(() => mau_danh_gia_service_1.MauDanhGiaService))),
    __metadata("design:paramtypes", [mongoose_2.Model,
        nguoi_dung_service_1.NguoiDungService,
        mau_danh_gia_service_1.MauDanhGiaService,
        lop_hoc_service_1.LopHocService,
        mon_hoc_service_1.MonHocService,
        tuan_hoc_service_1.TuanHocService])
], DanhGiaService);
exports.DanhGiaService = DanhGiaService;
//# sourceMappingURL=danh-gia.service.js.map