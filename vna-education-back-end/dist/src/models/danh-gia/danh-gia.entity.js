"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DanhGiaSchema = exports.DanhGia = void 0;
const openapi = require("@nestjs/swagger");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const lop_hoc_entity_1 = require("../lop-hoc/lop-hoc.entity");
const mau_danh_gia_entity_1 = require("../mau-danh-gia/mau-danh-gia.entity");
const mon_hoc_entity_1 = require("../mon-hoc/mon-hoc.entity");
const nguoi_dung_entity_1 = require("../nguoi-dung/nguoi-dung.entity");
const tuan_hoc_entity_1 = require("../tuan-hoc/tuan-hoc.entity");
const chiTietDG_schema_1 = require("./chiTietDG.schema");
let DanhGia = class DanhGia {
    tenDG;
    lopHoc;
    chiTiet;
    monHoc;
    giaoVien;
    tuanDG;
    mauDG;
    choGVCN;
    daDuyet;
    static _OPENAPI_METADATA_FACTORY() {
        return { tenDG: { required: true, type: () => String }, lopHoc: { required: true, type: () => require("../lop-hoc/lop-hoc.entity").LopHoc }, chiTiet: { required: true, type: () => [require("./chiTietDG.schema").ChiTietDG] }, monHoc: { required: true, type: () => require("../mon-hoc/mon-hoc.entity").MonHoc }, giaoVien: { required: true, type: () => require("../nguoi-dung/nguoi-dung.entity").NguoiDung }, tuanDG: { required: true, type: () => require("../tuan-hoc/tuan-hoc.entity").TuanHoc }, mauDG: { required: true, type: () => require("../mau-danh-gia/mau-danh-gia.entity").MauDanhGia }, choGVCN: { required: true, type: () => Boolean }, daDuyet: { required: true, type: () => Boolean } };
    }
};
__decorate([
    mongoose_1.Prop({ required: true, unique: true, index: true }),
    __metadata("design:type", String)
], DanhGia.prototype, "tenDG", void 0);
__decorate([
    mongoose_1.Prop({
        type: mongoose_2.Schema.Types.ObjectId,
        ref: 'lop_hoc',
    }),
    __metadata("design:type", lop_hoc_entity_1.LopHoc)
], DanhGia.prototype, "lopHoc", void 0);
__decorate([
    mongoose_1.Prop({ type: [chiTietDG_schema_1.ChiTietDGSchema] }),
    __metadata("design:type", Array)
], DanhGia.prototype, "chiTiet", void 0);
__decorate([
    mongoose_1.Prop({
        type: mongoose_2.Schema.Types.ObjectId,
        ref: 'mon_hoc',
    }),
    __metadata("design:type", mon_hoc_entity_1.MonHoc)
], DanhGia.prototype, "monHoc", void 0);
__decorate([
    mongoose_1.Prop({
        required: true,
        type: mongoose_2.Schema.Types.ObjectId,
        ref: 'nguoi_dung',
    }),
    __metadata("design:type", nguoi_dung_entity_1.NguoiDung)
], DanhGia.prototype, "giaoVien", void 0);
__decorate([
    mongoose_1.Prop({
        required: true,
        type: mongoose_2.Schema.Types.ObjectId,
        ref: 'tuan_hoc',
    }),
    __metadata("design:type", tuan_hoc_entity_1.TuanHoc)
], DanhGia.prototype, "tuanDG", void 0);
__decorate([
    mongoose_1.Prop({
        required: true,
        type: mongoose_2.Schema.Types.ObjectId,
        ref: 'mau_danh_gia',
    }),
    __metadata("design:type", mau_danh_gia_entity_1.MauDanhGia)
], DanhGia.prototype, "mauDG", void 0);
__decorate([
    mongoose_1.Prop({ default: false }),
    __metadata("design:type", Boolean)
], DanhGia.prototype, "choGVCN", void 0);
__decorate([
    mongoose_1.Prop({ default: false }),
    __metadata("design:type", Boolean)
], DanhGia.prototype, "daDuyet", void 0);
DanhGia = __decorate([
    mongoose_1.Schema({
        timestamps: {
            createdAt: 'thoiDiemTao',
            updatedAt: 'thoiDiemSua',
        },
        versionKey: false,
    })
], DanhGia);
exports.DanhGia = DanhGia;
exports.DanhGiaSchema = mongoose_1.SchemaFactory.createForClass(DanhGia);
//# sourceMappingURL=danh-gia.entity.js.map