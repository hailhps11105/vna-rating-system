"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BangDiemMonModule = void 0;
const common_1 = require("@nestjs/common");
const bang_diem_mon_service_1 = require("./bang-diem-mon.service");
const bang_diem_mon_controller_1 = require("./bang-diem-mon.controller");
const mongoose_1 = require("@nestjs/mongoose");
const bang_diem_mon_entity_1 = require("./bang-diem-mon.entity");
const nguoi_dung_module_1 = require("../nguoi-dung/nguoi-dung.module");
const mon_hoc_module_1 = require("../mon-hoc/mon-hoc.module");
let BangDiemMonModule = class BangDiemMonModule {
};
BangDiemMonModule = __decorate([
    common_1.Module({
        imports: [
            mongoose_1.MongooseModule.forFeature([
                {
                    name: 'bang_diem_mon',
                    collection: 'bang_diem_mon',
                    schema: bang_diem_mon_entity_1.BangDiemMonSchema,
                },
            ]),
            nguoi_dung_module_1.NguoiDungModule,
            mon_hoc_module_1.MonHocModule,
        ],
        controllers: [bang_diem_mon_controller_1.BangDiemMonController],
        providers: [bang_diem_mon_service_1.BangDiemMonService],
        exports: [bang_diem_mon_service_1.BangDiemMonService],
    })
], BangDiemMonModule);
exports.BangDiemMonModule = BangDiemMonModule;
//# sourceMappingURL=bang-diem-mon.module.js.map