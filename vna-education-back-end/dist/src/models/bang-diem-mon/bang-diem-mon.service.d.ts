import { Model } from 'mongoose';
import { MonHocService } from '../mon-hoc/mon-hoc.service';
import { NguoiDungService } from '../nguoi-dung/nguoi-dung.service';
import { BangDiemMonDto } from './bang-diem-mon.dto';
import { BangDiemMon } from './bang-diem-mon.entity';
export declare class BangDiemMonService {
    private model;
    private readonly ndSer;
    private readonly mhSer;
    constructor(model: Model<BangDiemMon>, ndSer: NguoiDungService, mhSer: MonHocService);
    create(dto: BangDiemMonDto): Promise<BangDiemMon & import("mongoose").Document<any, any, BangDiemMon>>;
    findAll(condition?: any): Promise<any[]>;
    getAll(condition?: any): Promise<any[]>;
    findOne(p: string): Promise<{
        _id: string;
        hocSinh: {
            _id: any;
            hoTen: string;
        };
        giaoVien: {
            _id: any;
            hoTen: string;
        };
        monHoc: {
            _id: any;
            tenMH: string;
        };
        hocKy1: import("./theoHK.schema").TheoHK;
        hocKy2: import("./theoHK.schema").TheoHK;
        diemTB: number;
        nhanXet: string;
    }>;
    update(id: string, dto: BangDiemMonDto): Promise<BangDiemMon & import("mongoose").Document<any, any, BangDiemMon>>;
    getAll_byHS(hs: string): Promise<any[]>;
    objectify(rec: string): Promise<any>;
    bulkObjectify(rec: string[]): Promise<any[]>;
    remove(id: string): Promise<BangDiemMon & import("mongoose").Document<any, any, BangDiemMon>>;
}
