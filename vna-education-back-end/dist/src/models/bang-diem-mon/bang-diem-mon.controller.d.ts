/// <reference types="mongoose" />
import { BangDiemMonDto } from './bang-diem-mon.dto';
import { BangDiemMonService } from './bang-diem-mon.service';
export declare class BangDiemMonController {
    private readonly service;
    constructor(service: BangDiemMonService);
    create(dto: BangDiemMonDto): Promise<import("./bang-diem-mon.entity").BangDiemMon & import("mongoose").Document<any, any, import("./bang-diem-mon.entity").BangDiemMon>>;
    findAll(): Promise<any[]>;
    findOne(id: string): Promise<{
        _id: string;
        hocSinh: {
            _id: any;
            hoTen: string;
        };
        giaoVien: {
            _id: any;
            hoTen: string;
        };
        monHoc: {
            _id: any;
            tenMH: string;
        };
        hocKy1: import("./theoHK.schema").TheoHK;
        hocKy2: import("./theoHK.schema").TheoHK;
        diemTB: number;
        nhanXet: string;
    }>;
    update(id: string, dto: BangDiemMonDto): Promise<import("./bang-diem-mon.entity").BangDiemMon & import("mongoose").Document<any, any, import("./bang-diem-mon.entity").BangDiemMon>>;
    remove(id: string): Promise<import("./bang-diem-mon.entity").BangDiemMon & import("mongoose").Document<any, any, import("./bang-diem-mon.entity").BangDiemMon>>;
}
