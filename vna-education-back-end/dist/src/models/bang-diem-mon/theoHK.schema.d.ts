/// <reference types="mongoose" />
export declare class TheoHK {
    kiemTra_mieng: number[];
    kiemTra_15phut: number[];
    kiemTra_1tiet: number[];
    thiHK: number;
    diemTong: number;
}
export declare const TheoHKSchema: import("mongoose").Schema<import("mongoose").Document<TheoHK, any, any>, import("mongoose").Model<any, any, any>, undefined, any>;
