import { Schema as MongooseSchema } from 'mongoose';
import { MonHoc } from '../mon-hoc/mon-hoc.entity';
import { NguoiDung } from '../nguoi-dung/nguoi-dung.entity';
import { TheoHK } from './theoHK.schema';
export declare type BangDiemMonDocument = BangDiemMon & Document;
export declare class BangDiemMon {
    monHoc: MonHoc;
    hocSinh: NguoiDung;
    giaoVien: NguoiDung;
    hocKy1: TheoHK;
    hocKy2: TheoHK;
    diemTB: number;
    nhanXet: string;
}
export declare const BangDiemMonSchema: MongooseSchema<import("mongoose").Document<BangDiemMon, any, any>, import("mongoose").Model<any, any, any>, undefined, any>;
