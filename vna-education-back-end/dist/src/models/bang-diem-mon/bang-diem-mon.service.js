"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BangDiemMonService = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const mon_hoc_service_1 = require("../mon-hoc/mon-hoc.service");
const nguoi_dung_service_1 = require("../nguoi-dung/nguoi-dung.service");
let BangDiemMonService = class BangDiemMonService {
    model;
    ndSer;
    mhSer;
    constructor(model, ndSer, mhSer) {
        this.model = model;
        this.ndSer = ndSer;
        this.mhSer = mhSer;
    }
    async create(dto) {
        return await this.model.create({
            nhanXet: dto.nhanXet,
            diemTB: dto.diemTB,
            hocKy1: {
                kiemTra_mieng: dto.ktMieng_HK1,
                kiemTra_15phut: dto.kt15phut_HK1,
                kiemTra_1tiet: dto.kt1tiet_HK1,
                thiHK: dto.thiHK1,
                diemTong: dto.tbHK1,
            },
            hocKy2: {
                kiemTra_mieng: dto.ktMieng_HK2,
                kiemTra_15phut: dto.kt15phut_HK2,
                kiemTra_1tiet: dto.kt1tiet_HK2,
                thiHK: dto.thiHK2,
                diemTong: dto.tbHK2,
            },
            hocSinh: mongoose_2.Types.ObjectId(dto.hocSinh),
            giaoVien: mongoose_2.Types.ObjectId(dto.giaoVien),
            monHoc: mongoose_2.Types.ObjectId(dto.monHoc),
        });
    }
    async findAll(condition = {}) {
        const all = await this.model.find(condition).populate([
            {
                path: 'hocSinh',
                select: 'hoTen',
            },
            { path: 'giaoVien', select: 'hoTen' },
            { path: 'monHoc', select: 'tenMH' },
        ]);
        const result = [];
        for (let i = 0; i < all.length; i++) {
            result.push({
                _id: all[i]._id,
                hocSinh: {
                    _id: all[i].populated('hocSinh'),
                    hoTen: all[i].hocSinh?.hoTen,
                },
                giaoVien: {
                    _id: all[i].populated('giaoVien'),
                    hoTen: all[i].giaoVien?.hoTen,
                },
                monHoc: {
                    _id: all[i].populated('monHoc'),
                    tenMH: all[i].monHoc?.tenMH,
                },
                hocKy1: all[i].hocKy1,
                hocKy2: all[i].hocKy2,
                diemTB: all[i].diemTB,
                nhanXet: all[i].nhanXet,
            });
        }
        return result;
    }
    async getAll(condition = {}) {
        const all = await this.model.find(condition).populate([
            {
                path: 'hocSinh',
                select: 'hoTen',
            },
            { path: 'giaoVien', select: 'hoTen' },
            { path: 'monHoc', select: 'tenMH' },
        ]);
        const result = [];
        for (let i = 0; i < all.length; i++) {
            result.push({
                _id: all[i]._id,
                hocSinh: all[i].hocSinh?.hoTen,
                giaoVien: all[i].giaoVien?.hoTen,
                monHoc: all[i].monHoc?.tenMH,
                hocKy1: all[i].hocKy1,
                hocKy2: all[i].hocKy2,
                diemTB: all[i].diemTB,
                nhanXet: all[i].nhanXet,
            });
        }
        return result;
    }
    async findOne(p) {
        const bd = await (await this.model.findById(p))
            .populate([
            {
                path: 'hocSinh',
                model: 'nguoi_dung',
            },
            { path: 'giaoVien', model: 'nguoi_dung' },
            { path: 'monHoc', model: 'mon_hoc' },
        ])
            .execPopulate();
        return {
            _id: p,
            hocSinh: {
                _id: bd.populated('hocSinh'),
                hoTen: bd.hocSinh?.hoTen,
            },
            giaoVien: {
                _id: bd.populated('giaoVien'),
                hoTen: bd.giaoVien?.hoTen,
            },
            monHoc: {
                _id: bd.populated('monHoc'),
                tenMH: bd.monHoc?.tenMH,
            },
            hocKy1: bd.hocKy1,
            hocKy2: bd.hocKy2,
            diemTB: bd.diemTB,
            nhanXet: bd.nhanXet,
        };
    }
    async update(id, dto) {
        return await this.model.findById(id, null, null, async (err, doc) => {
            if (err)
                throw err;
            if (dto.nhanXet)
                doc.nhanXet = dto.nhanXet;
            if (dto.diemTB)
                doc.diemTB = dto.diemTB;
            if (dto.ktMieng_HK1)
                doc.hocKy1.kiemTra_mieng = dto.ktMieng_HK1;
            if (dto.kt15phut_HK1)
                doc.hocKy1.kiemTra_15phut = dto.kt15phut_HK1;
            if (dto.ktMieng_HK1)
                doc.hocKy1.kiemTra_1tiet = dto.ktMieng_HK1;
            if (dto.thiHK1)
                doc.hocKy1.thiHK = dto.thiHK1;
            if (dto.tbHK1)
                doc.hocKy1.diemTong = dto.tbHK1;
            if (dto.ktMieng_HK2)
                doc.hocKy1.kiemTra_mieng = dto.ktMieng_HK2;
            if (dto.kt15phut_HK2)
                doc.hocKy1.kiemTra_15phut = dto.kt15phut_HK2;
            if (dto.ktMieng_HK2)
                doc.hocKy1.kiemTra_1tiet = dto.ktMieng_HK2;
            if (dto.thiHK2)
                doc.hocKy1.thiHK = dto.thiHK2;
            if (dto.tbHK2)
                doc.hocKy1.diemTong = dto.tbHK2;
            if (dto.hocSinh)
                doc.hocSinh = await this.ndSer.objectify(dto.hocSinh);
            if (dto.giaoVien)
                doc.giaoVien = await this.ndSer.objectify(dto.giaoVien);
            if (dto.monHoc)
                doc.monHoc = await this.mhSer.objectify(dto.monHoc);
            await doc.save();
        });
    }
    async getAll_byHS(hs) {
        const all = await this.getAll({ hocSinh: Object(hs) });
        const result = [];
        for (let i = 0; i < all.length; i++) {
            const { hocSinh, ...rest } = all[i];
            result.push(rest);
        }
        return result;
    }
    async objectify(rec) {
        return (await this.model.findById(rec))._id;
    }
    async bulkObjectify(rec) {
        const result = [];
        for (let i = 0; i < rec.length; i++) {
            result.push(await this.objectify(rec[i]));
        }
        return result;
    }
    async remove(id) {
        return await this.model.findByIdAndDelete(id);
    }
};
BangDiemMonService = __decorate([
    common_1.Injectable(),
    __param(0, mongoose_1.InjectModel('bang_diem_mon')),
    __metadata("design:paramtypes", [mongoose_2.Model,
        nguoi_dung_service_1.NguoiDungService,
        mon_hoc_service_1.MonHocService])
], BangDiemMonService);
exports.BangDiemMonService = BangDiemMonService;
//# sourceMappingURL=bang-diem-mon.service.js.map