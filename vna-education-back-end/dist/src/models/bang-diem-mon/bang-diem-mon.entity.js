"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BangDiemMonSchema = exports.BangDiemMon = void 0;
const openapi = require("@nestjs/swagger");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const mon_hoc_entity_1 = require("../mon-hoc/mon-hoc.entity");
const nguoi_dung_entity_1 = require("../nguoi-dung/nguoi-dung.entity");
const theoHK_schema_1 = require("./theoHK.schema");
let BangDiemMon = class BangDiemMon {
    monHoc;
    hocSinh;
    giaoVien;
    hocKy1;
    hocKy2;
    diemTB;
    nhanXet;
    static _OPENAPI_METADATA_FACTORY() {
        return { monHoc: { required: true, type: () => require("../mon-hoc/mon-hoc.entity").MonHoc }, hocSinh: { required: true, type: () => require("../nguoi-dung/nguoi-dung.entity").NguoiDung }, giaoVien: { required: true, type: () => require("../nguoi-dung/nguoi-dung.entity").NguoiDung }, hocKy1: { required: true, type: () => require("./theoHK.schema").TheoHK }, hocKy2: { required: true, type: () => require("./theoHK.schema").TheoHK }, diemTB: { required: true, type: () => Number }, nhanXet: { required: true, type: () => String } };
    }
};
__decorate([
    mongoose_1.Prop({
        required: true,
        type: mongoose_2.Schema.Types.ObjectId,
        ref: 'mon_hoc',
    }),
    __metadata("design:type", mon_hoc_entity_1.MonHoc)
], BangDiemMon.prototype, "monHoc", void 0);
__decorate([
    mongoose_1.Prop({
        type: mongoose_2.Schema.Types.ObjectId,
        ref: 'nguoi_dung',
        required: true,
    }),
    __metadata("design:type", nguoi_dung_entity_1.NguoiDung)
], BangDiemMon.prototype, "hocSinh", void 0);
__decorate([
    mongoose_1.Prop({
        type: mongoose_2.Schema.Types.ObjectId,
        ref: 'nguoi_dung',
        required: true,
    }),
    __metadata("design:type", nguoi_dung_entity_1.NguoiDung)
], BangDiemMon.prototype, "giaoVien", void 0);
__decorate([
    mongoose_1.Prop({ type: theoHK_schema_1.TheoHKSchema, default: {} }),
    __metadata("design:type", theoHK_schema_1.TheoHK)
], BangDiemMon.prototype, "hocKy1", void 0);
__decorate([
    mongoose_1.Prop({ type: theoHK_schema_1.TheoHKSchema, default: {} }),
    __metadata("design:type", theoHK_schema_1.TheoHK)
], BangDiemMon.prototype, "hocKy2", void 0);
__decorate([
    mongoose_1.Prop({ min: 0, max: 10, default: 0 }),
    __metadata("design:type", Number)
], BangDiemMon.prototype, "diemTB", void 0);
__decorate([
    mongoose_1.Prop({ default: '' }),
    __metadata("design:type", String)
], BangDiemMon.prototype, "nhanXet", void 0);
BangDiemMon = __decorate([
    mongoose_1.Schema({
        timestamps: {
            createdAt: 'thoiDiemTao',
            updatedAt: 'thoiDiemSua',
        },
        versionKey: false,
    })
], BangDiemMon);
exports.BangDiemMon = BangDiemMon;
exports.BangDiemMonSchema = mongoose_1.SchemaFactory.createForClass(BangDiemMon);
//# sourceMappingURL=bang-diem-mon.entity.js.map