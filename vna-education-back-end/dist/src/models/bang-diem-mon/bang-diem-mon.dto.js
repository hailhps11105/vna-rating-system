"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BangDiemMonDto = void 0;
const openapi = require("@nestjs/swagger");
class BangDiemMonDto {
    monHoc;
    hocSinh;
    giaoVien;
    diemTB;
    nhanXet;
    ktMieng_HK1;
    kt15phut_HK1;
    kt1tiet_HK1;
    thiHK1;
    tbHK1;
    ktMieng_HK2;
    kt15phut_HK2;
    kt1tiet_HK2;
    thiHK2;
    tbHK2;
    static _OPENAPI_METADATA_FACTORY() {
        return { monHoc: { required: true, type: () => String }, hocSinh: { required: true, type: () => String }, giaoVien: { required: true, type: () => String }, diemTB: { required: true, type: () => Number }, nhanXet: { required: true, type: () => String }, ktMieng_HK1: { required: true, type: () => [Number] }, kt15phut_HK1: { required: true, type: () => [Number] }, kt1tiet_HK1: { required: true, type: () => [Number] }, thiHK1: { required: true, type: () => Number }, tbHK1: { required: true, type: () => Number }, ktMieng_HK2: { required: true, type: () => [Number] }, kt15phut_HK2: { required: true, type: () => [Number] }, kt1tiet_HK2: { required: true, type: () => [Number] }, thiHK2: { required: true, type: () => Number }, tbHK2: { required: true, type: () => Number } };
    }
}
exports.BangDiemMonDto = BangDiemMonDto;
//# sourceMappingURL=bang-diem-mon.dto.js.map