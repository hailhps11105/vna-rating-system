"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BangDiemTongModule = void 0;
const common_1 = require("@nestjs/common");
const bang_diem_tong_service_1 = require("./bang-diem-tong.service");
const bang_diem_tong_controller_1 = require("./bang-diem-tong.controller");
const mongoose_1 = require("@nestjs/mongoose");
const bang_diem_tong_entity_1 = require("./bang-diem-tong.entity");
const nguoi_dung_module_1 = require("../nguoi-dung/nguoi-dung.module");
const bang_diem_mon_module_1 = require("../bang-diem-mon/bang-diem-mon.module");
let BangDiemTongModule = class BangDiemTongModule {
};
BangDiemTongModule = __decorate([
    common_1.Module({
        imports: [
            mongoose_1.MongooseModule.forFeature([
                {
                    name: 'bang_diem_tong',
                    collection: 'bang_diem_tong',
                    schema: bang_diem_tong_entity_1.BangDiemTongSchema,
                },
            ]),
            nguoi_dung_module_1.NguoiDungModule,
            bang_diem_mon_module_1.BangDiemMonModule,
        ],
        controllers: [bang_diem_tong_controller_1.BangDiemTongController],
        providers: [bang_diem_tong_service_1.BangDiemTongService],
        exports: [bang_diem_tong_service_1.BangDiemTongService],
    })
], BangDiemTongModule);
exports.BangDiemTongModule = BangDiemTongModule;
//# sourceMappingURL=bang-diem-tong.module.js.map