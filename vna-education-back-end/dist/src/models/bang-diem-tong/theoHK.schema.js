"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TheoHKSchema = exports.TheoHK = void 0;
const mongoose_1 = require("@nestjs/mongoose");
let TheoHK = class TheoHK {
    hocLuc;
    hanhKiem;
    diemTB;
};
__decorate([
    mongoose_1.Prop({ default: '' }),
    __metadata("design:type", String)
], TheoHK.prototype, "hocLuc", void 0);
__decorate([
    mongoose_1.Prop({ default: '' }),
    __metadata("design:type", String)
], TheoHK.prototype, "hanhKiem", void 0);
__decorate([
    mongoose_1.Prop({ min: 0, max: 10, default: 0 }),
    __metadata("design:type", Number)
], TheoHK.prototype, "diemTB", void 0);
TheoHK = __decorate([
    mongoose_1.Schema()
], TheoHK);
exports.TheoHK = TheoHK;
exports.TheoHKSchema = mongoose_1.SchemaFactory.createForClass(TheoHK);
//# sourceMappingURL=theoHK.schema.js.map