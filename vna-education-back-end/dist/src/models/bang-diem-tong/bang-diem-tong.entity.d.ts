import { Document, Schema as MongooseSchema } from 'mongoose';
import { BangDiemMon } from '../bang-diem-mon/bang-diem-mon.entity';
import { NguoiDung } from '../nguoi-dung/nguoi-dung.entity';
import { TheoHK } from './theoHK.schema';
export declare type BangDiemTongDocument = BangDiemTong & Document;
export declare class BangDiemTong {
    hocSinh: NguoiDung;
    GVCN: NguoiDung;
    hocKy1: TheoHK;
    hocKy2: TheoHK;
    caNam: TheoHK;
    bangDiemMon: BangDiemMon[];
    nhanXet: string;
    xepLoai: string;
}
export declare const BangDiemTongSchema: MongooseSchema<Document<BangDiemTong, any, any>, import("mongoose").Model<any, any, any>, undefined, any>;
