import { BangDiemTongDto } from './bang-diem-tong.dto';
import { BangDiemTongService } from './bang-diem-tong.service';
export declare class BangDiemTongController {
    private readonly service;
    constructor(service: BangDiemTongService);
    create(dto: BangDiemTongDto): Promise<import("./bang-diem-tong.entity").BangDiemTongDocument>;
    findAll(): Promise<any[]>;
    findOne(id: string): Promise<{
        _id: string;
        hocSinh: {
            _id: any;
            hoTen: string;
        };
        lopHoc: {
            _id: any;
            maLH: string;
        };
        GVCN: {
            _id: any;
            hoTen: string;
        };
        hocKy1: import("./theoHK.schema").TheoHK;
        hocKy2: import("./theoHK.schema").TheoHK;
        caNam: import("./theoHK.schema").TheoHK;
        xepLoai: string;
        nhanXet: string;
    }>;
    update(id: string, dto: BangDiemTongDto): Promise<import("./bang-diem-tong.entity").BangDiemTongDocument>;
    remove(id: string): Promise<import("./bang-diem-tong.entity").BangDiemTongDocument>;
}
