"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BangDiemTongDto = void 0;
const openapi = require("@nestjs/swagger");
class BangDiemTongDto {
    hocSinh;
    GVCN;
    bangDiemMon;
    nhanXet;
    xepLoai;
    hanhKiem_hk1;
    hocLuc_hk1;
    diemTB_hk1;
    hanhKiem_hk2;
    hocLuc_hk2;
    diemTB_hk2;
    hanhKiem_caNam;
    hocLuc_caNam;
    diemTB_caNam;
    static _OPENAPI_METADATA_FACTORY() {
        return { hocSinh: { required: true, type: () => String }, GVCN: { required: true, type: () => String }, bangDiemMon: { required: true, type: () => [String] }, nhanXet: { required: true, type: () => String }, xepLoai: { required: true, type: () => String }, hanhKiem_hk1: { required: true, type: () => String }, hocLuc_hk1: { required: true, type: () => String }, diemTB_hk1: { required: true, type: () => Number }, hanhKiem_hk2: { required: true, type: () => String }, hocLuc_hk2: { required: true, type: () => String }, diemTB_hk2: { required: true, type: () => Number }, hanhKiem_caNam: { required: true, type: () => String }, hocLuc_caNam: { required: true, type: () => String }, diemTB_caNam: { required: true, type: () => Number } };
    }
}
exports.BangDiemTongDto = BangDiemTongDto;
//# sourceMappingURL=bang-diem-tong.dto.js.map