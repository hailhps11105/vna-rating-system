import { Model } from 'mongoose';
import { BangDiemMonService } from '../bang-diem-mon/bang-diem-mon.service';
import { NguoiDungService } from '../nguoi-dung/nguoi-dung.service';
import { BangDiemTongDto } from './bang-diem-tong.dto';
import { BangDiemTongDocument } from './bang-diem-tong.entity';
export declare class BangDiemTongService {
    private model;
    private readonly ndSer;
    private readonly bdMSer;
    constructor(model: Model<BangDiemTongDocument>, ndSer: NguoiDungService, bdMSer: BangDiemMonService);
    create(dto: BangDiemTongDto): Promise<BangDiemTongDocument>;
    findAll(condition?: any): Promise<any[]>;
    getAll(condition?: any): Promise<any[]>;
    getOne_byHS(hs: string): Promise<any>;
    findOne(rec: string): Promise<{
        _id: string;
        hocSinh: {
            _id: any;
            hoTen: string;
        };
        lopHoc: {
            _id: any;
            maLH: string;
        };
        GVCN: {
            _id: any;
            hoTen: string;
        };
        hocKy1: import("./theoHK.schema").TheoHK;
        hocKy2: import("./theoHK.schema").TheoHK;
        caNam: import("./theoHK.schema").TheoHK;
        xepLoai: string;
        nhanXet: string;
    }>;
    update(id: string, dto: BangDiemTongDto): Promise<BangDiemTongDocument>;
    remove(id: string): Promise<BangDiemTongDocument>;
}
