"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BangDiemTongSchema = exports.BangDiemTong = void 0;
const openapi = require("@nestjs/swagger");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const nguoi_dung_entity_1 = require("../nguoi-dung/nguoi-dung.entity");
const theoHK_schema_1 = require("./theoHK.schema");
let BangDiemTong = class BangDiemTong {
    hocSinh;
    GVCN;
    hocKy1;
    hocKy2;
    caNam;
    bangDiemMon;
    nhanXet;
    xepLoai;
    static _OPENAPI_METADATA_FACTORY() {
        return { hocSinh: { required: true, type: () => require("../nguoi-dung/nguoi-dung.entity").NguoiDung }, GVCN: { required: true, type: () => require("../nguoi-dung/nguoi-dung.entity").NguoiDung }, hocKy1: { required: true, type: () => require("./theoHK.schema").TheoHK }, hocKy2: { required: true, type: () => require("./theoHK.schema").TheoHK }, caNam: { required: true, type: () => require("./theoHK.schema").TheoHK }, bangDiemMon: { required: true, type: () => [require("../bang-diem-mon/bang-diem-mon.entity").BangDiemMon] }, nhanXet: { required: true, type: () => String }, xepLoai: { required: true, type: () => String } };
    }
};
__decorate([
    mongoose_1.Prop({
        type: mongoose_2.Schema.Types.ObjectId,
        ref: 'nguoi_dung',
        required: true,
    }),
    __metadata("design:type", nguoi_dung_entity_1.NguoiDung)
], BangDiemTong.prototype, "hocSinh", void 0);
__decorate([
    mongoose_1.Prop({
        type: mongoose_2.Schema.Types.ObjectId,
        ref: 'nguoi_dung',
        required: true,
    }),
    __metadata("design:type", nguoi_dung_entity_1.NguoiDung)
], BangDiemTong.prototype, "GVCN", void 0);
__decorate([
    mongoose_1.Prop({ type: theoHK_schema_1.TheoHKSchema, default: {} }),
    __metadata("design:type", theoHK_schema_1.TheoHK)
], BangDiemTong.prototype, "hocKy1", void 0);
__decorate([
    mongoose_1.Prop({ type: theoHK_schema_1.TheoHKSchema, default: {} }),
    __metadata("design:type", theoHK_schema_1.TheoHK)
], BangDiemTong.prototype, "hocKy2", void 0);
__decorate([
    mongoose_1.Prop({ type: theoHK_schema_1.TheoHKSchema, default: {} }),
    __metadata("design:type", theoHK_schema_1.TheoHK)
], BangDiemTong.prototype, "caNam", void 0);
__decorate([
    mongoose_1.Prop({
        type: [
            {
                type: mongoose_2.Schema.Types.ObjectId,
                ref: 'bang_diem_mon',
            },
        ],
        default: [],
    }),
    __metadata("design:type", Array)
], BangDiemTong.prototype, "bangDiemMon", void 0);
__decorate([
    mongoose_1.Prop({ default: '' }),
    __metadata("design:type", String)
], BangDiemTong.prototype, "nhanXet", void 0);
__decorate([
    mongoose_1.Prop({ default: '' }),
    __metadata("design:type", String)
], BangDiemTong.prototype, "xepLoai", void 0);
BangDiemTong = __decorate([
    mongoose_1.Schema({
        timestamps: {
            createdAt: 'thoiDiemTao',
            updatedAt: 'thoiDiemSua',
        },
        versionKey: false,
    })
], BangDiemTong);
exports.BangDiemTong = BangDiemTong;
exports.BangDiemTongSchema = mongoose_1.SchemaFactory.createForClass(BangDiemTong);
//# sourceMappingURL=bang-diem-tong.entity.js.map