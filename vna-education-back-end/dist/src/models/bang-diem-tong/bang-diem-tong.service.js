"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BangDiemTongService = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const utilities_1 = require("../../helpers/utilities");
const bang_diem_mon_service_1 = require("../bang-diem-mon/bang-diem-mon.service");
const nguoi_dung_service_1 = require("../nguoi-dung/nguoi-dung.service");
let BangDiemTongService = class BangDiemTongService {
    model;
    ndSer;
    bdMSer;
    constructor(model, ndSer, bdMSer) {
        this.model = model;
        this.ndSer = ndSer;
        this.bdMSer = bdMSer;
    }
    async create(dto) {
        return await this.model.create({
            nhanXet: dto.nhanXet,
            xepLoai: dto.xepLoai,
            hocKy1: {
                hanhKiem: dto.hanhKiem_hk1,
                hocLuc: dto.hocLuc_hk1,
                diemTB: dto.diemTB_hk1,
            },
            hocKy2: {
                hocLuc: dto.hocLuc_hk2,
                hanhKiem: dto.hanhKiem_hk2,
                diemTB: dto.diemTB_hk2,
            },
            caNam: {
                hocLuc: dto.hocLuc_caNam,
                hanhKiem: dto.hanhKiem_caNam,
                diemTB: dto.diemTB_caNam,
            },
            hocSinh: mongoose_2.Types.ObjectId(dto.hocSinh),
            GVCN: mongoose_2.Types.ObjectId(dto.GVCN),
            bangDiemMon: utilities_1.bulkObjectID(dto.bangDiemMon),
        });
    }
    async findAll(condition = {}) {
        const all = await this.model
            .find(condition)
            .populate([
            {
                path: 'hocSinh',
                select: ['hoTen', 'lopHoc'],
                populate: {
                    path: 'lopHoc',
                    select: 'maLH',
                },
            },
            { path: 'GVCN', select: 'hoTen' },
        ])
            .exec();
        const result = [];
        for (let i = 0; i < all.length; i++) {
            result.push({
                _id: all[i]._id,
                hocSinh: {
                    _id: all[i].populated('hocSinh'),
                    hoTen: all[i].hocSinh?.hoTen,
                },
                lopHoc: {
                    _id: all[i].populated('lopHoc'),
                    maLH: all[i].hocSinh?.lopHoc.maLH,
                },
                GVCN: {
                    _id: all[i].populated('GVCN'),
                    hoTen: all[i].GVCN?.hoTen,
                },
                hocKy1: all[i].hocKy1,
                hocKy2: all[i].hocKy2,
                caNam: all[i].caNam,
                xepLoai: all[i].xepLoai,
                nhanXet: all[i].nhanXet,
            });
        }
        return result;
    }
    async getAll(condition = {}) {
        const all = await this.model
            .find(condition)
            .populate([
            {
                path: 'hocSinh',
                select: ['hoTen', 'lopHoc'],
                populate: {
                    path: 'lopHoc',
                    select: 'maLH',
                },
            },
            { path: 'GVCN', select: 'hoTen' },
        ])
            .exec();
        const result = [];
        for (let i = 0; i < all.length; i++) {
            result.push({
                _id: all[i]._id,
                hocSinh: all[i].hocSinh?.hoTen,
                lopHoc: all[i].hocSinh?.lopHoc.maLH,
                GVCN: all[i].GVCN?.hoTen,
                hocKy1: all[i].hocKy1,
                hocKy2: all[i].hocKy2,
                caNam: all[i].caNam,
                xepLoai: all[i].xepLoai,
                nhanXet: all[i].nhanXet,
            });
        }
        return result;
    }
    async getOne_byHS(hs) {
        const m = await this.bdMSer.getAll_byHS(hs);
        const t = await this.getAll({ hocSinh: Object(hs) });
        return { ...t[0], bangDiemMon: m };
    }
    async findOne(rec) {
        const bd = await (await this.model.findById(rec))
            .populate([
            {
                path: 'hocSinh',
                model: 'nguoi_dung',
                populate: {
                    path: 'lopHoc',
                    model: 'lop_hoc',
                },
            },
            { path: 'GVCN', model: 'nguoi_dung' },
        ])
            .execPopulate();
        return {
            _id: rec,
            hocSinh: {
                _id: bd.populated('hocSinh'),
                hoTen: bd.hocSinh?.hoTen,
            },
            lopHoc: {
                _id: bd.populated('lopHoc'),
                maLH: bd.hocSinh?.lopHoc.maLH,
            },
            GVCN: {
                _id: bd.populated('GVCN'),
                hoTen: bd.GVCN?.hoTen,
            },
            hocKy1: bd.hocKy1,
            hocKy2: bd.hocKy2,
            caNam: bd.caNam,
            xepLoai: bd.xepLoai,
            nhanXet: bd.nhanXet,
        };
    }
    async update(id, dto) {
        return await this.model.findById(id, null, null, async (err, doc) => {
            if (err)
                throw err;
            if (dto.nhanXet)
                doc.nhanXet = dto.nhanXet;
            if (dto.xepLoai)
                doc.xepLoai = dto.xepLoai;
            if (dto.hanhKiem_hk1)
                doc.hocKy1.hanhKiem = dto.hanhKiem_hk1;
            if (dto.hocLuc_hk1)
                doc.hocKy1.hocLuc = dto.hocLuc_hk1;
            if (dto.diemTB_hk1)
                doc.hocKy1.diemTB = dto.diemTB_hk1;
            if (dto.hanhKiem_hk2)
                doc.hocKy2.hanhKiem = dto.hanhKiem_hk2;
            if (dto.hocLuc_hk2)
                doc.hocKy2.hocLuc = dto.hocLuc_hk2;
            if (dto.diemTB_hk2)
                doc.hocKy2.diemTB = dto.diemTB_hk2;
            if (dto.hanhKiem_caNam)
                doc.caNam.hanhKiem = dto.hanhKiem_caNam;
            if (dto.hocLuc_caNam)
                doc.caNam.hocLuc = dto.hocLuc_caNam;
            if (dto.diemTB_caNam)
                doc.caNam.diemTB = dto.diemTB_caNam;
            if (dto.hocSinh)
                doc.hocSinh = await this.ndSer.objectify(dto.hocSinh);
            if (dto.GVCN)
                doc.GVCN = await this.ndSer.objectify(dto.GVCN);
            if (dto.bangDiemMon)
                doc.bangDiemMon = await this.bdMSer.bulkObjectify(dto.bangDiemMon);
            await doc.save();
        });
    }
    async remove(id) {
        return await this.model.findByIdAndDelete(id);
    }
};
BangDiemTongService = __decorate([
    common_1.Injectable(),
    __param(0, mongoose_1.InjectModel('bang_diem_tong')),
    __metadata("design:paramtypes", [mongoose_2.Model,
        nguoi_dung_service_1.NguoiDungService,
        bang_diem_mon_service_1.BangDiemMonService])
], BangDiemTongService);
exports.BangDiemTongService = BangDiemTongService;
//# sourceMappingURL=bang-diem-tong.service.js.map