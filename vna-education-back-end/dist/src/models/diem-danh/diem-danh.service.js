"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DiemDanhService = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const nguoi_dung_service_1 = require("../nguoi-dung/nguoi-dung.service");
let DiemDanhService = class DiemDanhService {
    model;
    ndSer;
    constructor(model, ndSer) {
        this.model = model;
        this.ndSer = ndSer;
    }
    async create(dto) {
        return await this.model.create({
            hocSinh: mongoose_2.Types.ObjectId(dto.hocSinh),
            trangThai: dto.trangThai,
            ghiChu: dto.ghiChu,
        });
    }
    async findAll(condition = {}) {
        const all = await this.model
            .find(condition)
            .populate({ path: 'hocSinh', select: 'hoTen' })
            .exec();
        const result = [];
        for (let i = 0; i < all.length; i++) {
            result.push({
                _id: all[i]._id,
                hocSinh: {
                    _id: all[i].populated('hocSinh'),
                    hoTen: all[i].hocSinh?.hoTen,
                },
                trangThai: all[i].trangThai,
                ghiChu: all[i].ghiChu,
            });
        }
        return result;
    }
    async findOne(dd) {
        const one = await (await this.model.findById(dd))
            .populate({ path: 'hocSinh', select: 'hoTen' })
            .execPopulate();
        return {
            id: dd,
            hocSinh: {
                _id: one.populated('hocSinh'),
                hoTen: one.hocSinh?.hoTen,
            },
            trangThai: one.trangThai,
            ghiChu: one.ghiChu,
        };
    }
    async update(id, dto) {
        return await this.model.findById(id, null, null, async (err, doc) => {
            if (err)
                throw err;
            if (dto.hocSinh)
                doc.hocSinh = await this.ndSer.objectify(dto.hocSinh);
            if (dto.trangThai)
                doc.trangThai = dto.trangThai;
            if (dto.ghiChu)
                doc.trangThai = dto.trangThai;
            await doc.save();
        });
    }
    async objectify(dd) {
        return (await this.model.findById(dd))._id;
    }
    async bulkObjectify(dd) {
        const result = [];
        for (let i = 0; i < dd.length; i++) {
            result.push(await this.objectify(dd[i]));
        }
        return result;
    }
    async remove(id) {
        return await this.model.findByIdAndDelete(id);
    }
};
DiemDanhService = __decorate([
    common_1.Injectable(),
    __param(0, mongoose_1.InjectModel('diem_danh')),
    __metadata("design:paramtypes", [mongoose_2.Model,
        nguoi_dung_service_1.NguoiDungService])
], DiemDanhService);
exports.DiemDanhService = DiemDanhService;
//# sourceMappingURL=diem-danh.service.js.map