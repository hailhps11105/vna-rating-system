import { DiemDanhDto } from './diem-danh.dto';
import { DiemDanhService } from './diem-danh.service';
export declare class DiemDanhController {
    private readonly service;
    constructor(service: DiemDanhService);
    create(dto: DiemDanhDto): Promise<import("./diem-danh.entity").DiemDanhDocument>;
    findAll(): Promise<any[]>;
    findOne(id: string): Promise<{
        id: string;
        hocSinh: {
            _id: any;
            hoTen: string;
        };
        trangThai: boolean;
        ghiChu: string;
    }>;
    update(id: string, dto: DiemDanhDto): Promise<import("./diem-danh.entity").DiemDanhDocument>;
    remove(id: string): Promise<import("./diem-danh.entity").DiemDanhDocument>;
}
