"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DiemDanhDto = void 0;
const openapi = require("@nestjs/swagger");
class DiemDanhDto {
    hocSinh;
    ghiChu;
    trangThai;
    static _OPENAPI_METADATA_FACTORY() {
        return { hocSinh: { required: true, type: () => String }, ghiChu: { required: true, type: () => String }, trangThai: { required: true, type: () => Boolean } };
    }
}
exports.DiemDanhDto = DiemDanhDto;
//# sourceMappingURL=diem-danh.dto.js.map