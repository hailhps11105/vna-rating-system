import { Model } from 'mongoose';
import { NguoiDungService } from '../nguoi-dung/nguoi-dung.service';
import { DiemDanhDto } from './diem-danh.dto';
import { DiemDanhDocument } from './diem-danh.entity';
export declare class DiemDanhService {
    private model;
    private readonly ndSer;
    constructor(model: Model<DiemDanhDocument>, ndSer: NguoiDungService);
    create(dto: DiemDanhDto): Promise<DiemDanhDocument>;
    findAll(condition?: any): Promise<any[]>;
    findOne(dd: string): Promise<{
        id: string;
        hocSinh: {
            _id: any;
            hoTen: string;
        };
        trangThai: boolean;
        ghiChu: string;
    }>;
    update(id: string, dto: DiemDanhDto): Promise<DiemDanhDocument>;
    objectify(dd: string): Promise<any>;
    bulkObjectify(dd: string[]): Promise<any[]>;
    remove(id: string): Promise<DiemDanhDocument>;
}
