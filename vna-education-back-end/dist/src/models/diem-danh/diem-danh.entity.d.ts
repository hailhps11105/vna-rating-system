import { Document, Schema as MongooseSchema } from 'mongoose';
import { NguoiDung } from '../nguoi-dung/nguoi-dung.entity';
export declare type DiemDanhDocument = DiemDanh & Document;
export declare class DiemDanh {
    hocSinh: NguoiDung;
    ghiChu: string;
    trangThai: boolean;
}
export declare const DiemDanhSchema: MongooseSchema<Document<DiemDanh, any, any>, import("mongoose").Model<any, any, any>, undefined, any>;
