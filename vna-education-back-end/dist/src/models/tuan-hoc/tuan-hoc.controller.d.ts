import { TuanHocService } from './tuan-hoc.service';
import { TuanHocDto } from './tuan-hoc.dto';
export declare class TuanHocController {
    private readonly service;
    constructor(service: TuanHocService);
    create(dto: TuanHocDto): Promise<import("./tuan-hoc.entity").TuanHocDocument>;
    findAll(): Promise<any[]>;
    findOne(id: string): Promise<{
        _id: any;
        soTuan: number;
        tenTuan: string;
        ngayBatDau: string;
        ngayKetThuc: string;
        hocKy: number;
    }>;
    update(id: string, dto: TuanHocDto): Promise<import("./tuan-hoc.entity").TuanHocDocument>;
    remove(id: string): Promise<import("./tuan-hoc.entity").TuanHocDocument>;
}
