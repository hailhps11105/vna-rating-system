"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TuanHocDto = void 0;
const openapi = require("@nestjs/swagger");
class TuanHocDto {
    soTuan;
    tenTuan;
    ngayBatDau;
    ngayKetThuc;
    hocKy;
    static _OPENAPI_METADATA_FACTORY() {
        return { soTuan: { required: true, type: () => Number }, tenTuan: { required: true, type: () => String }, ngayBatDau: { required: true, type: () => String }, ngayKetThuc: { required: true, type: () => String }, hocKy: { required: true, type: () => Number } };
    }
}
exports.TuanHocDto = TuanHocDto;
//# sourceMappingURL=tuan-hoc.dto.js.map