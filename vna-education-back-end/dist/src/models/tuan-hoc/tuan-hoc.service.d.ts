import { Model } from 'mongoose';
import { TuanHocDto } from './tuan-hoc.dto';
import { TuanHocDocument } from './tuan-hoc.entity';
export declare class TuanHocService {
    private model;
    constructor(model: Model<TuanHocDocument>);
    create(dto: TuanHocDto): Promise<TuanHocDocument>;
    findAll(): Promise<any[]>;
    findOne(id: string): Promise<{
        _id: any;
        soTuan: number;
        tenTuan: string;
        ngayBatDau: string;
        ngayKetThuc: string;
        hocKy: number;
    }>;
    update(id: string, dto: TuanHocDto): Promise<TuanHocDocument>;
    objectify(tuan: string): Promise<any>;
    bulkObjectify(tuan: string[]): Promise<any[]>;
    remove(id: string): Promise<TuanHocDocument>;
}
