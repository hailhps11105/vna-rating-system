"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TuanHocService = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const utilities_1 = require("../../helpers/utilities");
let TuanHocService = class TuanHocService {
    model;
    constructor(model) {
        this.model = model;
    }
    async create(dto) {
        return await this.model.create(dto);
    }
    async findAll() {
        const all = await this.model.find();
        const result = [];
        for (let i = 0; i < all.length; i++) {
            result.push({
                _id: all[i]._id,
                soTuan: all[i].soTuan,
                tenTuan: all[i].tenTuan,
                ngayBatDau: all[i].ngayBatDau,
                ngayKetThuc: all[i].ngayKetThuc,
                hocKy: all[i].hocKy,
            });
        }
        return result;
    }
    async findOne(id) {
        const tuan = await this.model.findById(id);
        return {
            _id: tuan._id,
            soTuan: tuan.soTuan,
            tenTuan: tuan.tenTuan,
            ngayBatDau: tuan.ngayBatDau,
            ngayKetThuc: tuan.ngayKetThuc,
            hocKy: tuan.hocKy,
        };
    }
    async update(id, dto) {
        return await this.model.findById(id, null, null, async (err, doc) => {
            if (err)
                throw err;
            utilities_1.assign(dto, doc);
            await doc.save();
        });
    }
    async objectify(tuan) {
        return (await this.model.findById(tuan))._id;
    }
    async bulkObjectify(tuan) {
        const result = [];
        for (let i = 0; i < tuan.length; i++) {
            result.push(await this.objectify(tuan[i]));
        }
        return result;
    }
    async remove(id) {
        return await this.model.findByIdAndDelete(id);
    }
};
TuanHocService = __decorate([
    common_1.Injectable(),
    __param(0, mongoose_1.InjectModel('tuan_hoc')),
    __metadata("design:paramtypes", [mongoose_2.Model])
], TuanHocService);
exports.TuanHocService = TuanHocService;
//# sourceMappingURL=tuan-hoc.service.js.map