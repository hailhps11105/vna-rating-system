"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TuanHocModule = void 0;
const common_1 = require("@nestjs/common");
const tuan_hoc_service_1 = require("./tuan-hoc.service");
const tuan_hoc_controller_1 = require("./tuan-hoc.controller");
const mongoose_1 = require("@nestjs/mongoose");
const tuan_hoc_entity_1 = require("./tuan-hoc.entity");
const nguoi_dung_module_1 = require("../nguoi-dung/nguoi-dung.module");
let TuanHocModule = class TuanHocModule {
};
TuanHocModule = __decorate([
    common_1.Module({
        imports: [
            mongoose_1.MongooseModule.forFeature([
                {
                    name: 'tuan_hoc',
                    collection: 'tuan_hoc',
                    schema: tuan_hoc_entity_1.TuanHocSchema,
                },
            ]),
            common_1.forwardRef(() => nguoi_dung_module_1.NguoiDungModule),
        ],
        controllers: [tuan_hoc_controller_1.TuanHocController],
        providers: [tuan_hoc_service_1.TuanHocService],
        exports: [tuan_hoc_service_1.TuanHocService],
    })
], TuanHocModule);
exports.TuanHocModule = TuanHocModule;
//# sourceMappingURL=tuan-hoc.module.js.map