import { Document } from 'mongoose';
export declare type TuanHocDocument = TuanHoc & Document;
export declare class TuanHoc {
    soTuan: number;
    tenTuan: string;
    ngayBatDau: string;
    ngayKetThuc: string;
    hocKy: number;
}
export declare const TuanHocSchema: import("mongoose").Schema<Document<TuanHoc, any, any>, import("mongoose").Model<any, any, any>, undefined, any>;
