"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TuanHocSchema = exports.TuanHoc = void 0;
const openapi = require("@nestjs/swagger");
const mongoose_1 = require("@nestjs/mongoose");
let TuanHoc = class TuanHoc {
    soTuan;
    tenTuan;
    ngayBatDau;
    ngayKetThuc;
    hocKy;
    static _OPENAPI_METADATA_FACTORY() {
        return { soTuan: { required: true, type: () => Number }, tenTuan: { required: true, type: () => String }, ngayBatDau: { required: true, type: () => String }, ngayKetThuc: { required: true, type: () => String }, hocKy: { required: true, type: () => Number } };
    }
};
__decorate([
    mongoose_1.Prop({ default: 0, required: true }),
    __metadata("design:type", Number)
], TuanHoc.prototype, "soTuan", void 0);
__decorate([
    mongoose_1.Prop({ default: 'Tuần 0', required: true }),
    __metadata("design:type", String)
], TuanHoc.prototype, "tenTuan", void 0);
__decorate([
    mongoose_1.Prop({ default: Date.now() }),
    __metadata("design:type", String)
], TuanHoc.prototype, "ngayBatDau", void 0);
__decorate([
    mongoose_1.Prop({ default: Date.now() }),
    __metadata("design:type", String)
], TuanHoc.prototype, "ngayKetThuc", void 0);
__decorate([
    mongoose_1.Prop({ required: true }),
    __metadata("design:type", Number)
], TuanHoc.prototype, "hocKy", void 0);
TuanHoc = __decorate([
    mongoose_1.Schema({
        timestamps: {
            createdAt: 'thoiDiemTao',
            updatedAt: 'thoiDiemSua',
        },
        versionKey: false,
    })
], TuanHoc);
exports.TuanHoc = TuanHoc;
exports.TuanHocSchema = mongoose_1.SchemaFactory.createForClass(TuanHoc);
//# sourceMappingURL=tuan-hoc.entity.js.map