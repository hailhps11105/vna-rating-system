"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MauDanhGiaDto = void 0;
const openapi = require("@nestjs/swagger");
const swagger_1 = require("@nestjs/swagger");
class MauDanhGiaDto {
    tenMau;
    ghiChu;
    tieuChi;
    static _OPENAPI_METADATA_FACTORY() {
        return { tenMau: { required: true, type: () => String }, ghiChu: { required: true, type: () => String }, tieuChi: { required: true, type: () => [Object] } };
    }
}
__decorate([
    swagger_1.ApiProperty({
        required: true,
        name: 'tenMau',
        type: String,
        example: 'Mẫu đánh giá gì đó',
        description: 'Tên gọi mẫu đánh giá',
    }),
    __metadata("design:type", String)
], MauDanhGiaDto.prototype, "tenMau", void 0);
__decorate([
    swagger_1.ApiPropertyOptional({
        name: 'ghiChu',
        type: String,
        example: 'abcd',
        description: 'Ghi chú về mẫu đánh giá của người tạo',
    }),
    __metadata("design:type", String)
], MauDanhGiaDto.prototype, "ghiChu", void 0);
__decorate([
    swagger_1.ApiProperty({
        required: true,
        name: 'tieuChi',
        type: 'array',
        example: "[{tenTC: 'abc', moTa: 'abc', mucTieu: [noiDung: 'cba',luaChon: [{'a',1},{'b',2},]]}]",
        description: 'Mẫu đánh giá đặc thù dành cho môn học',
    }),
    __metadata("design:type", Array)
], MauDanhGiaDto.prototype, "tieuChi", void 0);
exports.MauDanhGiaDto = MauDanhGiaDto;
//# sourceMappingURL=mau-danh-gia.dto.js.map