import { Model } from 'mongoose';
import { DanhGiaService } from '../danh-gia/danh-gia.service';
import { MauDanhGiaDto } from './mau-danh-gia.dto';
import { MauDanhGiaDocument } from './mau-danh-gia.entity';
export declare class MauDanhGiaService {
    private model;
    private readonly dgSer;
    constructor(model: Model<MauDanhGiaDocument>, dgSer: DanhGiaService);
    create(dto: MauDanhGiaDto): Promise<MauDanhGiaDocument>;
    findAll(): Promise<any[]>;
    findOne(mau: string): Promise<{
        _id: string;
        tenMau: string;
        ghiChu: string;
        tieuChi: import("./tieuChi.schema").TieuChi[];
    }>;
    update(id: string, dto: MauDanhGiaDto): Promise<MauDanhGiaDocument>;
    objectify(id: string): Promise<any>;
    remove(id: string): Promise<MauDanhGiaDocument>;
}
