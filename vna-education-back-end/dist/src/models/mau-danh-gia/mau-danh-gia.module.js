"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MauDanhGiaModule = void 0;
const common_1 = require("@nestjs/common");
const mau_danh_gia_service_1 = require("./mau-danh-gia.service");
const mau_danh_gia_controller_1 = require("./mau-danh-gia.controller");
const mongoose_1 = require("@nestjs/mongoose");
const mau_danh_gia_entity_1 = require("./mau-danh-gia.entity");
const nguoi_dung_module_1 = require("../nguoi-dung/nguoi-dung.module");
const danh_gia_module_1 = require("../danh-gia/danh-gia.module");
let MauDanhGiaModule = class MauDanhGiaModule {
};
MauDanhGiaModule = __decorate([
    common_1.Module({
        imports: [
            mongoose_1.MongooseModule.forFeature([
                {
                    name: 'mau_danh_gia',
                    collection: 'mau_danh_gia',
                    schema: mau_danh_gia_entity_1.MauDanhGiaSchema,
                },
            ]),
            nguoi_dung_module_1.NguoiDungModule,
            common_1.forwardRef(() => danh_gia_module_1.DanhGiaModule),
        ],
        controllers: [mau_danh_gia_controller_1.MauDanhGiaController],
        providers: [mau_danh_gia_service_1.MauDanhGiaService],
        exports: [mau_danh_gia_service_1.MauDanhGiaService],
    })
], MauDanhGiaModule);
exports.MauDanhGiaModule = MauDanhGiaModule;
//# sourceMappingURL=mau-danh-gia.module.js.map