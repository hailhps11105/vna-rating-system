"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MauDanhGiaSchema = exports.MauDanhGia = void 0;
const openapi = require("@nestjs/swagger");
const mongoose_1 = require("@nestjs/mongoose");
const tieuChi_schema_1 = require("./tieuChi.schema");
let MauDanhGia = class MauDanhGia {
    tenMau;
    ghiChu;
    tieuChi;
    static _OPENAPI_METADATA_FACTORY() {
        return { tenMau: { required: true, type: () => String }, ghiChu: { required: true, type: () => String }, tieuChi: { required: true, type: () => [require("./tieuChi.schema").TieuChi] } };
    }
};
__decorate([
    mongoose_1.Prop({ required: true, unique: true, index: true }),
    __metadata("design:type", String)
], MauDanhGia.prototype, "tenMau", void 0);
__decorate([
    mongoose_1.Prop(),
    __metadata("design:type", String)
], MauDanhGia.prototype, "ghiChu", void 0);
__decorate([
    mongoose_1.Prop({
        required: true,
        type: [tieuChi_schema_1.TieuChiSchema],
    }),
    __metadata("design:type", Array)
], MauDanhGia.prototype, "tieuChi", void 0);
MauDanhGia = __decorate([
    mongoose_1.Schema({
        timestamps: {
            createdAt: 'thoiDiemTao',
            updatedAt: 'thoiDiemSua',
        },
        versionKey: false,
    })
], MauDanhGia);
exports.MauDanhGia = MauDanhGia;
exports.MauDanhGiaSchema = mongoose_1.SchemaFactory.createForClass(MauDanhGia);
//# sourceMappingURL=mau-danh-gia.entity.js.map