"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TieuChiSchema = exports.TieuChi = void 0;
const mongoose_1 = require("@nestjs/mongoose");
const mucTieu_schema_1 = require("./mucTieu.schema");
let TieuChi = class TieuChi {
    tenTC;
    id;
    noiDung;
    mucTieu;
};
__decorate([
    mongoose_1.Prop({ required: true }),
    __metadata("design:type", String)
], TieuChi.prototype, "tenTC", void 0);
__decorate([
    mongoose_1.Prop(),
    __metadata("design:type", Number)
], TieuChi.prototype, "id", void 0);
__decorate([
    mongoose_1.Prop(),
    __metadata("design:type", String)
], TieuChi.prototype, "noiDung", void 0);
__decorate([
    mongoose_1.Prop({
        required: true,
        type: [mucTieu_schema_1.MucTieuSchema],
    }),
    __metadata("design:type", Array)
], TieuChi.prototype, "mucTieu", void 0);
TieuChi = __decorate([
    mongoose_1.Schema({
        timestamps: {
            createdAt: 'thoiDiemTao',
            updatedAt: 'thoiDiemSua',
        },
        versionKey: false,
    })
], TieuChi);
exports.TieuChi = TieuChi;
exports.TieuChiSchema = mongoose_1.SchemaFactory.createForClass(TieuChi);
//# sourceMappingURL=tieuChi.schema.js.map