"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MauDanhGiaService = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const utilities_1 = require("../../helpers/utilities");
const danh_gia_service_1 = require("../danh-gia/danh-gia.service");
let MauDanhGiaService = class MauDanhGiaService {
    model;
    dgSer;
    constructor(model, dgSer) {
        this.model = model;
        this.dgSer = dgSer;
    }
    async create(dto) {
        const to404 = await this.model.find({
            tenMau: dto.tenMau,
        });
        if (to404.length > 0)
            return null;
        else
            return await this.model.create(dto);
    }
    async findAll() {
        const all = await this.model.find({});
        const result = [];
        for (let i = 0; i < all.length; i++) {
            result.push({
                _id: all[i]._id,
                tenMau: all[i].tenMau,
                ghiChu: all[i].ghiChu,
                tieuChi: all[i].tieuChi,
            });
        }
        return result;
    }
    async findOne(mau) {
        const one = await this.model.findById(mau);
        return {
            _id: mau,
            tenMau: one.tenMau,
            ghiChu: one.ghiChu,
            tieuChi: one.tieuChi,
        };
    }
    async update(id, dto) {
        return await this.model.findById(id, null, null, async (err, doc) => {
            if (err)
                throw err;
            utilities_1.assign(dto, doc);
            await doc.save();
        });
    }
    async objectify(id) {
        return (await this.model.findById(id))._id;
    }
    async remove(id) {
        const revs = await this.dgSer.findAll();
        const name = (await this.findOne(id)).tenMau;
        for (let i = 0; i < revs.length; i++) {
            if (revs[i].mauDG === name) {
                return null;
            }
        }
        return await this.model.findByIdAndDelete(id);
    }
};
MauDanhGiaService = __decorate([
    common_1.Injectable(),
    __param(0, mongoose_1.InjectModel('mau_danh_gia')),
    __param(1, common_1.Inject(common_1.forwardRef(() => danh_gia_service_1.DanhGiaService))),
    __metadata("design:paramtypes", [mongoose_2.Model,
        danh_gia_service_1.DanhGiaService])
], MauDanhGiaService);
exports.MauDanhGiaService = MauDanhGiaService;
//# sourceMappingURL=mau-danh-gia.service.js.map