import { Document } from 'mongoose';
import { TieuChi } from './tieuChi.schema';
export declare type MauDanhGiaDocument = MauDanhGia & Document;
export declare class MauDanhGia {
    tenMau: string;
    ghiChu: string;
    tieuChi: TieuChi[];
}
export declare const MauDanhGiaSchema: import("mongoose").Schema<Document<MauDanhGia, any, any>, import("mongoose").Model<any, any, any>, undefined, any>;
