import { MauDanhGiaService } from './mau-danh-gia.service';
import { MauDanhGiaDto } from './mau-danh-gia.dto';
export declare class MauDanhGiaController {
    private readonly service;
    constructor(service: MauDanhGiaService);
    create(dto: MauDanhGiaDto): Promise<import("./mau-danh-gia.entity").MauDanhGiaDocument>;
    findAll(): Promise<any[]>;
    findOne(id: string): Promise<{
        _id: string;
        tenMau: string;
        ghiChu: string;
        tieuChi: import("./tieuChi.schema").TieuChi[];
    }>;
    update(id: string, dto: MauDanhGiaDto): Promise<import("./mau-danh-gia.entity").MauDanhGiaDocument>;
    remove(id: string): Promise<import("./mau-danh-gia.entity").MauDanhGiaDocument>;
}
