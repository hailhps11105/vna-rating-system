/// <reference types="mongoose" />
import { MucTieu } from './mucTieu.schema';
export declare class TieuChi {
    tenTC: string;
    id: number;
    noiDung: string;
    mucTieu: MucTieu[];
}
export declare const TieuChiSchema: import("mongoose").Schema<import("mongoose").Document<TieuChi, any, any>, import("mongoose").Model<any, any, any>, undefined, any>;
