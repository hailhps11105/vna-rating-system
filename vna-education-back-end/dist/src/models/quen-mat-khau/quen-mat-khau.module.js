"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.QuenMatKhauModule = void 0;
const common_1 = require("@nestjs/common");
const quen_mat_khau_service_1 = require("./quen-mat-khau.service");
const quen_mat_khau_controller_1 = require("./quen-mat-khau.controller");
const nguoi_dung_module_1 = require("../nguoi-dung/nguoi-dung.module");
const mongoose_1 = require("@nestjs/mongoose");
const quen_mat_khau_entity_1 = require("./quen-mat-khau.entity");
let QuenMatKhauModule = class QuenMatKhauModule {
};
QuenMatKhauModule = __decorate([
    common_1.Module({
        imports: [
            mongoose_1.MongooseModule.forFeature([
                {
                    name: 'quen_matKhau',
                    collection: 'quen_matKhau',
                    schema: quen_mat_khau_entity_1.QuenMatKhauSchema,
                },
            ]),
            nguoi_dung_module_1.NguoiDungModule,
        ],
        controllers: [quen_mat_khau_controller_1.QuenMatKhauController],
        providers: [quen_mat_khau_service_1.QuenMatKhauService],
        exports: [quen_mat_khau_service_1.QuenMatKhauService],
    })
], QuenMatKhauModule);
exports.QuenMatKhauModule = QuenMatKhauModule;
//# sourceMappingURL=quen-mat-khau.module.js.map