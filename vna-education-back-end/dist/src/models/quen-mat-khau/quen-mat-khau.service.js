"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.QuenMatKhauService = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
let QuenMatKhauService = class QuenMatKhauService {
    model;
    constructor(model) {
        this.model = model;
    }
    async create(dto) {
        return await this.model.create({
            emailND: dto.emailND,
            nguoiDung: mongoose_2.Types.ObjectId(dto.nguoiDung),
            conHan: true,
        });
    }
    async findAll() {
        const all = await this.model
            .find()
            .populate({ path: 'nguoiDung', select: 'hoTen' })
            .exec();
        const result = [];
        for (let i = 0; i < all.length; i++) {
            result.push({
                _id: all[i]._id,
                emailND: all[i].emailND,
                nguoiDung: all[i].nguoiDung
                    ? {
                        _id: all[i].populated('nguoiDung'),
                        hoTen: all[i].nguoiDung.hoTen,
                    }
                    : null,
                conHan: all[i].conHan,
            });
        }
        return result;
    }
    async findOne(id) {
        const one = await this.model
            .findById(id)
            .populate({ path: 'nguoiDung', select: 'hoTen' })
            .exec();
        return {
            _id: id,
            emailND: one.emailND,
            nguoiDung: one.nguoiDung
                ? {
                    _id: one.populated('nguoiDung'),
                    hoTen: one.nguoiDung.hoTen,
                }
                : null,
            conHan: one.conHan,
        };
    }
    async check_conHan(id) {
        const one = await this.model.findById(id);
        return one.conHan;
    }
    async update(id, dto) {
        return await this.model.findById(id, null, null, async (err, doc) => {
            if (err)
                throw err;
            if (dto.emailND)
                doc.emailND = dto.emailND;
            doc.conHan = dto.conHan ? dto.conHan : false;
            await doc.save();
        });
    }
    async remove(id) {
        return await this.model.findByIdAndDelete(id);
    }
};
QuenMatKhauService = __decorate([
    common_1.Injectable(),
    __param(0, mongoose_1.InjectModel('quen_matKhau')),
    __metadata("design:paramtypes", [mongoose_2.Model])
], QuenMatKhauService);
exports.QuenMatKhauService = QuenMatKhauService;
//# sourceMappingURL=quen-mat-khau.service.js.map