"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.QuenMatKhauSchema = exports.QuenMatKhau = void 0;
const openapi = require("@nestjs/swagger");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const nguoi_dung_entity_1 = require("../nguoi-dung/nguoi-dung.entity");
let QuenMatKhau = class QuenMatKhau {
    nguoiDung;
    emailND;
    conHan;
    static _OPENAPI_METADATA_FACTORY() {
        return { nguoiDung: { required: true, type: () => require("../nguoi-dung/nguoi-dung.entity").NguoiDung }, emailND: { required: true, type: () => String }, conHan: { required: true, type: () => Boolean } };
    }
};
__decorate([
    mongoose_1.Prop({
        type: mongoose_2.Schema.Types.ObjectId,
        required: true,
        ref: 'nguoi_dung',
    }),
    __metadata("design:type", nguoi_dung_entity_1.NguoiDung)
], QuenMatKhau.prototype, "nguoiDung", void 0);
__decorate([
    mongoose_1.Prop({ required: true }),
    __metadata("design:type", String)
], QuenMatKhau.prototype, "emailND", void 0);
__decorate([
    mongoose_1.Prop({ default: true }),
    __metadata("design:type", Boolean)
], QuenMatKhau.prototype, "conHan", void 0);
QuenMatKhau = __decorate([
    mongoose_1.Schema({
        timestamps: {
            createdAt: 'thoiDiemTao',
            updatedAt: 'thoiDiemSua',
        },
        versionKey: false,
    })
], QuenMatKhau);
exports.QuenMatKhau = QuenMatKhau;
exports.QuenMatKhauSchema = mongoose_1.SchemaFactory.createForClass(QuenMatKhau);
//# sourceMappingURL=quen-mat-khau.entity.js.map