"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.QuenMatKhauController = void 0;
const openapi = require("@nestjs/swagger");
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const auth_guard_1 = require("../../auth.guard");
const quen_mat_khau_dto_1 = require("./quen-mat-khau.dto");
const quen_mat_khau_service_1 = require("./quen-mat-khau.service");
let QuenMatKhauController = class QuenMatKhauController {
    service;
    constructor(service) {
        this.service = service;
    }
    async create(dto) {
        return await this.service.create(dto);
    }
    async findAll() {
        return await this.service.findAll();
    }
    async kiemTra_conHan(id) {
        return await this.service.check_conHan(id);
    }
    async findOne(id) {
        return await this.service.findOne(id);
    }
    async update(id, dto) {
        return await this.service.update(id, dto);
    }
    async remove(id) {
        return await this.service.remove(id);
    }
};
__decorate([
    common_1.Post(),
    openapi.ApiResponse({ status: 201, type: Object }),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [quen_mat_khau_dto_1.QuenMatKhauDto]),
    __metadata("design:returntype", Promise)
], QuenMatKhauController.prototype, "create", null);
__decorate([
    common_1.Get(),
    openapi.ApiResponse({ status: 200, type: [Object] }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], QuenMatKhauController.prototype, "findAll", null);
__decorate([
    common_1.Get('con-han/:id'),
    openapi.ApiResponse({ status: 200, type: Boolean }),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], QuenMatKhauController.prototype, "kiemTra_conHan", null);
__decorate([
    common_1.Get(':id'),
    openapi.ApiResponse({ status: 200 }),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], QuenMatKhauController.prototype, "findOne", null);
__decorate([
    common_1.Patch(':id'),
    openapi.ApiResponse({ status: 200, type: Object }),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, quen_mat_khau_dto_1.QuenMatKhauDto]),
    __metadata("design:returntype", Promise)
], QuenMatKhauController.prototype, "update", null);
__decorate([
    common_1.Delete(':id'),
    openapi.ApiResponse({ status: 200, type: Object }),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], QuenMatKhauController.prototype, "remove", null);
QuenMatKhauController = __decorate([
    common_1.Controller('quen-mat-khau'),
    swagger_1.ApiTags('quen-mat-khau'),
    common_1.UseGuards(auth_guard_1.AuthGuard),
    __metadata("design:paramtypes", [quen_mat_khau_service_1.QuenMatKhauService])
], QuenMatKhauController);
exports.QuenMatKhauController = QuenMatKhauController;
//# sourceMappingURL=quen-mat-khau.controller.js.map