import { Model } from 'mongoose';
import { QuenMatKhauDto } from './quen-mat-khau.dto';
import { QuenMatKhauDocument } from './quen-mat-khau.entity';
export declare class QuenMatKhauService {
    private model;
    constructor(model: Model<QuenMatKhauDocument>);
    create(dto: QuenMatKhauDto): Promise<QuenMatKhauDocument>;
    findAll(): Promise<any[]>;
    findOne(id: string): Promise<{
        _id: string;
        emailND: string;
        nguoiDung: {
            _id: any;
            hoTen: string;
        };
        conHan: boolean;
    }>;
    check_conHan(id: string): Promise<boolean>;
    update(id: string, dto: QuenMatKhauDto): Promise<QuenMatKhauDocument>;
    remove(id: string): Promise<QuenMatKhauDocument>;
}
