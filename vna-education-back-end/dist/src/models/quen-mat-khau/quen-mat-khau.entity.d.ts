import { Document, Schema as MongooseSchema } from 'mongoose';
import { NguoiDung } from '../nguoi-dung/nguoi-dung.entity';
export declare type QuenMatKhauDocument = QuenMatKhau & Document;
export declare class QuenMatKhau {
    nguoiDung: NguoiDung;
    emailND: string;
    conHan: boolean;
}
export declare const QuenMatKhauSchema: MongooseSchema<Document<QuenMatKhau, any, any>, import("mongoose").Model<any, any, any>, undefined, any>;
