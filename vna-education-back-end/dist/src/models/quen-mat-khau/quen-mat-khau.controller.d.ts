import { QuenMatKhauDto } from './quen-mat-khau.dto';
import { QuenMatKhauService } from './quen-mat-khau.service';
export declare class QuenMatKhauController {
    private readonly service;
    constructor(service: QuenMatKhauService);
    create(dto: QuenMatKhauDto): Promise<import("./quen-mat-khau.entity").QuenMatKhauDocument>;
    findAll(): Promise<any[]>;
    kiemTra_conHan(id: string): Promise<boolean>;
    findOne(id: string): Promise<{
        _id: string;
        emailND: string;
        nguoiDung: {
            _id: any;
            hoTen: string;
        };
        conHan: boolean;
    }>;
    update(id: string, dto: QuenMatKhauDto): Promise<import("./quen-mat-khau.entity").QuenMatKhauDocument>;
    remove(id: string): Promise<import("./quen-mat-khau.entity").QuenMatKhauDocument>;
}
