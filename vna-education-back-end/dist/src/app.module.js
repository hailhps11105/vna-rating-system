"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppModule = void 0;
const common_1 = require("@nestjs/common");
const config_1 = require("@nestjs/config");
const mongoose_1 = require("@nestjs/mongoose");
const mailer_1 = require("@nestjs-modules/mailer");
const pug_adapter_1 = require("@nestjs-modules/mailer/dist/adapters/pug.adapter");
const app_controller_1 = require("./app.controller");
const app_service_1 = require("./app.service");
const nguoi_dung_module_1 = require("./models/nguoi-dung/nguoi-dung.module");
const danh_gia_module_1 = require("./models/danh-gia/danh-gia.module");
const thong_bao_module_1 = require("./models/thong-bao/thong-bao.module");
const diem_danh_module_1 = require("./models/diem-danh/diem-danh.module");
const tuan_hoc_module_1 = require("./models/tuan-hoc/tuan-hoc.module");
const tiet_hoc_module_1 = require("./models/tiet-hoc/tiet-hoc.module");
const lop_hoc_module_1 = require("./models/lop-hoc/lop-hoc.module");
const mon_hoc_module_1 = require("./models/mon-hoc/mon-hoc.module");
const mau_danh_gia_module_1 = require("./models/mau-danh-gia/mau-danh-gia.module");
const bang_diem_tong_module_1 = require("./models/bang-diem-tong/bang-diem-tong.module");
const bang_diem_mon_module_1 = require("./models/bang-diem-mon/bang-diem-mon.module");
const nam_hoc_module_1 = require("./models/nam-hoc/nam-hoc.module");
const buoi_hoc_module_1 = require("./models/buoi-hoc/buoi-hoc.module");
const hoc_sinh_module_1 = require("./users/hoc-sinh/hoc-sinh.module");
const quan_tri_module_1 = require("./users/quan-tri/quan-tri.module");
const giao_vien_module_1 = require("./users/giao-vien/giao-vien.module");
const phu_huynh_module_1 = require("./users/phu-huynh/phu-huynh.module");
const hieu_truong_module_1 = require("./users/hieu-truong/hieu-truong.module");
const quen_mat_khau_module_1 = require("./models/quen-mat-khau/quen-mat-khau.module");
let AppModule = class AppModule {
};
AppModule = __decorate([
    common_1.Module({
        imports: [
            config_1.ConfigModule.forRoot(),
            mongoose_1.MongooseModule.forRoot(`mongodb+srv://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@${process.env.DB_CLUSTER}.dlidw.mongodb.net/${process.env.DB_COLLECTION}?retryWrites=true&w=majority`, {
                useCreateIndex: true,
                useNewUrlParser: true,
                useUnifiedTopology: true,
                useFindAndModify: false,
            }),
            mailer_1.MailerModule.forRoot({
                transport: {
                    host: 'smtp.mailtrap.io',
                    port: 2525,
                    auth: {
                        user: 'd6b759ebf83e35',
                        pass: '80a62a0bdec129',
                    },
                },
                template: {
                    dir: __dirname + '/helpers/mails/',
                    adapter: new pug_adapter_1.PugAdapter(),
                    options: {
                        strict: true,
                    },
                },
            }),
            nguoi_dung_module_1.NguoiDungModule,
            danh_gia_module_1.DanhGiaModule,
            thong_bao_module_1.ThongBaoModule,
            diem_danh_module_1.DiemDanhModule,
            tuan_hoc_module_1.TuanHocModule,
            tiet_hoc_module_1.TietHocModule,
            lop_hoc_module_1.LopHocModule,
            mon_hoc_module_1.MonHocModule,
            mau_danh_gia_module_1.MauDanhGiaModule,
            bang_diem_tong_module_1.BangDiemTongModule,
            bang_diem_mon_module_1.BangDiemMonModule,
            nam_hoc_module_1.NamHocModule,
            buoi_hoc_module_1.BuoiHocModule,
            hoc_sinh_module_1.HocSinhModule,
            quan_tri_module_1.QuanTriModule,
            giao_vien_module_1.GiaoVienModule,
            phu_huynh_module_1.PhuHuynhModule,
            hieu_truong_module_1.HieuTruongModule,
            quen_mat_khau_module_1.QuenMatKhauModule,
        ],
        controllers: [app_controller_1.AppController],
        providers: [app_service_1.AppService],
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map