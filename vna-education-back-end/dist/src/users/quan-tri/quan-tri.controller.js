"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.QuanTriController = void 0;
const openapi = require("@nestjs/swagger");
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const auth_guard_1 = require("../../auth.guard");
const quan_tri_service_1 = require("./quan-tri.service");
let QuanTriController = class QuanTriController {
    service;
    constructor(service) {
        this.service = service;
    }
    async tatCa_danhGia() {
        return await this.service.tatCa_danhGia();
    }
    async layCDG(hs, tuan) {
        return await this.service.layDG_chuaXong(hs, tuan);
    }
    async layLichHoc(lop, tuan) {
        return await this.service.traLichHoc(tuan, lop);
    }
    async dongMo_taiKhoan(tk, tt) {
        return await this.service.dongMo_taiKhoan(tk, tt);
    }
};
__decorate([
    common_1.Get('danh-gia'),
    openapi.ApiResponse({ status: 200, type: [Object] }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], QuanTriController.prototype, "tatCa_danhGia", null);
__decorate([
    common_1.Get('chua-danh-gia'),
    openapi.ApiResponse({ status: 200, type: [Object] }),
    __param(0, common_1.Query('hs')),
    __param(1, common_1.Query('tuan')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String]),
    __metadata("design:returntype", Promise)
], QuanTriController.prototype, "layCDG", null);
__decorate([
    common_1.Get('lich-hoc'),
    openapi.ApiResponse({ status: 200, type: [Object] }),
    __param(0, common_1.Query('lop')),
    __param(1, common_1.Query('tuan')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String]),
    __metadata("design:returntype", Promise)
], QuanTriController.prototype, "layLichHoc", null);
__decorate([
    common_1.Patch('tai-khoan/:tk'),
    openapi.ApiResponse({ status: 200, type: Object }),
    __param(0, common_1.Param('tk')),
    __param(1, common_1.Query('trangThai')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Boolean]),
    __metadata("design:returntype", Promise)
], QuanTriController.prototype, "dongMo_taiKhoan", null);
QuanTriController = __decorate([
    common_1.Controller('quan-tri'),
    common_1.UseGuards(auth_guard_1.AuthGuard),
    swagger_1.ApiTags('quan-tri'),
    __metadata("design:paramtypes", [quan_tri_service_1.QuanTriService])
], QuanTriController);
exports.QuanTriController = QuanTriController;
//# sourceMappingURL=quan-tri.controller.js.map