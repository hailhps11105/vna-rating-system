import { QuanTriService } from './quan-tri.service';
export declare class QuanTriController {
    private service;
    constructor(service: QuanTriService);
    tatCa_danhGia(): Promise<any[]>;
    layCDG(hs: string, tuan: string): Promise<any[]>;
    layLichHoc(lop: string, tuan: string): Promise<any[]>;
    dongMo_taiKhoan(tk: string, tt: boolean): Promise<import("../../models/nguoi-dung/nguoi-dung.entity").NguoiDungDocument>;
}
