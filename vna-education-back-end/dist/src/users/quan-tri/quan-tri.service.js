"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.QuanTriService = void 0;
const common_1 = require("@nestjs/common");
const utilities_1 = require("../../helpers/utilities");
const danh_gia_service_1 = require("../../models/danh-gia/danh-gia.service");
const lop_hoc_service_1 = require("../../models/lop-hoc/lop-hoc.service");
const account_service_1 = require("../../models/nguoi-dung/actions/account.service");
const tiet_hoc_service_1 = require("../../models/tiet-hoc/tiet-hoc.service");
const tuan_hoc_service_1 = require("../../models/tuan-hoc/tuan-hoc.service");
let QuanTriService = class QuanTriService {
    dgSer;
    thSer;
    lhSer;
    tuanSer;
    accSer;
    constructor(dgSer, thSer, lhSer, tuanSer, accSer) {
        this.dgSer = dgSer;
        this.thSer = thSer;
        this.lhSer = lhSer;
        this.tuanSer = tuanSer;
        this.accSer = accSer;
    }
    async tatCa_danhGia() {
        return await this.dgSer.findAll();
    }
    async layDG_chuaXong(hs, tuan) {
        return await this.dgSer.getUnfinished(hs, tuan);
    }
    async traLichHoc(tuan, lop) {
        const week = await this.tuanSer.findOne(tuan);
        const tiet = await this.thSer.findAll({ lopHoc: Object(lop) });
        const result = [];
        for (let i = 0; i < tiet.length; i++) {
            if (tiet[i].buoiHoc.tuanHoc === week.soTuan) {
                const { lopHoc, buoiHoc, thoiGian, ...t } = tiet[i];
                result.push(t);
            }
        }
        return result.sort((a, b) => {
            return utilities_1.sessionSort(a.thuTiet, b.thuTiet);
        });
    }
    async dongMo_taiKhoan(tk, trangThai) {
        return await this.accSer.toggleActivation(tk, trangThai);
    }
};
QuanTriService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [danh_gia_service_1.DanhGiaService,
        tiet_hoc_service_1.TietHocService,
        lop_hoc_service_1.LopHocService,
        tuan_hoc_service_1.TuanHocService,
        account_service_1.AccountService])
], QuanTriService);
exports.QuanTriService = QuanTriService;
//# sourceMappingURL=quan-tri.service.js.map