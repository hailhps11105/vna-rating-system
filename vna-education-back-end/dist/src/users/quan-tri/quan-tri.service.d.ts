import { DanhGiaService } from '../../models/danh-gia/danh-gia.service';
import { LopHocService } from '../../models/lop-hoc/lop-hoc.service';
import { AccountService } from '../../models/nguoi-dung/actions/account.service';
import { TietHocService } from '../../models/tiet-hoc/tiet-hoc.service';
import { TuanHocService } from '../../models/tuan-hoc/tuan-hoc.service';
export declare class QuanTriService {
    private readonly dgSer;
    private readonly thSer;
    private readonly lhSer;
    private readonly tuanSer;
    private readonly accSer;
    constructor(dgSer: DanhGiaService, thSer: TietHocService, lhSer: LopHocService, tuanSer: TuanHocService, accSer: AccountService);
    tatCa_danhGia(): Promise<any[]>;
    layDG_chuaXong(hs: string, tuan: string): Promise<any[]>;
    traLichHoc(tuan: string, lop: string): Promise<any[]>;
    dongMo_taiKhoan(tk: string, trangThai: boolean): Promise<import("../../models/nguoi-dung/nguoi-dung.entity").NguoiDungDocument>;
}
