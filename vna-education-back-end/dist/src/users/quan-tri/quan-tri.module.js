"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.QuanTriModule = void 0;
const common_1 = require("@nestjs/common");
const danh_gia_module_1 = require("../../models/danh-gia/danh-gia.module");
const lop_hoc_module_1 = require("../../models/lop-hoc/lop-hoc.module");
const nguoi_dung_module_1 = require("../../models/nguoi-dung/nguoi-dung.module");
const tiet_hoc_module_1 = require("../../models/tiet-hoc/tiet-hoc.module");
const tuan_hoc_module_1 = require("../../models/tuan-hoc/tuan-hoc.module");
const quan_tri_controller_1 = require("./quan-tri.controller");
const quan_tri_service_1 = require("./quan-tri.service");
let QuanTriModule = class QuanTriModule {
};
QuanTriModule = __decorate([
    common_1.Module({
        imports: [
            nguoi_dung_module_1.NguoiDungModule,
            danh_gia_module_1.DanhGiaModule,
            tuan_hoc_module_1.TuanHocModule,
            lop_hoc_module_1.LopHocModule,
            tiet_hoc_module_1.TietHocModule,
        ],
        controllers: [quan_tri_controller_1.QuanTriController],
        providers: [quan_tri_service_1.QuanTriService],
    })
], QuanTriModule);
exports.QuanTriModule = QuanTriModule;
//# sourceMappingURL=quan-tri.module.js.map