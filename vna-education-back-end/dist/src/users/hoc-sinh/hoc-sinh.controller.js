"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.HocSinhController = void 0;
const openapi = require("@nestjs/swagger");
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const auth_guard_1 = require("../../auth.guard");
const HSDG_dto_1 = require("../../models/danh-gia/dto/HSDG.dto");
const hoc_sinh_service_1 = require("./hoc-sinh.service");
let HocSinhController = class HocSinhController {
    service;
    constructor(service) {
        this.service = service;
    }
    async vaoLop_nhieuHS(hs, lop) {
        return await this.service.enrollMore(hs, lop);
    }
    async vaoLop_motHS(hs, lop) {
        return await this.service.enrollOne(hs, lop);
    }
    async xemDiem(hs) {
        return await this.service.seeReport(hs);
    }
    async danhSach_hocSinh(lop) {
        return await this.service.ofClass(lop);
    }
    async hetDG(hs, tuan) {
        return await this.service.getReviews(hs, tuan);
    }
    async danhGia(id, dto) {
        return await this.service.makeReview(id, dto);
    }
    async motDG(id) {
        return await this.service.getReview(id);
    }
    async hetDD(hs) {
        return await this.service.getPresented(hs);
    }
};
__decorate([
    common_1.Patch('vao-lop/:lop'),
    openapi.ApiResponse({ status: 200, type: Object }),
    __param(0, common_1.Body()),
    __param(1, common_1.Param('lop')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Array, String]),
    __metadata("design:returntype", Promise)
], HocSinhController.prototype, "vaoLop_nhieuHS", null);
__decorate([
    common_1.Patch(':hs/vao-lop/:lop'),
    openapi.ApiResponse({ status: 200, type: Object }),
    __param(0, common_1.Param('hs')),
    __param(1, common_1.Param('lop')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String]),
    __metadata("design:returntype", Promise)
], HocSinhController.prototype, "vaoLop_motHS", null);
__decorate([
    common_1.Get(':hs/diem-so'),
    openapi.ApiResponse({ status: 200, type: Object }),
    __param(0, common_1.Param('hs')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], HocSinhController.prototype, "xemDiem", null);
__decorate([
    common_1.Get('thuoc-lop/:lop'),
    openapi.ApiResponse({ status: 200, type: [Object] }),
    __param(0, common_1.Param('lop')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], HocSinhController.prototype, "danhSach_hocSinh", null);
__decorate([
    common_1.Get(':hs/danh-gia'),
    openapi.ApiResponse({ status: 200, type: [Object] }),
    __param(0, common_1.Param('hs')),
    __param(1, common_1.Query('tuan')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String]),
    __metadata("design:returntype", Promise)
], HocSinhController.prototype, "hetDG", null);
__decorate([
    common_1.Post('danh-gia/:id'),
    openapi.ApiResponse({ status: 201, type: Object }),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, HSDG_dto_1.HSDGDto]),
    __metadata("design:returntype", Promise)
], HocSinhController.prototype, "danhGia", null);
__decorate([
    common_1.Get('danh-gia/:id'),
    openapi.ApiResponse({ status: 200 }),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], HocSinhController.prototype, "motDG", null);
__decorate([
    common_1.Get(':hs/diem-danh'),
    openapi.ApiResponse({ status: 200, type: [Object] }),
    __param(0, common_1.Param('hs')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], HocSinhController.prototype, "hetDD", null);
HocSinhController = __decorate([
    common_1.Controller('hoc-sinh'),
    common_1.UseGuards(auth_guard_1.AuthGuard),
    swagger_1.ApiTags('hoc-sinh'),
    __metadata("design:paramtypes", [hoc_sinh_service_1.HocSinhService])
], HocSinhController);
exports.HocSinhController = HocSinhController;
//# sourceMappingURL=hoc-sinh.controller.js.map