"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.HocSinhService = void 0;
const common_1 = require("@nestjs/common");
const bang_diem_tong_service_1 = require("../../models/bang-diem-tong/bang-diem-tong.service");
const choHS_service_1 = require("../../models/danh-gia/roles/choHS.service");
const diem_danh_service_1 = require("../../models/diem-danh/diem-danh.service");
const lop_hoc_service_1 = require("../../models/lop-hoc/lop-hoc.service");
let HocSinhService = class HocSinhService {
    dgSer;
    lhSer;
    bdtSer;
    ddSer;
    constructor(dgSer, lhSer, bdtSer, ddSer) {
        this.dgSer = dgSer;
        this.lhSer = lhSer;
        this.bdtSer = bdtSer;
        this.ddSer = ddSer;
    }
    async enrollMore(hs, lop) {
        return await this.lhSer.addBulkHS(hs, lop);
    }
    async enrollOne(hs, lop) {
        return await this.lhSer.addOneHS(hs, lop);
    }
    async makeReview(id, dto) {
        return await this.dgSer.update_fromHS(id, dto);
    }
    async ofClass(lop) {
        return await this.lhSer.onlyHS(lop);
    }
    async seeReport(hs) {
        return await this.bdtSer.getOne_byHS(hs);
    }
    async getReviews(hs, week) {
        return await this.dgSer.getAll_byHS(hs, week);
    }
    async getReview(rev) {
        return await this.dgSer.getOne_forHS(rev);
    }
    async getPresented(hs) {
        return await this.ddSer.findAll({ hocSinh: Object(hs) });
    }
};
HocSinhService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [choHS_service_1.ChoHocSinhService,
        lop_hoc_service_1.LopHocService,
        bang_diem_tong_service_1.BangDiemTongService,
        diem_danh_service_1.DiemDanhService])
], HocSinhService);
exports.HocSinhService = HocSinhService;
//# sourceMappingURL=hoc-sinh.service.js.map