import { BangDiemTongService } from '../../models/bang-diem-tong/bang-diem-tong.service';
import { HSDGDto } from '../../models/danh-gia/dto/HSDG.dto';
import { ChoHocSinhService } from '../../models/danh-gia/roles/choHS.service';
import { DiemDanhService } from '../../models/diem-danh/diem-danh.service';
import { LopHocService } from '../../models/lop-hoc/lop-hoc.service';
export declare class HocSinhService {
    private readonly dgSer;
    private readonly lhSer;
    private readonly bdtSer;
    private readonly ddSer;
    constructor(dgSer: ChoHocSinhService, lhSer: LopHocService, bdtSer: BangDiemTongService, ddSer: DiemDanhService);
    enrollMore(hs: string[], lop: string): Promise<import("../../models/lop-hoc/lop-hoc.entity").LopHocDocument>;
    enrollOne(hs: string, lop: string): Promise<any[] | import("../../models/lop-hoc/lop-hoc.entity").LopHocDocument>;
    makeReview(id: string, dto: HSDGDto): Promise<import("../../models/danh-gia/danh-gia.entity").DanhGiaDocument>;
    ofClass(lop: string): Promise<any[]>;
    seeReport(hs: string): Promise<any>;
    getReviews(hs: string, week: string): Promise<any[]>;
    getReview(rev: string): Promise<{
        chiTiet: {
            idLop: any;
            lopHoc: string;
            siSo: number;
            hocSinhDG: import("../../models/danh-gia/chiTietDG.schema").ChiTietDG[];
        };
        hetHan: boolean;
        _id: string;
        tenDG: string;
        tieuChi: import("../../models/mau-danh-gia/tieuChi.schema").TieuChi[];
        tuanDG: number;
        giaoVien: string;
        choGVCN: boolean;
        daDuyet: boolean;
        monHoc: string;
    }>;
    getPresented(hs: string): Promise<any[]>;
}
