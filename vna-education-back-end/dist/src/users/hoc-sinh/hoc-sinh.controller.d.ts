import { HSDGDto } from '../../models/danh-gia/dto/HSDG.dto';
import { HocSinhService } from './hoc-sinh.service';
export declare class HocSinhController {
    private service;
    constructor(service: HocSinhService);
    vaoLop_nhieuHS(hs: string[], lop: string): Promise<import("../../models/lop-hoc/lop-hoc.entity").LopHocDocument>;
    vaoLop_motHS(hs: string, lop: string): Promise<any[] | import("../../models/lop-hoc/lop-hoc.entity").LopHocDocument>;
    xemDiem(hs: string): Promise<any>;
    danhSach_hocSinh(lop: string): Promise<any[]>;
    hetDG(hs: string, tuan: string): Promise<any[]>;
    danhGia(id: string, dto: HSDGDto): Promise<import("../../models/danh-gia/danh-gia.entity").DanhGiaDocument>;
    motDG(id: string): Promise<{
        chiTiet: {
            idLop: any;
            lopHoc: string;
            siSo: number;
            hocSinhDG: import("../../models/danh-gia/chiTietDG.schema").ChiTietDG[];
        };
        hetHan: boolean;
        _id: string;
        tenDG: string;
        tieuChi: import("../../models/mau-danh-gia/tieuChi.schema").TieuChi[];
        tuanDG: number;
        giaoVien: string;
        choGVCN: boolean;
        daDuyet: boolean;
        monHoc: string;
    }>;
    hetDD(hs: string): Promise<any[]>;
}
