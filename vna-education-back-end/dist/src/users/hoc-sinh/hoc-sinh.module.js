"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.HocSinhModule = void 0;
const common_1 = require("@nestjs/common");
const bang_diem_tong_module_1 = require("../../models/bang-diem-tong/bang-diem-tong.module");
const danh_gia_module_1 = require("../../models/danh-gia/danh-gia.module");
const diem_danh_module_1 = require("../../models/diem-danh/diem-danh.module");
const lop_hoc_module_1 = require("../../models/lop-hoc/lop-hoc.module");
const nguoi_dung_module_1 = require("../../models/nguoi-dung/nguoi-dung.module");
const hoc_sinh_controller_1 = require("./hoc-sinh.controller");
const hoc_sinh_service_1 = require("./hoc-sinh.service");
let HocSinhModule = class HocSinhModule {
};
HocSinhModule = __decorate([
    common_1.Module({
        imports: [
            danh_gia_module_1.DanhGiaModule,
            nguoi_dung_module_1.NguoiDungModule,
            lop_hoc_module_1.LopHocModule,
            bang_diem_tong_module_1.BangDiemTongModule,
            diem_danh_module_1.DiemDanhModule,
        ],
        controllers: [hoc_sinh_controller_1.HocSinhController],
        providers: [hoc_sinh_service_1.HocSinhService],
    })
], HocSinhModule);
exports.HocSinhModule = HocSinhModule;
//# sourceMappingURL=hoc-sinh.module.js.map