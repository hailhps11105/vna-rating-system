"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PhuHuynhModule = void 0;
const common_1 = require("@nestjs/common");
const nguoi_dung_module_1 = require("../../models/nguoi-dung/nguoi-dung.module");
const phu_huynh_controller_1 = require("./phu-huynh.controller");
const phu_huynh_service_1 = require("./phu-huynh.service");
let PhuHuynhModule = class PhuHuynhModule {
};
PhuHuynhModule = __decorate([
    common_1.Module({
        imports: [nguoi_dung_module_1.NguoiDungModule],
        controllers: [phu_huynh_controller_1.PhuHuynhController],
        providers: [phu_huynh_service_1.PhuHuynhService],
    })
], PhuHuynhModule);
exports.PhuHuynhModule = PhuHuynhModule;
//# sourceMappingURL=phu-huynh.module.js.map