"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PhuHuynhController = void 0;
const openapi = require("@nestjs/swagger");
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const auth_guard_1 = require("../../auth.guard");
let PhuHuynhController = class PhuHuynhController {
    async phuHuynh() {
        return '';
    }
};
__decorate([
    common_1.Get(),
    common_1.Render('phu-huynh'),
    openapi.ApiResponse({ status: 200, type: String }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], PhuHuynhController.prototype, "phuHuynh", null);
PhuHuynhController = __decorate([
    common_1.Controller('phu-huynh'),
    common_1.UseGuards(auth_guard_1.AuthGuard),
    swagger_1.ApiTags('phu-huynh')
], PhuHuynhController);
exports.PhuHuynhController = PhuHuynhController;
//# sourceMappingURL=phu-huynh.controller.js.map