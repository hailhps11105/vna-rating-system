"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GiaoVienService = void 0;
const common_1 = require("@nestjs/common");
const choGV_service_1 = require("../../models/danh-gia/roles/choGV.service");
const lop_hoc_service_1 = require("../../models/lop-hoc/lop-hoc.service");
const mon_hoc_service_1 = require("../../models/mon-hoc/mon-hoc.service");
let GiaoVienService = class GiaoVienService {
    mhSer;
    lhSer;
    dgSer;
    constructor(mhSer, lhSer, dgSer) {
        this.mhSer = mhSer;
        this.lhSer = lhSer;
        this.dgSer = dgSer;
    }
    async xemHet_danhGia(gv, tuan) {
        return await this.dgSer.getAll_byGV(gv, tuan);
    }
    async xemMot_danhGia(rev) {
        return await this.dgSer.getOne_forGV(rev);
    }
    async giaoVien_theoMon(mon) {
        const sub = await this.mhSer.findOne(mon);
        return sub.giaoVien;
    }
    async laGVCN() {
        return await this.lhSer.onlyGV();
    }
};
GiaoVienService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [mon_hoc_service_1.MonHocService,
        lop_hoc_service_1.LopHocService,
        choGV_service_1.ChoGiaoVienService])
], GiaoVienService);
exports.GiaoVienService = GiaoVienService;
//# sourceMappingURL=giao-vien.service.js.map