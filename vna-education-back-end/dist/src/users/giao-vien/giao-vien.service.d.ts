import { ChoGiaoVienService } from '../../models/danh-gia/roles/choGV.service';
import { LopHocService } from '../../models/lop-hoc/lop-hoc.service';
import { MonHocService } from '../../models/mon-hoc/mon-hoc.service';
export declare class GiaoVienService {
    private readonly mhSer;
    private readonly lhSer;
    private readonly dgSer;
    constructor(mhSer: MonHocService, lhSer: LopHocService, dgSer: ChoGiaoVienService);
    xemHet_danhGia(gv: string, tuan: string): Promise<any[]>;
    xemMot_danhGia(rev: string): Promise<{
        _id: string;
        tenDG: string;
        monHoc: string;
        tieuChi: import("../../models/mau-danh-gia/tieuChi.schema").TieuChi[];
        choGVCN: boolean;
        daDuyet: boolean;
        lopHoc: {
            maLH: string;
            siSo: number;
            luotDG: number;
        };
        tuanDG: number;
        chiTiet: import("../../models/danh-gia/chiTietDG.schema").ChiTietDG[];
        diemDG: number;
    }>;
    giaoVien_theoMon(mon: string): Promise<{
        _id: any;
        hoTen: string;
    }[]>;
    laGVCN(): Promise<any[]>;
}
