"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GiaoVienModule = void 0;
const common_1 = require("@nestjs/common");
const danh_gia_module_1 = require("../../models/danh-gia/danh-gia.module");
const lop_hoc_module_1 = require("../../models/lop-hoc/lop-hoc.module");
const mon_hoc_module_1 = require("../../models/mon-hoc/mon-hoc.module");
const nguoi_dung_module_1 = require("../../models/nguoi-dung/nguoi-dung.module");
const giao_vien_controller_1 = require("./giao-vien.controller");
const giao_vien_service_1 = require("./giao-vien.service");
let GiaoVienModule = class GiaoVienModule {
};
GiaoVienModule = __decorate([
    common_1.Module({
        imports: [nguoi_dung_module_1.NguoiDungModule, mon_hoc_module_1.MonHocModule, lop_hoc_module_1.LopHocModule, danh_gia_module_1.DanhGiaModule],
        controllers: [giao_vien_controller_1.GiaoVienController],
        providers: [giao_vien_service_1.GiaoVienService],
    })
], GiaoVienModule);
exports.GiaoVienModule = GiaoVienModule;
//# sourceMappingURL=giao-vien.module.js.map