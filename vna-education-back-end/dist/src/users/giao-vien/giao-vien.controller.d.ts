import { GiaoVienService } from './giao-vien.service';
export declare class GiaoVienController {
    private readonly service;
    constructor(service: GiaoVienService);
    layDanhGia(gv: string, tuan: string): Promise<any[]>;
    xemDanhGia(dg: string): Promise<{
        _id: string;
        tenDG: string;
        monHoc: string;
        tieuChi: import("../../models/mau-danh-gia/tieuChi.schema").TieuChi[];
        choGVCN: boolean;
        daDuyet: boolean;
        lopHoc: {
            maLH: string;
            siSo: number;
            luotDG: number;
        };
        tuanDG: number;
        chiTiet: import("../../models/danh-gia/chiTietDG.schema").ChiTietDG[];
        diemDG: number;
    }>;
    giaoVien(mon: string): Promise<{
        _id: any;
        hoTen: string;
    }[]>;
    chuNhiem(): Promise<any[]>;
}
