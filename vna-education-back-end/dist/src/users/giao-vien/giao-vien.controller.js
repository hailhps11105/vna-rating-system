"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GiaoVienController = void 0;
const openapi = require("@nestjs/swagger");
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const auth_guard_1 = require("../../auth.guard");
const giao_vien_service_1 = require("./giao-vien.service");
let GiaoVienController = class GiaoVienController {
    service;
    constructor(service) {
        this.service = service;
    }
    async layDanhGia(gv, tuan) {
        return await this.service.xemHet_danhGia(gv, tuan);
    }
    async xemDanhGia(dg) {
        return await this.service.xemMot_danhGia(dg);
    }
    async giaoVien(mon) {
        if (mon && mon != '')
            return await this.service.giaoVien_theoMon(mon);
    }
    async chuNhiem() {
        return await this.service.laGVCN();
    }
};
__decorate([
    common_1.Get('danh-gia'),
    openapi.ApiResponse({ status: 200, type: [Object] }),
    __param(0, common_1.Query('gv')),
    __param(1, common_1.Query('tuan')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String]),
    __metadata("design:returntype", Promise)
], GiaoVienController.prototype, "layDanhGia", null);
__decorate([
    common_1.Get('danh-gia/:dg'),
    openapi.ApiResponse({ status: 200 }),
    __param(0, common_1.Param('dg')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], GiaoVienController.prototype, "xemDanhGia", null);
__decorate([
    common_1.Get('theo'),
    swagger_1.ApiQuery({
        name: 'mon',
        type: String,
        description: '_id của môn học',
    }),
    swagger_1.ApiOkResponse({ description: 'Trả về các đối tượng giáo viên' }),
    swagger_1.ApiForbiddenResponse({
        description: 'Ngăn cản truy cập do chưa đăng nhập vào hệ thống',
    }),
    openapi.ApiResponse({ status: 200 }),
    __param(0, common_1.Query('mon')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], GiaoVienController.prototype, "giaoVien", null);
__decorate([
    common_1.Get('chu-nhiem'),
    openapi.ApiResponse({ status: 200, type: [Object] }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], GiaoVienController.prototype, "chuNhiem", null);
GiaoVienController = __decorate([
    common_1.Controller('giao-vien'),
    common_1.UseGuards(auth_guard_1.AuthGuard),
    swagger_1.ApiTags('giao-vien'),
    __metadata("design:paramtypes", [giao_vien_service_1.GiaoVienService])
], GiaoVienController);
exports.GiaoVienController = GiaoVienController;
//# sourceMappingURL=giao-vien.controller.js.map