import { HieuTruongService } from './hieu-truong.service';
export declare class HieuTruongController {
    private service;
    constructor(service: HieuTruongService);
    xemDanhGia(tuan: string, lop: string): Promise<any[]>;
    xemMot_danhGia(dg: string): Promise<{
        _id: string;
        tenDG: string;
        monHoc: string;
        giaoVien: string;
        tieuChi: import("../../models/mau-danh-gia/tieuChi.schema").TieuChi[];
        choGVCN: boolean;
        daDuyet: boolean;
        lopHoc: {
            maLH: string;
            siSo: number;
            luotDG: number;
        };
        tuanDG: number;
        chiTiet: import("../../models/danh-gia/chiTietDG.schema").ChiTietDG[];
        diemDG: number;
    }>;
    xemDanhGiaGVBM(gv: string, lop: string): Promise<any[]>;
    xemDanhGiaGVCN(dg: string): Promise<{
        _id: string;
        tenDG: string;
        monHoc: import("../../models/mon-hoc/mon-hoc.entity").MonHoc;
        giaoVien: import("../../models/nguoi-dung/nguoi-dung.entity").NguoiDung;
        lopHoc: {
            _id: any;
            maLH: string;
            siSo: number;
            luotDG: number;
        };
        tuanDG: import("../../models/tuan-hoc/tuan-hoc.entity").TuanHoc;
        chiTiet: import("../../models/danh-gia/chiTietDG.schema").ChiTietDG[];
        diemTB: number;
    }>;
    xemDS_hocSinh(): Promise<any[]>;
    xemDS_giaoVien(): Promise<any[]>;
    duyetDG(dg: string, trangThai: boolean): Promise<import("../../models/danh-gia/danh-gia.entity").DanhGiaDocument>;
}
