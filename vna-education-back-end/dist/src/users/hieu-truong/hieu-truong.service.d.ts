import { DanhGiaService } from '../../models/danh-gia/danh-gia.service';
import { ChoHieuTruongService } from '../../models/danh-gia/roles/choHT.service';
import { NguoiDungService } from '../../models/nguoi-dung/nguoi-dung.service';
export declare class HieuTruongService {
    private dgSer;
    private ndSer;
    private ht;
    constructor(dgSer: DanhGiaService, ndSer: NguoiDungService, ht: ChoHieuTruongService);
    tatCa_DanhGia(tuan: string, lop?: string): Promise<any[]>;
    motDanhGia(dg: string): Promise<{
        _id: string;
        tenDG: string;
        monHoc: string;
        giaoVien: string;
        tieuChi: import("../../models/mau-danh-gia/tieuChi.schema").TieuChi[];
        choGVCN: boolean;
        daDuyet: boolean;
        lopHoc: {
            maLH: string;
            siSo: number;
            luotDG: number;
        };
        tuanDG: number;
        chiTiet: import("../../models/danh-gia/chiTietDG.schema").ChiTietDG[];
        diemDG: number;
    }>;
    duyetDG(idDG: string, tinhTrang?: boolean): Promise<import("../../models/danh-gia/danh-gia.entity").DanhGiaDocument>;
    danhGiaGVBM(gv: string, lop: string): Promise<any[]>;
    danhGiaGVCN(dg: string): Promise<{
        _id: string;
        tenDG: string;
        monHoc: import("../../models/mon-hoc/mon-hoc.entity").MonHoc;
        giaoVien: import("../../models/nguoi-dung/nguoi-dung.entity").NguoiDung;
        lopHoc: {
            _id: any;
            maLH: string;
            siSo: number;
            luotDG: number;
        };
        tuanDG: import("../../models/tuan-hoc/tuan-hoc.entity").TuanHoc;
        chiTiet: import("../../models/danh-gia/chiTietDG.schema").ChiTietDG[];
        diemTB: number;
    }>;
    tatCa_HocSinh(): Promise<any[]>;
    tatCa_GiaoVien(): Promise<any[]>;
}
