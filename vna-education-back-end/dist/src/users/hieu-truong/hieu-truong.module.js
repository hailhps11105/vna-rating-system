"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.HieuTruongModule = void 0;
const common_1 = require("@nestjs/common");
const danh_gia_module_1 = require("../../models/danh-gia/danh-gia.module");
const nguoi_dung_module_1 = require("../../models/nguoi-dung/nguoi-dung.module");
const hieu_truong_controller_1 = require("./hieu-truong.controller");
const hieu_truong_service_1 = require("./hieu-truong.service");
let HieuTruongModule = class HieuTruongModule {
};
HieuTruongModule = __decorate([
    common_1.Module({
        imports: [danh_gia_module_1.DanhGiaModule, nguoi_dung_module_1.NguoiDungModule],
        controllers: [hieu_truong_controller_1.HieuTruongController],
        providers: [hieu_truong_service_1.HieuTruongService],
    })
], HieuTruongModule);
exports.HieuTruongModule = HieuTruongModule;
//# sourceMappingURL=hieu-truong.module.js.map