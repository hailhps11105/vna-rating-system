"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.HieuTruongService = void 0;
const common_1 = require("@nestjs/common");
const danh_gia_service_1 = require("../../models/danh-gia/danh-gia.service");
const choHT_service_1 = require("../../models/danh-gia/roles/choHT.service");
const nguoi_dung_service_1 = require("../../models/nguoi-dung/nguoi-dung.service");
let HieuTruongService = class HieuTruongService {
    dgSer;
    ndSer;
    ht;
    constructor(dgSer, ndSer, ht) {
        this.dgSer = dgSer;
        this.ndSer = ndSer;
        this.ht = ht;
    }
    async tatCa_DanhGia(tuan, lop) {
        return await this.ht.getAll_forHT(tuan, lop);
    }
    async motDanhGia(dg) {
        return await this.ht.getOne_forHT(dg);
    }
    async duyetDG(idDG, tinhTrang = true) {
        return await this.ht.approve(idDG, tinhTrang);
    }
    async danhGiaGVBM(gv, lop) {
        return await this.ht.findAll_ofGVBM(gv, lop);
    }
    async danhGiaGVCN(dg) {
        return await this.ht.findOne_ofGVCN(dg);
    }
    async tatCa_HocSinh() {
        const result = [];
        const hs = await this.ndSer.findAll_byRole('HS');
        for (let i = 0; i < hs.length; i++) {
            const temp = {
                _id: hs[i]._id,
                maND: hs[i].maND,
                lopHoc: hs[i].lopHoc,
                hoTen: hs[i].hoTen,
            };
            result.push(temp);
        }
        return result;
    }
    async tatCa_GiaoVien() {
        const result = [];
        const gv = await this.ndSer.findAll_byRole('GV');
        for (let i = 0; i < gv.length; i++) {
            const temp = {
                _id: gv[i]._id,
                maND: gv[i].maND,
                chuNhiem: gv[i].chuNhiem,
                hoTen: gv[i].hoTen,
            };
            result.push(temp);
        }
        return result;
    }
};
HieuTruongService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [danh_gia_service_1.DanhGiaService,
        nguoi_dung_service_1.NguoiDungService,
        choHT_service_1.ChoHieuTruongService])
], HieuTruongService);
exports.HieuTruongService = HieuTruongService;
//# sourceMappingURL=hieu-truong.service.js.map