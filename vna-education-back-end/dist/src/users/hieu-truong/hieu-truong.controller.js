"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.HieuTruongController = void 0;
const openapi = require("@nestjs/swagger");
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const auth_guard_1 = require("../../auth.guard");
const hieu_truong_service_1 = require("./hieu-truong.service");
let HieuTruongController = class HieuTruongController {
    service;
    constructor(service) {
        this.service = service;
    }
    async xemDanhGia(tuan, lop) {
        return await this.service.tatCa_DanhGia(tuan, lop);
    }
    async xemMot_danhGia(dg) {
        return await this.service.motDanhGia(dg);
    }
    async xemDanhGiaGVBM(gv, lop) {
        return await this.service.danhGiaGVBM(gv, lop);
    }
    async xemDanhGiaGVCN(dg) {
        return await this.service.danhGiaGVCN(dg);
    }
    async xemDS_hocSinh() {
        return await this.service.tatCa_HocSinh();
    }
    async xemDS_giaoVien() {
        return await this.service.tatCa_GiaoVien();
    }
    async duyetDG(dg, trangThai) {
        return await this.service.duyetDG(dg, trangThai);
    }
};
__decorate([
    common_1.Get('danh-gia/theo'),
    openapi.ApiResponse({ status: 200, type: [Object] }),
    __param(0, common_1.Query('tuan')),
    __param(1, common_1.Query('lop')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String]),
    __metadata("design:returntype", Promise)
], HieuTruongController.prototype, "xemDanhGia", null);
__decorate([
    common_1.Get('danh-gia/:dg'),
    openapi.ApiResponse({ status: 200 }),
    __param(0, common_1.Param('dg')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], HieuTruongController.prototype, "xemMot_danhGia", null);
__decorate([
    common_1.Get('danh-gia/bo-mon'),
    openapi.ApiResponse({ status: 200, type: [Object] }),
    __param(0, common_1.Query('gv')),
    __param(1, common_1.Query('lop')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String]),
    __metadata("design:returntype", Promise)
], HieuTruongController.prototype, "xemDanhGiaGVBM", null);
__decorate([
    common_1.Get('danh-gia/chu-nhiem/:dg'),
    openapi.ApiResponse({ status: 200 }),
    __param(0, common_1.Param('dg')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], HieuTruongController.prototype, "xemDanhGiaGVCN", null);
__decorate([
    common_1.Get('danh-sach/hoc-sinh'),
    openapi.ApiResponse({ status: 200, type: [Object] }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], HieuTruongController.prototype, "xemDS_hocSinh", null);
__decorate([
    common_1.Get('danh-sach/giao-vien'),
    openapi.ApiResponse({ status: 200, type: [Object] }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], HieuTruongController.prototype, "xemDS_giaoVien", null);
__decorate([
    common_1.Patch('duyet-danh-gia/:dg'),
    openapi.ApiResponse({ status: 200, type: Object }),
    __param(0, common_1.Param('dg')),
    __param(1, common_1.Query('trangThai')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Boolean]),
    __metadata("design:returntype", Promise)
], HieuTruongController.prototype, "duyetDG", null);
HieuTruongController = __decorate([
    common_1.Controller('hieu-truong'),
    common_1.UseGuards(auth_guard_1.AuthGuard),
    swagger_1.ApiTags('hieu-truong'),
    __metadata("design:paramtypes", [hieu_truong_service_1.HieuTruongService])
], HieuTruongController);
exports.HieuTruongController = HieuTruongController;
//# sourceMappingURL=hieu-truong.controller.js.map